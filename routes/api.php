<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Faq Categories
    Route::apiResource('faq-categories', 'FaqCategoryApiController');

    // Faq Questions
    Route::apiResource('faq-questions', 'FaqQuestionApiController');

    // Content Categories
    Route::apiResource('content-categories', 'ContentCategoryApiController');

    // Content Tags
    Route::apiResource('content-tags', 'ContentTagApiController');

    // Content Pages
    Route::post('content-pages/media', 'ContentPageApiController@storeMedia')->name('content-pages.storeMedia');
    Route::apiResource('content-pages', 'ContentPageApiController');

    // Boards
    Route::post('boards/media', 'BoardApiController@storeMedia')->name('boards.storeMedia');
    Route::apiResource('boards', 'BoardApiController');

    // My Classes
    Route::post('my-classes/media', 'MyClassApiController@storeMedia')->name('my-classes.storeMedia');
    Route::apiResource('my-classes', 'MyClassApiController');

    // Subjects
    Route::post('subjects/media', 'SubjectApiController@storeMedia')->name('subjects.storeMedia');
    Route::apiResource('subjects', 'SubjectApiController');

    // Chapters
    Route::post('chapters/media', 'ChapterApiController@storeMedia')->name('chapters.storeMedia');
    Route::apiResource('chapters', 'ChapterApiController');

    // Units
    Route::post('units/media', 'UnitApiController@storeMedia')->name('units.storeMedia');
    Route::apiResource('units', 'UnitApiController');

    // Question Types
    Route::apiResource('question-types', 'QuestionTypeApiController');

    // Countries
    Route::apiResource('countries', 'CountriesApiController');

    // States
    Route::apiResource('states', 'StateApiController');

    // Cities
    Route::apiResource('cities', 'CityApiController');

    // Students
    Route::apiResource('students', 'StudentApiController');

    // Student Study Units
    Route::apiResource('student-study-units', 'StudentStudyUnitsApiController');

    // Languages
    Route::apiResource('languages', 'LanguageApiController');

    // Question Answers
    Route::post('question-answers/media', 'QuestionAnswerApiController@storeMedia')->name('question-answers.storeMedia');
    Route::apiResource('question-answers', 'QuestionAnswerApiController');
});
