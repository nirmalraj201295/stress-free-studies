<?php


Auth::routes();
Route::get('userVerification/{token}', 'UserVerificationController@approve')->name('userVerification');

Route::redirect('/', '/login');
// Route::get('/home', function () {
//     if (session('status')) {
//         return redirect()->route('admin.home')->with('status', session('status'));
//     }

//     return redirect()->route('admin.home');
// });


// Route::get('/', 'Auth\LoginController@login')->name('login');
// Route::get('/admin/login', 'Admin\LoginController@login')->name('login');


Route::get('admin/login', 'Auth\LoginController@showAdminLoginForm')->name('auth.admin.login');
Route::get('register', 'Auth\RegisterController@showStudentRegisterForm')->name('auth.register.student');

Route::post('register', 'Auth\RegisterController@createStudent')->name('athu.register.student');
Route::get('get_by_classes', 'Admin\SubjectController@get_by_myclasses')->name('subjects.get_by_classes');

Route::get('/dashboard', function () {
    if (session('status')) {
        return redirect()->route('admin.dashboard')->with('status', session('status'));
    }

    return redirect()->route('admin.dashboard');
});

Route::group(['as' => 'user.','namespace' => 'User', 'middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('subjectsajaxData','HomeController@subjectAjaxData')->name('subjects.ajaxData');
    Route::get('/mycourse', 'HomeController@mycourse')->name('mycourse');
	Route::get('mycoursesubajaxData','HomeController@mycourseSubAjaxData')->name('mycourse.subAajaxData');
    Route::post('courseassignajaxData','HomeController@courseAssignAjaxData')->name('course.assignAjaxData');
    Route::get('chaptersajaxData','HomeController@chapterAjaxData')->name('chapters.ajaxData');
    Route::get('unitsajaxData','HomeController@unitAjaxData')->name('units.ajaxData');
    Route::get('qaajaxData','HomeController@QAAjaxData')->name('qa.ajaxData');
    Route::post('qaajaxData','HomeController@QAAjaxSubmitData')->name('qa.ajaxData');

    Route::get('/chapters/{id}', 'HomeController@chaptersIndex')->name('chapters'); 
    Route::get('/units/{id}', 'HomeController@unitsIndex')->name('units'); 
    Route::get('/qa/{id}', 'HomeController@QAIndex')->name('qa'); 

    Route::get('/profile', 'ProfileController@index')->name('profile'); 
    Route::post('/changepassword', 'ProfileController@changePassword')->name('changepassword');
});

//Clear all cache:
Route::get('clearall', function() {
    // $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    return 'all cleared';
});




    Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth','adminChecker']], function () {
    // Route::get('/', 'HomeController@index')->name('home'); 

    Route::get('/', 'DashboardController@index')->name('dashboard'); 



    Route::get('user-alerts/read', 'UserAlertsController@read');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('settings/permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('settings/roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('settings/users', 'UsersController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // User Alerts
    Route::delete('user-alerts/destroy', 'UserAlertsController@massDestroy')->name('user-alerts.massDestroy');
    Route::resource('user-alerts', 'UserAlertsController', ['except' => ['edit', 'update']]);

    // Faq Categories
    Route::delete('faq-categories/destroy', 'FaqCategoryController@massDestroy')->name('faq-categories.massDestroy');
    Route::resource('faq-categories', 'FaqCategoryController');

    // Faq Questions
    Route::delete('faq-questions/destroy', 'FaqQuestionController@massDestroy')->name('faq-questions.massDestroy');
    Route::resource('faq-questions', 'FaqQuestionController');

    // Content Categories
    Route::delete('content-categories/destroy', 'ContentCategoryController@massDestroy')->name('content-categories.massDestroy');
    Route::resource('content-categories', 'ContentCategoryController');

    // Content Tags
    Route::delete('content-tags/destroy', 'ContentTagController@massDestroy')->name('content-tags.massDestroy');
    Route::resource('content-tags', 'ContentTagController');

    // Content Pages
    Route::delete('content-pages/destroy', 'ContentPageController@massDestroy')->name('content-pages.massDestroy');
    Route::post('content-pages/media', 'ContentPageController@storeMedia')->name('content-pages.storeMedia');
    Route::resource('content-pages', 'ContentPageController');

    // Boards
    Route::delete('boards/destroy', 'BoardController@massDestroy')->name('boards.massDestroy');
    Route::post('boards/media', 'BoardController@storeMedia')->name('boards.storeMedia');
    Route::resource('setup/boards', 'BoardController');
    Route::post('setup/boards/updates/{id}', 'BoardController@updates')->name('boards.updates');

    Route::get('ajaxData1','BoardController@ajaxData')->name('boards.ajaxData');
    Route::get('get_boards', 'BoardController@get_boards')->name('boards.get_boards');

    // My Classes
    Route::delete('my-classes/destroy', 'MyClassController@massDestroy')->name('my-classes.massDestroy');
    Route::post('my-classes/media', 'MyClassController@storeMedia')->name('my-classes.storeMedia');
    Route::resource('setup/my-classes', 'MyClassController');

    Route::get('ajaxData2','MyClassController@ajaxData')->name('my-classes.ajaxData');

    // Subjects
    Route::delete('subjects/destroy', 'SubjectController@massDestroy')->name('subjects.massDestroy');
    Route::post('subjects/media', 'SubjectController@storeMedia')->name('subjects.storeMedia');
    Route::resource('setup/subjects', 'SubjectController');
    
    Route::get('ajaxData3','SubjectController@ajaxData')->name('subjects.ajaxData');
    Route::get('get_by_classes', 'SubjectController@get_by_myclasses')->name('subjects.get_by_classes');

    // Chapters
    Route::delete('chapters/destroy', 'ChapterController@massDestroy')->name('chapters.massDestroy');
    Route::post('chapters/media', 'ChapterController@storeMedia')->name('chapters.storeMedia');
    Route::resource('setup/chapters', 'ChapterController');

    Route::get('ajaxData4','ChapterController@ajaxData')->name('chapters.ajaxData');
    Route::get('get_by_subjects', 'ChapterController@get_by_subjects')->name('chapters.get_by_subjects');
    Route::get('chapters/list', 'ChapterController@chapter_list')->name('chapters.list');

    // Units
    Route::delete('units/destroy', 'UnitController@massDestroy')->name('units.massDestroy');
    Route::post('units/media', 'UnitController@storeMedia')->name('units.storeMedia');
    Route::resource('units', 'UnitController');

    Route::get('ajaxData5','UnitController@ajaxData')->name('units.ajaxData');
    Route::get('get_by_chapters', 'UnitController@get_by_chapters')->name('units.get_by_chapters');
    Route::get('units/create', 'UnitController@create')->name('units.create');
    Route::post('units/sort_order', 'UnitController@sort_order')->name('units.sort_order');

    // Question Types
    Route::delete('question-types/destroy', 'QuestionTypeController@massDestroy')->name('question-types.massDestroy');
    Route::resource('question-types', 'QuestionTypeController');

    // Countries
    Route::delete('countries/destroy', 'CountriesController@massDestroy')->name('countries.massDestroy');
    Route::resource('countries', 'CountriesController');

    // States
    Route::delete('states/destroy', 'StateController@massDestroy')->name('states.massDestroy');
    Route::resource('states', 'StateController');

    // Cities
    Route::delete('cities/destroy', 'CityController@massDestroy')->name('cities.massDestroy');
    Route::resource('cities', 'CityController');

    // Students
    Route::delete('students/destroy', 'StudentController@massDestroy')->name('students.massDestroy');
    Route::resource('settings/students', 'StudentController');

    // Student Study Units
    Route::delete('student-study-units/destroy', 'StudentStudyUnitsController@massDestroy')->name('student-study-units.massDestroy');
    Route::resource('student-study-units', 'StudentStudyUnitsController');

    // Languages
    Route::delete('languages/destroy', 'LanguageController@massDestroy')->name('languages.massDestroy');
    Route::resource('languages', 'LanguageController');

    // Question Answers
    Route::delete('question-answers/destroy', 'QuestionAnswerController@massDestroy')->name('question-answers.massDestroy');
    Route::post('question-answers/media', 'QuestionAnswerController@storeMedia')->name('question-answers.storeMedia');
    Route::post('question-answers/parse-csv-import', 'QuestionAnswerController@parseCsvImport')->name('question-answers.parseCsvImport');
    Route::post('question-answers/process-csv-import', 'QuestionAnswerController@processCsvImport')->name('question-answers.processCsvImport');
    Route::resource('question-answers', 'QuestionAnswerController');

    Route::post('question-answers/sort_order', 'QuestionAnswerController@sort_order')->name('question-answers.sort_order');

    Route::get('global-search', 'GlobalSearchController@search')->name('globalSearch');
    Route::get('messenger', 'MessengerController@index')->name('messenger.index');
    Route::get('messenger/create', 'MessengerController@createTopic')->name('messenger.createTopic');
    Route::post('messenger', 'MessengerController@storeTopic')->name('messenger.storeTopic');
    Route::get('messenger/inbox', 'MessengerController@showInbox')->name('messenger.showInbox');
    Route::get('messenger/outbox', 'MessengerController@showOutbox')->name('messenger.showOutbox');
    Route::get('messenger/{topic}', 'MessengerController@showMessages')->name('messenger.showMessages');
    Route::delete('messenger/{topic}', 'MessengerController@destroyTopic')->name('messenger.destroyTopic');
    Route::post('messenger/{topic}/reply', 'MessengerController@replyToTopic')->name('messenger.reply');
    Route::get('messenger/{topic}/reply', 'MessengerController@showReply')->name('messenger.showReply');
});
