<?php

namespace App;

use App\Notifications\VerifyUserNotification;
use Carbon\Carbon;
use Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable, HasApiTokens;

    public $table = 'users';

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'verified_at',
        'email_verified_at',
    ];

    protected $fillable = [
        'name',
        'email',
        'approved',
        'verified',
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
        'verified_at',
        'remember_token',
        'email_verified_at',
        'verification_token',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::created(function (User $user) {
            if (auth()->check()) {
                $user->verified    = 1;
                $user->verified_at = Carbon::now()->format(config('panel.date_format') . ' ' . config('panel.time_format'));
                $user->save();
            } elseif (!$user->verification_token) {
                $token     = Str::random(64);
                $usedToken = User::where('verification_token', $token)->first();

                while ($usedToken) {
                    $token     = Str::random(64);
                    $usedToken = User::where('verification_token', $token)->first();
                }

                $user->verification_token = $token;
                $user->save();

                $registrationRole = config('panel.registration_default_role');

                if (!$user->roles()->get()->contains($registrationRole)) {
                    $user->roles()->attach($registrationRole);
                }

                $user->notify(new VerifyUserNotification($user));
            }
        });
    }

    public function boards()
    {
        return $this->hasMany(Board::class, 'modified_by_id', 'id');
    }

    public function myClasses()
    {
        return $this->hasMany(MyClass::class, 'modified_by_id', 'id');
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'modified_by_id', 'id');
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class, 'modified_by_id', 'id');
    }

    public function units()
    {
        return $this->hasMany(Unit::class, 'modified_by_id', 'id');
    }

    public function questionTypes()
    {
        return $this->hasMany(QuestionType::class, 'modified_by_id', 'id');
    }

    public function states()
    {
        return $this->hasMany(State::class, 'modified_by_id', 'id');
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'modified_by_id', 'id');
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'modified_by_id', 'id');
    }

    public function languages()
    {
        return $this->hasMany(Language::class, 'modified_by_id', 'id');
    }

    public function questionAnswers()
    {
        return $this->hasMany(QuestionAnswer::class, 'modified_by_id', 'id');
    }

    public function userAlerts()
    {
        return $this->belongsToMany(UserAlert::class);
    }

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setEmailVerifiedAtAttribute($value)
    {
        $this->attributes['email_verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function getVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setVerifiedAtAttribute($value)
    {
        $this->attributes['verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
