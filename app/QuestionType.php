<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionType extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'question_types';

    const TYPE_SELECT = [
        'flash_card' => 'Flash Card',
    ];

    const IS_ACTIVE_RADIO = [
        'yes' => 'Yes',
        'no'  => 'No',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'type',
        'is_active',
        'created_at',
        'updated_at',
        'deleted_at',
        'modified_by_id',
    ];

    public function questionAnswers()
    {
        return $this->hasMany(QuestionAnswer::class, 'question_type_id', 'id');
    }

    public function modified_by()
    {
        return $this->belongsTo(User::class, 'modified_by_id');
    }
}
