<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'cities';

    const IS_ACTIVE_RADIO = [
        'yes' => 'Yes',
        'no'  => 'No',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'code',
        'name',
        'state_id',
        'is_active',
        'country_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'modified_by_id',
    ];

    public function students()
    {
        return $this->hasMany(Student::class, 'city_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function modified_by()
    {
        return $this->belongsTo(User::class, 'modified_by_id');
    }
}
