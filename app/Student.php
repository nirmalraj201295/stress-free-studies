<?php

namespace App;

use App\Traits\Auditable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'students';

    public static $searchable = [
        'name',
    ];

    const IS_ACTIVE_RADIO = [
        'yes' => 'Yes',
        'no'  => 'No',
    ];

    protected $dates = [
        'dob',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'dob',
        'name',
        'email',
        'city_id',
        'board_id',
        'class_id',
        'state_id',
        'studentid',
        'is_active',
        'country_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'modified_by_id',
        'current_address',
    ];

    public function studentStudyUnits()
    {
        return $this->hasMany(StudentStudyUnit::class, 'studentid_id', 'id');
    }

    public function getDobAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function board()
    {
        return $this->belongsTo(Board::class, 'board_id');
    }

    function class()
    {
        return $this->belongsTo(MyClass::class, 'class_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function modified_by()
    {
        return $this->belongsTo(User::class, 'modified_by_id');
    }
}
