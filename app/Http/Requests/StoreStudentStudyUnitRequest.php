<?php

namespace App\Http\Requests;

use App\StudentStudyUnit;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreStudentStudyUnitRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('student_study_unit_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'studentid_id' => [
                'required',
                'integer',
            ],
            'unitid_id'    => [
                'required',
                'integer',
            ],
            'chapterid_id' => [
                'required',
                'integer',
            ],
            'subjectid_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
