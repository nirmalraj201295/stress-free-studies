<?php

namespace App\Http\Requests;

use App\Student;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateStudentRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('student_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'studentid'      => [
                'required',
            ],
            'name'           => [
                'required',
            ],
            'dob'            => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'email'          => [
                'required',
            ],
            'board_id'       => [
                'required',
                'integer',
            ],
            'class_id'       => [
                'required',
                'integer',
            ],
            'is_active'      => [
                'required',
            ],
            'modified_by_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
