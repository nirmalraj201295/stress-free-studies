<?php

namespace App\Http\Requests;

use App\Subject;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreSubjectRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('subject_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'board_id'       => [
                'required',
                'integer',
            ],
            'class_id'       => [
                'required',
                'integer',
            ],
            'code'           => [
                'min:3',
                'max:50',
                'required',
                'unique:subjects',
            ],
            'name'           => [
                'min:4',
                'max:50',
                'required',
            ],
            'status'         => [
                'required',
            ],
            'is_active'      => [
                'required',
            ],
            'modified_by_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
