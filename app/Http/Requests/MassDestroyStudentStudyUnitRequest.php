<?php

namespace App\Http\Requests;

use App\StudentStudyUnit;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyStudentStudyUnitRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('student_study_unit_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:student_study_units,id',
        ];
    }
}
