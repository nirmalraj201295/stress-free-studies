<?php

namespace App\Http\Requests;

use App\MyClass;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateMyClassRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('my_class_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'board_id'       => [
                'required',
                'integer',
            ],
            'code'           => [
                'min:3',
                'max:12',
                'required',
                'unique:my_classes,code,' . request()->route('my_class')->id,
            ],
            'name'           => [
                'min:4',
                'max:50',
                'required',
            ],
            'status'         => [
                'required',
            ],
            'is_active'      => [
                'required',
            ],
            'modified_by_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
