<?php

namespace App\Http\Requests;

use App\QuestionType;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateQuestionTypeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('question_type_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'is_active'      => [
                'required',
            ],
            'modified_by_id' => [
                'required',
                'integer',
            ],
            'type'           => [
                'required',
            ],
        ];
    }
}
