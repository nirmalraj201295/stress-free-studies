<?php

namespace App\Http\Requests;

use App\MyClass;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreMyClassRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('my_class_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'board_id'       => [
                'required',
                'integer',
            ],
            'code'           => [
                'min:3',
                'max:50',
                'required',
                'unique:my_classes',
            ],
            'name'           => [
                'min:4',
                'max:50',
                'required',
            ],
            'status'         => [
                'required',
            ],
            'is_active'      => [
                'required',
            ],
            'modified_by_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
