<?php

namespace App\Http\Requests;

use App\QuestionAnswer;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreQuestionAnswerRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('question_answer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'unit_id'          => [
                'required',
                'integer',
            ],
            'language_id'      => [
                'required',
                'integer',
            ],
            'question_type_id' => [
                'required',
                'integer',
            ],
            'question'         => [
                'required',
            ],
            'answer'           => [
                'required',
            ],
            'sort_order'       => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'status'           => [
                'required',
            ],
            'is_active'        => [
                'required',
            ],
            'modified_by_id'   => [
                'required',
                'integer',
            ],
        ];
    }
}
