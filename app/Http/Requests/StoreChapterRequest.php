<?php

namespace App\Http\Requests;

use App\Chapter;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreChapterRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('chapter_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'board_id'       => [
                'required',
                'integer',
            ],
            'class_id'       => [
                'required',
                'integer',
            ],
            'subject_id'     => [
                'required',
                'integer',
            ],
            'code'           => [
                'min:3',
                'max:50',
                'required',
                'unique:chapters',
            ],
            'name'           => [
                'min:4',
                'max:50',
                'required',
            ],
            'status'         => [
                'required',
            ],
            'is_active'      => [
                'required',
            ],
            'modified_by_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
