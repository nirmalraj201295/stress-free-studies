<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Student;
use App\MyClass;
use App\Board;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showAdminRegisterForm()
    {
        return view('auth.admin.register');
    }

    public function showStudentRegisterForm()
    {
    	$boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelectBoard'), '');
    	
    	$classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelectClass'), '');
    	
    	return view('auth.register', compact('boards', 'classes'));
    }

    protected function createAdmin(Request $request)
    {
        // $this->validator($request->all())->validate();
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        if(isset($user)) {
            // $student = Student::create([
            //     'studentid' =>  $user->id,
            //     'name'  => $user->name,
            //     'email' => $user->email,
            //     // 'dob'   => $request['dob'],
            //     // 'board_id' => $request['dob'],
            //     // 'class_id' => $request['dob'],
            //     'is_active' => 'yes',
            //     'modified_by_id' => 1,
            // ]);
            session()->flash('success', 'You have Successfully Registered, Please check it your email and verify your account.'); 
        }
        
        // return $user;
        return redirect()->intended('admin/login');
    }

    protected function createStudent(Request $request)
    {
        // $this->validator($request->all())->validate();
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        if(isset($user)) {
            $student = Student::create([
                'studentid' =>  $user->id,
                'name'  => $user->name,
                'email' => $user->email,
                // 'dob'   => $request['dob'],
            	'board_id' => $request['board_id'],
            	'class_id' => $request['class_id'],
                'is_active' => 'yes',
                'modified_by_id' => 1,
            ]);
            session()->flash('success', 'You have Successfully Registered, Please check it your email and verify your account.'); 
        }
        
        // return $user;
        return redirect()->intended('login');
    }
}
