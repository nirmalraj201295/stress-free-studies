<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyQuestionAnswerRequest;
use App\Http\Requests\StoreQuestionAnswerRequest;
use App\Http\Requests\UpdateQuestionAnswerRequest;
use App\Language;
use App\QuestionAnswer;
use App\QuestionType;
use App\Unit;
use App\Board;
use App\MyClass;
use App\Subject;
use App\Chapter;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class QuestionAnswerController extends Controller
{
    use MediaUploadingTrait, CsvImportTrait;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = QuestionAnswer::with(['unit', 'language', 'question_type', 'modified_by'])->select(sprintf('%s.*', (new QuestionAnswer)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'question_answer_show';
                $editGate      = 'question_answer_edit';
                $deleteGate    = 'question_answer_delete';
                $crudRoutePart = 'question-answers';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('unit_name', function ($row) {
                return $row->unit ? $row->unit->name : '';
            });

            $table->addColumn('language_name', function ($row) {
                return $row->language ? $row->language->name : '';
            });

            $table->addColumn('question_type_type', function ($row) {
                return $row->question_type ? $row->question_type->type : '';
            });

            $table->editColumn('question', function ($row) {
                return $row->question ? $row->question : "";
            });
            $table->editColumn('option', function ($row) {
                return $row->option ? $row->option : "";
            });
            $table->editColumn('answer', function ($row) {
                return $row->answer ? $row->answer : "";
            });
            $table->editColumn('image', function ($row) {
                if ($photo = $row->image) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('sort_order', function ($row) {
                return $row->sort_order ? $row->sort_order : "";
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? QuestionAnswer::STATUS_SELECT[$row->status] : '';
            });
            $table->editColumn('is_active', function ($row) {
                return $row->is_active ? QuestionAnswer::IS_ACTIVE_RADIO[$row->is_active] : '';
            });
            $table->addColumn('modified_by_name', function ($row) {
                return $row->modified_by ? $row->modified_by->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'unit', 'language', 'question_type', 'image', 'modified_by']);

            return $table->make(true);
        }

        $chapter = Chapter::where('id',$request->id)->first(); 
        $unit = Unit::where('id',$request->uid)->first();
        $questionAnswers = QuestionAnswer::where('unit_id',$request->uid)->orderBy('sort_order','asc')->get();

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjects = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.questionAnswers.index',compact('boards', 'classes', 'subjects','chapter', 'unit', 'questionAnswers'));

        // return view('admin.questionAnswers.index');
    }

    public function sort_order(Request $request) {
        $position = $request->position;
        $i=1;
        foreach($position as $k=>$v){ 
            // $sql = "Update sorting_items SET position_order=".$i." WHERE id=".$v;
            // $mysqli->query($sql);
            if(!empty($v)) {
                QuestionAnswer::where('id',$v)->update(['sort_order' => $i]);
                // DB::statement("Update sorting_items SET position_order=".$i." WHERE id=".$v;
                $i++;
            }
        }
    }

    public function create()
    {
        abort_if(Gate::denies('question_answer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $units = Unit::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $languages = Language::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $question_types = QuestionType::all()->pluck('type', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.questionAnswers.create', compact('units', 'languages', 'question_types', 'modified_bies'));
    }

    public function store(StoreQuestionAnswerRequest $request)
    {
        $questionAnswer = QuestionAnswer::create($request->all());

        if ($request->input('image', false)) {
            $questionAnswer->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }

        return redirect()->route('admin.question-answers.index');
    }

    public function edit(QuestionAnswer $questionAnswer)
    {
        abort_if(Gate::denies('question_answer_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $units = Unit::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $languages = Language::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $question_types = QuestionType::all()->pluck('type', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $questionAnswer->load('unit', 'language', 'question_type', 'modified_by');

        return view('admin.questionAnswers.edit', compact('units', 'languages', 'question_types', 'modified_bies', 'questionAnswer'));
    }

    public function update(UpdateQuestionAnswerRequest $request, QuestionAnswer $questionAnswer)
    {
        $questionAnswer->update($request->all());

        if ($request->input('image', false)) {
            if (!$questionAnswer->image || $request->input('image') !== $questionAnswer->image->file_name) {
                $questionAnswer->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($questionAnswer->image) {
            $questionAnswer->image->delete();
        }

        return redirect()->route('admin.question-answers.index');
    }

    public function show(QuestionAnswer $questionAnswer)
    {
        abort_if(Gate::denies('question_answer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $questionAnswer->load('unit', 'language', 'question_type', 'modified_by');

        return view('admin.questionAnswers.show', compact('questionAnswer'));
    }

    public function destroy(QuestionAnswer $questionAnswer)
    {
        abort_if(Gate::denies('question_answer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $questionAnswer->delete();

        return back();
    }

    public function massDestroy(MassDestroyQuestionAnswerRequest $request)
    {
        QuestionAnswer::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
