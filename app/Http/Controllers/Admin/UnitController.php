<?php

namespace App\Http\Controllers\Admin;

use App\Board;
use App\Chapter;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyUnitRequest;
use App\Http\Requests\StoreUnitRequest;
use App\Http\Requests\UpdateUnitRequest;
use App\MyClass;
use App\Subject;
use App\Unit;
use App\QuestionAnswer;
use App\QuestionType;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class UnitController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Unit::with(['board', 'class', 'subject', 'chapter', 'modified_by'])->select(sprintf('%s.*', (new Unit)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'unit_show';
                $editGate      = 'unit_edit';
                $deleteGate    = 'unit_delete';
                $crudRoutePart = 'units';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('board_name', function ($row) {
                return $row->board ? $row->board->name : '';
            });

            $table->editColumn('board.code', function ($row) {
                return $row->board ? (is_string($row->board) ? $row->board : $row->board->code) : '';
            });
            $table->addColumn('class_name', function ($row) {
                return $row->class ? $row->class->name : '';
            });

            $table->editColumn('class.code', function ($row) {
                return $row->class ? (is_string($row->class) ? $row->class : $row->class->code) : '';
            });
            $table->addColumn('subject_name', function ($row) {
                return $row->subject ? $row->subject->name : '';
            });

            $table->editColumn('subject.code', function ($row) {
                return $row->subject ? (is_string($row->subject) ? $row->subject : $row->subject->code) : '';
            });
            $table->addColumn('chapter_name', function ($row) {
                return $row->chapter ? $row->chapter->name : '';
            });

            $table->editColumn('chapter.code', function ($row) {
                return $row->chapter ? (is_string($row->chapter) ? $row->chapter : $row->chapter->code) : '';
            });
            $table->editColumn('code', function ($row) {
                return $row->code ? $row->code : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description : "";
            });
            $table->editColumn('image', function ($row) {
                if ($photo = $row->image) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? Unit::STATUS_SELECT[$row->status] : '';
            });
            $table->editColumn('is_active', function ($row) {
                return $row->is_active ? Unit::IS_ACTIVE_RADIO[$row->is_active] : '';
            });
            $table->addColumn('modified_by_name', function ($row) {
                return $row->modified_by ? $row->modified_by->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'board', 'class', 'subject', 'chapter', 'image', 'modified_by']);

            return $table->make(true);
        }
        $chapter = Chapter::where('id',$request->id)->first(); 
        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjects = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.units.index',compact('boards', 'classes', 'subjects','chapter'));
    }

    public function ajaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 5;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    $data = Unit::leftJoin('question_answers', function($join){
                        $join->on('question_answers.unit_id', '=', 'units.id')->where('question_answers.deleted_at', '=', NULL);
                    })
                    ->select('units.*', \DB::raw("COUNT(question_answers.unit_id) AS total"))
                    ->where('units.deleted_at','=',NULL)
                    ->groupBy('sfs.units.id')
                    ->where ( 'units.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'units.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy('sort_order','asc')->paginate($page)->setPath ( '' );
                } else {
                    // $data = Chapter::orderBy($sort[0],$sort[1])->paginate($page);
                    $data = Unit::leftJoin('question_answers', function($join){
                        $join->on('question_answers.unit_id', '=', 'units.id')->where('question_answers.deleted_at', '=', NULL);
                    })
                    ->leftjoin('users', 'units.modified_by_id', '=', 'users.id')
                    ->select('units.*', \DB::raw("COUNT(question_answers.unit_id) AS total"),'users.name as username')
                    ->where('units.deleted_at','=',NULL)
                    ->groupBy('sfs.units.id')
                    ->orderBy('sort_order','asc')->paginate($page);
                }

                // $data = Unit::orderBy($sort[0],$sort[1])->paginate($page);//where('chapters.deleted_at','=',NULL);

                $boards = Board::all()->pluck('name', 'id');
                $data->boards = $boards;

                $classes = MyClass::all()->pluck('name', 'id');
                $data->classes = $classes;

                $subjects = Subject::all()->pluck('name', 'id');
                $data->subjects = $subjects;
                if($request->sv == "list")
                    return view('cdesign.card1', compact('data'));
                else if($request->sv == "ulist")
                    return view('cdesign.card_unitlist', compact('data'));

                return view('cdesign.card', compact('data'));
            }
        return false;
    }

    public function get_by_chapters(Request $request)
    {   
        if (!$request->subject_id) {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
        } else {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
            $chapters = Chapter::where('subject_id', $request->subject_id)->get();
            foreach ($chapters as $chapter) {
                $html .= '<option value="'.$chapter->id.'">'.$chapter->name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function create(Request $request)
    {
        abort_if(Gate::denies('unit_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // print_r($request->id);
        // $data = Unit::where('id',$request->id)->first();
        // print_r($data);die;
        // if(!empty($data)){
            $chapter = Chapter::where('id',$request->id)->first(); 
        // }
            
        // $data = Chapter::leftjoin('units', 'units.chapter_id', '=', 'chapters.id')
        //             ->select('chapters.*', \DB::raw("COUNT(units.chapter_id) AS total"))
        //             ->where('chapters.deleted_at','=',NULL)
        //             ->groupBy('sfs.chapters.id');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjects = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $chapters = Chapter::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.units.create', compact('boards', 'classes', 'subjects', 'chapters', 'modified_bies','chapter'));
    }

    public function store(Request $request)
    {
        // $unit = Unit::create($request->all());
        $unit = Unit::updateOrCreate(['code' => $request->code],$request->all());
        $question = $request->question;
        $answer = $request->answer;
        $sort_order = $request->qa_sort_order;
        $count_question = count($question);
        for($count = 0; $count < $count_question; $count++)
        {
        $data = array(
            'question' => $question[$count],
            'answer'  => $answer[$count],
            'unit_id' => $unit->id,
            'language_id' => 1,
            'question_type_id' => 1,
            'sort_order' => $sort_order[$count],
            'status' => $request->status,
            'is_active' => $request->is_active,
            'modified_by_id' => $request->modified_by_id,
        );
        $insert_data[] = $data; 
        if(isset($request->qa_id[$count]))
            QuestionAnswer::updateOrCreate(['id' => $request->qa_id[$count]],$data);
        else
            QuestionAnswer::create($data);
        }

        // QuestionAnswer::insert($insert_data);

        if ($request->input('image', false)) {
            $unit->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }
        return response()->json(["status" => true, "message" => "Created successfully.", "unit_id" => $unit->id]);
        // return redirect()->route('admin.units.index');
    }

    public function sort_order(Request $request) {
        $position = $request->position;
        $i=1;
        foreach($position as $k=>$v){
            // $sql = "Update sorting_items SET position_order=".$i." WHERE id=".$v;
            // $mysqli->query($sql);
            Unit::where('id',$v)->update(['sort_order' => $i]);
            // DB::statement("Update sorting_items SET position_order=".$i." WHERE id=".$v;
            $i++;
        }
    }

    public function edit(Unit $unit)
    {
        abort_if(Gate::denies('unit_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjects = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $chapters = Chapter::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $unit->load('board', 'class', 'subject', 'chapter', 'modified_by');

        return view('admin.units.edit', compact('boards', 'classes', 'subjects', 'chapters', 'modified_bies', 'unit'));
    }

    public function update(UpdateUnitRequest $request, Unit $unit)
    {
        $unit->update($request->all());

        if ($request->input('image', false)) {
            if (!$unit->image || $request->input('image') !== $unit->image->file_name) {
                $unit->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($unit->image) {
            $unit->image->delete();
        }

        // return redirect()->route('admin.units.index');

        return response()->json(["status" => true, "message" => "Updated successfully."]);
    }

    public function show(Unit $unit)
    {
        abort_if(Gate::denies('unit_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $unit->load('board', 'class', 'subject', 'chapter', 'modified_by');

        return view('admin.units.show', compact('unit'));
    }

    public function destroy(Unit $unit)
    {
        abort_if(Gate::denies('unit_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $unit->delete();

        return back();
    }

    public function massDestroy(MassDestroyUnitRequest $request)
    {
        Unit::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
