<?php

namespace App\Http\Controllers\Admin;

use App\Chapter;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyStudentStudyUnitRequest;
use App\Http\Requests\StoreStudentStudyUnitRequest;
use App\Http\Requests\UpdateStudentStudyUnitRequest;
use App\Student;
use App\StudentStudyUnit;
use App\Subject;
use App\Unit;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class StudentStudyUnitsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = StudentStudyUnit::with(['studentid', 'unitid', 'chapterid', 'subjectid'])->select(sprintf('%s.*', (new StudentStudyUnit)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'student_study_unit_show';
                $editGate      = 'student_study_unit_edit';
                $deleteGate    = 'student_study_unit_delete';
                $crudRoutePart = 'student-study-units';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('studentid_studentid', function ($row) {
                return $row->studentid ? $row->studentid->studentid : '';
            });

            $table->addColumn('unitid_name', function ($row) {
                return $row->unitid ? $row->unitid->name : '';
            });

            $table->addColumn('chapterid_name', function ($row) {
                return $row->chapterid ? $row->chapterid->name : '';
            });

            $table->addColumn('subjectid_name', function ($row) {
                return $row->subjectid ? $row->subjectid->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'studentid', 'unitid', 'chapterid', 'subjectid']);

            return $table->make(true);
        }

        return view('admin.studentStudyUnits.index');
    }

    public function create()
    {
        abort_if(Gate::denies('student_study_unit_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studentids = Student::all()->pluck('studentid', 'id')->prepend(trans('global.pleaseSelect'), '');

        $unitids = Unit::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $chapterids = Chapter::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjectids = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.studentStudyUnits.create', compact('studentids', 'unitids', 'chapterids', 'subjectids'));
    }

    public function store(StoreStudentStudyUnitRequest $request)
    {
        $studentStudyUnit = StudentStudyUnit::create($request->all());

        return redirect()->route('admin.student-study-units.index');
    }

    public function edit(StudentStudyUnit $studentStudyUnit)
    {
        abort_if(Gate::denies('student_study_unit_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studentids = Student::all()->pluck('studentid', 'id')->prepend(trans('global.pleaseSelect'), '');

        $unitids = Unit::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $chapterids = Chapter::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjectids = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $studentStudyUnit->load('studentid', 'unitid', 'chapterid', 'subjectid');

        return view('admin.studentStudyUnits.edit', compact('studentids', 'unitids', 'chapterids', 'subjectids', 'studentStudyUnit'));
    }

    public function update(UpdateStudentStudyUnitRequest $request, StudentStudyUnit $studentStudyUnit)
    {
        $studentStudyUnit->update($request->all());

        return redirect()->route('admin.student-study-units.index');
    }

    public function show(StudentStudyUnit $studentStudyUnit)
    {
        abort_if(Gate::denies('student_study_unit_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studentStudyUnit->load('studentid', 'unitid', 'chapterid', 'subjectid');

        return view('admin.studentStudyUnits.show', compact('studentStudyUnit'));
    }

    public function destroy(StudentStudyUnit $studentStudyUnit)
    {
        abort_if(Gate::denies('student_study_unit_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studentStudyUnit->delete();

        return back();
    }

    public function massDestroy(MassDestroyStudentStudyUnitRequest $request)
    {
        StudentStudyUnit::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
