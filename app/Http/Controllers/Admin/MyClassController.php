<?php

namespace App\Http\Controllers\Admin;

use App\Board;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyMyClassRequest;
use App\Http\Requests\StoreMyClassRequest;
use App\Http\Requests\UpdateMyClassRequest;
use App\MyClass;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class MyClassController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        if ($request->ajax()) {
            $query = MyClass::with(['board', 'modified_by'])->select(sprintf('%s.*', (new MyClass)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'my_class_show';
                $editGate      = 'my_class_edit';
                $deleteGate    = 'my_class_delete';
                $crudRoutePart = 'my-classes';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('board_name', function ($row) {
                return $row->board ? $row->board->name : '';
            });

            $table->editColumn('board.code', function ($row) {
                return $row->board ? (is_string($row->board) ? $row->board : $row->board->code) : '';
            });
            $table->editColumn('code', function ($row) {
                return $row->code ? $row->code : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description : "";
            });
            $table->editColumn('image', function ($row) {
                if ($photo = $row->image) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? MyClass::STATUS_SELECT[$row->status] : '';
            });
            $table->editColumn('is_active', function ($row) {
                return $row->is_active ? MyClass::IS_ACTIVE_RADIO[$row->is_active] : '';
            });
            $table->addColumn('modified_by_name', function ($row) {
                return $row->modified_by ? $row->modified_by->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'board', 'image', 'modified_by']);

            return $table->make(true);
        }

        return view('admin.myClasses.index', compact('boards'));
    }

    public function ajaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 5;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    $data = MyClass::leftJoin('subjects', function($join){
                        $join->on('subjects.class_id', '=', 'my_classes.id')->where('subjects.deleted_at', '=', NULL);
                    })
                    ->select('my_classes.*', \DB::raw("COUNT(subjects.board_id) AS total"))
                    ->where('my_classes.deleted_at','=',NULL)
                    ->groupBy('sfs.my_classes.id')
                    ->where ( 'my_classes.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'my_classes.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );
                } elseif(isset($request->filterData) && !empty($request->filterData)) {
                      $data = MyClass::leftJoin('subjects', function($join){
                        $join->on('subjects.class_id', '=', 'my_classes.id')->where('subjects.deleted_at', '=', NULL);
                    })
                    ->select('my_classes.*', \DB::raw("COUNT(subjects.board_id) AS total"))
                    ->where('my_classes.deleted_at','=',NULL)
                    ->where("my_classes.board_id","=",$request->filterData,'AND')
                    ->groupBy('sfs.my_classes.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
                } else {
                     $data = MyClass::leftJoin('subjects', function($join){
                        $join->on('subjects.class_id', '=', 'my_classes.id')->where('subjects.deleted_at', '=', NULL);
                    })
                    ->select('my_classes.*', \DB::raw("COUNT(subjects.board_id) AS total"))
                    ->where('my_classes.deleted_at','=',NULL)
                    ->groupBy('sfs.my_classes.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
                }
                $boards = Board::all()->pluck('name', 'id');
                if(!empty($data))
                $data->boards = $boards;
                    //   print_r($boards);die;
                return view('cdesign.card', compact('data'));
            }
        return false;
    }

    public function create()
    {
        abort_if(Gate::denies('my_class_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.myClasses.create', compact('boards', 'modified_bies'));
    }

    public function store(StoreMyClassRequest $request)
    {
        $myClass = MyClass::create($request->all());

        if ($request->input('image', false)) {
            $myClass->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }
        return response()->json(["status" => true, "message" => "Created successfully."]);
        // return redirect()->route('admin.my-classes.index');
    }

    public function edit(MyClass $myClass)
    {
        abort_if(Gate::denies('my_class_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $myClass->load('board', 'modified_by');

        return $myClass;

        // return view('admin.myClasses.edit', compact('boards', 'modified_bies', 'myClass'));
    }

    public function update(UpdateMyClassRequest $request, MyClass $myClass)
    {
        $myClass->update($request->all());

        if ($request->input('image', false)) {
            if (!$myClass->image || $request->input('image') !== $myClass->image->file_name) {
                $myClass->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($myClass->image) {
            $myClass->image->delete();
        }

        return response()->json(["status" => true, "message" => "Updated successfully."]);
        // return redirect()->route('admin.my-classes.index');
    }

    public function show(MyClass $myClass)
    {
        abort_if(Gate::denies('my_class_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $myClass->load('board', 'modified_by');

        return view('admin.myClasses.show', compact('myClass'));
    }

    public function destroy(MyClass $myClass)
    {
        abort_if(Gate::denies('my_class_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $myClass->delete();

        return back();
    }

    public function massDestroy(MassDestroyMyClassRequest $request)
    {
        MyClass::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
