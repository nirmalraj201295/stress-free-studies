<?php

namespace App\Http\Controllers\Admin;

use App\Board;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroySubjectRequest;
use App\Http\Requests\StoreSubjectRequest;
use App\Http\Requests\UpdateSubjectRequest;
use App\MyClass;
use App\Subject;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class SubjectController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Subject::with(['board', 'class', 'modified_by'])->select(sprintf('%s.*', (new Subject)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'subject_show';
                $editGate      = 'subject_edit';
                $deleteGate    = 'subject_delete';
                $crudRoutePart = 'subjects';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('board_name', function ($row) {
                return $row->board ? $row->board->name : '';
            });

            $table->editColumn('board.code', function ($row) {
                return $row->board ? (is_string($row->board) ? $row->board : $row->board->code) : '';
            });
            $table->addColumn('class_name', function ($row) {
                return $row->class ? $row->class->name : '';
            });

            $table->editColumn('class.code', function ($row) {
                return $row->class ? (is_string($row->class) ? $row->class : $row->class->code) : '';
            });
            $table->editColumn('code', function ($row) {
                return $row->code ? $row->code : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description : "";
            });
            $table->editColumn('image', function ($row) {
                if ($photo = $row->image) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? Subject::STATUS_SELECT[$row->status] : '';
            });
            $table->editColumn('is_active', function ($row) {
                return $row->is_active ? Subject::IS_ACTIVE_RADIO[$row->is_active] : '';
            });
            $table->addColumn('modified_by_name', function ($row) {
                return $row->modified_by ? $row->modified_by->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'board', 'class', 'image', 'modified_by']);

            return $table->make(true);
        }
        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.subjects.index', compact('boards', 'classes'));
    }

    public function ajaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 5;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    // $data = Subject::where ( 'name', 'LIKE', '%' . $q . '%' )->orWhere ( 'code', 'LIKE', '%' . $q . '%' )
                    // ->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );
                    $data = Subject::leftJoin('chapters', function($join){
                        $join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
                    })
                    ->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
                    ->where('subjects.deleted_at','=',NULL)
                    ->groupBy('sfs.subjects.id')
                    ->where ( 'subjects.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'subjects.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );
                } elseif(isset($request->filterData) && !empty($request->filterData)) {
                    $filterData = explode('_', $request->filterData);
                    $data = Subject::query();
                    $data = $data->leftJoin('chapters', function($join){
                        $join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
                    })
                    ->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
                    ->where('subjects.deleted_at','=',NULL)
                    ->where("subjects.board_id","=",$filterData[0],'AND')
                    ->groupBy('sfs.subjects.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
              } else {
                    // $data = Subject::orderBy($sort[0],$sort[1])->paginate($page);
                    // $data = Subject::leftjoin('chapters', 'chapters.subject_id', '=', 'subjects.id')
                    $data = Subject::leftJoin('chapters', function($join){
                        $join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
                    })
                    ->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
                    ->where('subjects.deleted_at','=',NULL)
                    ->groupBy('sfs.subjects.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
                }
                // print_r($data);die;
                $boards = Board::all()->pluck('name', 'id');
                $data->boards = $boards;

                $classes = MyClass::all()->pluck('name', 'id');
                $data->classes = $classes;

                
                    
                return view('cdesign.card', compact('data'));
            }
        return false;
    }

    public function get_by_myclasses(Request $request)
    {   
        if (!$request->board_id || $request->board_id == 'undefined') {
            $html = '<option value="">'.trans('global.pleaseSelectClass').'</option>';
            $myclasses = MyClass::get();
            foreach ($myclasses as $myclass) {
                $html .= '<option value="'.$myclass->id.'">'.$myclass->name.'</option>';
            }
        } else {
            $html = '<option value="">'.trans('global.pleaseSelectClass').'</option>';
            $myclasses = MyClass::where('board_id', $request->board_id)->get();
            foreach ($myclasses as $myclass) {
                $html .= '<option value="'.$myclass->id.'">'.$myclass->name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function create()
    {
        abort_if(Gate::denies('subject_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.subjects.create', compact('boards', 'classes', 'modified_bies'));
    }

    public function store(StoreSubjectRequest $request)
    {
        $subject = Subject::create($request->all());

        if ($request->input('image', false)) {
            $subject->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }
        return response()->json(["status" => true, "message" => "Created successfully."]);
        // return redirect()->route('admin.subjects.index');
    }

    public function edit(Subject $subject)
    {
        abort_if(Gate::denies('subject_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subject->load('board', 'class', 'modified_by');

        return $subject;

        // return view('admin.subjects.edit', compact('boards', 'classes', 'modified_bies', 'subject'));
    }

    public function update(UpdateSubjectRequest $request, Subject $subject)
    {
        $subject->update($request->all());

        if ($request->input('image', false)) {
            if (!$subject->image || $request->input('image') !== $subject->image->file_name) {
                $subject->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($subject->image) {
            $subject->image->delete();
        }

        // return redirect()->route('admin.subjects.index');
        return response()->json(["status" => true, "message" => "Updated successfully."]);
    }

    public function show(Subject $subject)
    {
        abort_if(Gate::denies('subject_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $subject->load('board', 'class', 'modified_by');

        return view('admin.subjects.show', compact('subject'));
    }

    public function destroy(Subject $subject)
    {
        abort_if(Gate::denies('subject_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $subject->delete();

        return back();
    }

    public function massDestroy(MassDestroySubjectRequest $request)
    {
        Subject::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
