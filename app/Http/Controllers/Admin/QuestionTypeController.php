<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyQuestionTypeRequest;
use App\Http\Requests\StoreQuestionTypeRequest;
use App\Http\Requests\UpdateQuestionTypeRequest;
use App\QuestionType;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class QuestionTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = QuestionType::with(['modified_by'])->select(sprintf('%s.*', (new QuestionType)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'question_type_show';
                $editGate      = 'question_type_edit';
                $deleteGate    = 'question_type_delete';
                $crudRoutePart = 'question-types';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('is_active', function ($row) {
                return $row->is_active ? QuestionType::IS_ACTIVE_RADIO[$row->is_active] : '';
            });
            $table->addColumn('modified_by_name', function ($row) {
                return $row->modified_by ? $row->modified_by->name : '';
            });

            $table->editColumn('type', function ($row) {
                return $row->type ? QuestionType::TYPE_SELECT[$row->type] : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'modified_by']);

            return $table->make(true);
        }

        return view('admin.questionTypes.index');
    }

    public function create()
    {
        abort_if(Gate::denies('question_type_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.questionTypes.create', compact('modified_bies'));
    }

    public function store(StoreQuestionTypeRequest $request)
    {
        $questionType = QuestionType::create($request->all());

        return redirect()->route('admin.question-types.index');
    }

    public function edit(QuestionType $questionType)
    {
        abort_if(Gate::denies('question_type_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $questionType->load('modified_by');

        return view('admin.questionTypes.edit', compact('modified_bies', 'questionType'));
    }

    public function update(UpdateQuestionTypeRequest $request, QuestionType $questionType)
    {
        $questionType->update($request->all());

        return redirect()->route('admin.question-types.index');
    }

    public function show(QuestionType $questionType)
    {
        abort_if(Gate::denies('question_type_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $questionType->load('modified_by');

        return view('admin.questionTypes.show', compact('questionType'));
    }

    public function destroy(QuestionType $questionType)
    {
        abort_if(Gate::denies('question_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $questionType->delete();

        return back();
    }

    public function massDestroy(MassDestroyQuestionTypeRequest $request)
    {
        QuestionType::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
