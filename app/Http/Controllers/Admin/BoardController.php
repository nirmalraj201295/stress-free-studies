<?php

namespace App\Http\Controllers\Admin;

use App\Board;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyBoardRequest;
use App\Http\Requests\StoreBoardRequest;
use App\Http\Requests\UpdateBoardRequest;
use App\User;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class BoardController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        if ($request->ajax()) {
            $query = Board::with(['modified_by'])->select(sprintf('%s.*', (new Board)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'board_show';
                $editGate      = 'board_edit';
                $deleteGate    = 'board_delete';
                $crudRoutePart = 'boards';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('code', function ($row) {
                return $row->code ? $row->code : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description : "";
            });
            $table->editColumn('image', function ($row) {
                if ($photo = $row->image) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? Board::STATUS_SELECT[$row->status] : '';
            });
            $table->editColumn('is_active', function ($row) {
                return $row->is_active ? Board::IS_ACTIVE_RADIO[$row->is_active] : '';
            });
            $table->addColumn('modified_by_name', function ($row) {
                return $row->modified_by ? $row->modified_by->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'image', 'modified_by']);

            return $table->make(true);
        }

        return view('admin.boards.index', compact('boards'));
    }

    public function ajaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 5;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    $data = Board::leftJoin('my_classes', function($join){
                        $join->on('my_classes.board_id', '=', 'boards.id')->where('my_classes.deleted_at', '=', NULL);
                    })
                    ->select('boards.*', \DB::raw("COUNT(my_classes.board_id) AS total"))
                    ->where('boards.deleted_at','=',NULL)
                    ->groupBy('sfs.boards.id')->where ( 'boards.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'boards.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );

                } else {
                    // $data = Board::leftjoin('my_classes', 'my_classes.board_id', '=', 'boards.id')
                    $data = Board::leftJoin('my_classes', function($join){
                        $join->on('my_classes.board_id', '=', 'boards.id')->where('my_classes.deleted_at', '=', NULL);
                    })
                    ->select('boards.*', \DB::raw("COUNT(my_classes.board_id) AS total"))
                    ->where('boards.deleted_at','=',NULL)
                    ->groupBy('sfs.boards.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
                }
                    // $data->cntrl = "Boards";
                    // print_r($data[0]->table);die;
                return view('cdesign.card', compact('data'));
            }
        return false;
    }

    public function get_boards()
    {   
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
            $boards = Board::all()->pluck('name', 'id');
            
            foreach ($boards as $id => $board) {
                $html .= '<option value="'.$id.'">'.$board.'</option>';
            }
       
        return response()->json(['html' => $html]);
    }

    public function create()
    {
        abort_if(Gate::denies('board_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.boards.create', compact('modified_bies'));
    }

    public function store(StoreBoardRequest $request)
    {
        $user = Auth::user();
        $board = Board::create(array_merge($request->all(), ['modified_by_id' => $user->id]));
        if ($request->input('image', false)) {
            $board->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }
        return response()->json(["status" => true, "message" => "Created successfully."]);
        // return redirect()->route('admin.boards.index');
    }

    public function edit(Board $board)
    {
        abort_if(Gate::denies('board_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $board->load('modified_by');

        return $board;
        // return view('admin.boards.edit', compact('modified_bies', 'board'));
    }

    public function update(UpdateBoardRequest $request, Board $board)
    {
        $board->update($request->all());
        if ($request->input('image', false)) {
            if (!$board->image || $request->input('image') !== $board->image->file_name) {
                $board->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($board->image) {
            $board->image->delete();
        }
        // return redirect()->route('admin.boards.index');
        return response()->json(["status" => true, "message" => "Updated successfully."]);
    }

    public function show(Board $board)
    {
        abort_if(Gate::denies('board_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $board->load('modified_by');

        return view('admin.boards.show', compact('board'));
    }

    public function destroy(Board $board)
    {
        abort_if(Gate::denies('board_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $board->delete();

        return back();
    }

    public function massDestroy(MassDestroyBoardRequest $request)
    {
        Board::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
