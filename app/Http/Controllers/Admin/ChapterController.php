<?php

namespace App\Http\Controllers\Admin;

use App\Board;
use App\Chapter;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyChapterRequest;
use App\Http\Requests\StoreChapterRequest;
use App\Http\Requests\UpdateChapterRequest;
use App\MyClass;
use App\Subject;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ChapterController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Chapter::with(['board', 'class', 'subject', 'modified_by'])->select(sprintf('%s.*', (new Chapter)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'chapter_show';
                $editGate      = 'chapter_edit';
                $deleteGate    = 'chapter_delete';
                $crudRoutePart = 'chapters';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('board_name', function ($row) {
                return $row->board ? $row->board->name : '';
            });

            $table->editColumn('board.code', function ($row) {
                return $row->board ? (is_string($row->board) ? $row->board : $row->board->code) : '';
            });
            $table->addColumn('class_name', function ($row) {
                return $row->class ? $row->class->name : '';
            });

            $table->editColumn('class.code', function ($row) {
                return $row->class ? (is_string($row->class) ? $row->class : $row->class->code) : '';
            });
            $table->addColumn('subject_name', function ($row) {
                return $row->subject ? $row->subject->name : '';
            });

            $table->editColumn('subject.code', function ($row) {
                return $row->subject ? (is_string($row->subject) ? $row->subject : $row->subject->code) : '';
            });
            $table->editColumn('code', function ($row) {
                return $row->code ? $row->code : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description : "";
            });
            $table->editColumn('image', function ($row) {
                if ($photo = $row->image) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? Chapter::STATUS_SELECT[$row->status] : '';
            });
            $table->editColumn('is_active', function ($row) {
                return $row->is_active ? Chapter::IS_ACTIVE_RADIO[$row->is_active] : '';
            });
            $table->addColumn('modified_by_name', function ($row) {
                return $row->modified_by ? $row->modified_by->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'board', 'class', 'subject', 'image', 'modified_by']);

            return $table->make(true);
        }
        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjects = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.chapters.index', compact('boards', 'classes', 'subjects'));
    }

    public function ajaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 5;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    $data = Chapter::leftJoin('units', function($join){
                        $join->on('units.chapter_id', '=', 'chapters.id')->where('units.deleted_at', '=', NULL);
                    })
                    ->select('chapters.*', \DB::raw("COUNT(units.chapter_id) AS total"))
                    ->where('chapters.deleted_at','=',NULL)
                    ->groupBy('sfs.chapters.id')
                    ->where ( 'chapters.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'chapters.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );
                } else {
                    // $data = Chapter::orderBy($sort[0],$sort[1])->paginate($page);
                    $data = Chapter::leftJoin('units', function($join){
                        $join->on('units.chapter_id', '=', 'chapters.id')->where('units.deleted_at', '=', NULL);
                    })
                    ->select('chapters.*', \DB::raw("COUNT(units.chapter_id) AS total"))
                    ->where('chapters.deleted_at','=',NULL)
                    ->groupBy('sfs.chapters.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
                }

                $boards = Board::all()->pluck('name', 'id');
                $data->boards = $boards;

                $classes = MyClass::all()->pluck('name', 'id');
                $data->classes = $classes;

                $subjects = Subject::all()->pluck('name', 'id');
                $data->subjects = $subjects;
                if($request->sv == "clist")
                    return view('cdesign.card1', compact('data'));
                    
                return view('cdesign.card', compact('data'));
            }
        return false;
    }

    public function get_by_subjects(Request $request)
    {   
        if (!$request->class_id || $request->class_id == 'undefined') {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
            $subjects = Subject::get();
            foreach ($subjects as $subject) {
                $html .= '<option value="'.$subject->id.'">'.$subject->name.'</option>';
            }
        } else {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
            $subjects = Subject::where('class_id', $request->class_id)->get();
            foreach ($subjects as $subject) {
                $html .= '<option value="'.$subject->id.'">'.$subject->name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function chapter_list(){
        $mydata = "";
        return view('admin.chapters.list', compact('mydata'));
    }

    public function create()
    {
        abort_if(Gate::denies('chapter_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjects = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.chapters.create', compact('boards', 'classes', 'subjects', 'modified_bies'));
    }

    public function store(StoreChapterRequest $request)
    {
        $chapter = Chapter::create($request->all());

        if ($request->input('image', false)) {
            $chapter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }
        return response()->json(["status" => true, "message" => "Created successfully."]);
        // return redirect()->route('admin.chapters.index');
    }

    public function edit(Chapter $chapter)
    {
        abort_if(Gate::denies('chapter_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boards = Board::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $classes = MyClass::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $subjects = Subject::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $modified_bies = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $chapter->load('board', 'class', 'subject', 'modified_by');

        return $chapter;
        // return view('admin.chapters.edit', compact('boards', 'classes', 'subjects', 'modified_bies', 'chapter'));
    }

    public function update(UpdateChapterRequest $request, Chapter $chapter)
    {
        $chapter->update($request->all());

        if ($request->input('image', false)) {
            if (!$chapter->image || $request->input('image') !== $chapter->image->file_name) {
                $chapter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($chapter->image) {
            $chapter->image->delete();
        }


        return response()->json(["status" => true, "message" => "Updated successfully."]);
        // return redirect()->route('admin.chapters.index');
    }

    public function show(Chapter $chapter)
    {
        abort_if(Gate::denies('chapter_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chapter->load('board', 'class', 'subject', 'modified_by');

        return view('admin.chapters.show', compact('chapter'));
    }

    public function destroy(Chapter $chapter)
    {
        abort_if(Gate::denies('chapter_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chapter->delete();

        return back();
    }

    public function massDestroy(MassDestroyChapterRequest $request)
    {
        Chapter::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
