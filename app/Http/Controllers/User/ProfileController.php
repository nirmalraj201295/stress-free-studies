<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use DB;
use Auth;
use App\Board;
use App\MyClass;
use App\Subject;
use App\Chapter;
use App\Unit;
use App\QuestionAnswer;
use App\StudentUnitAccessLog;
use App\StudentCourse;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    public function index()
    {
    	$user = auth()->user();
    	$userId = $user['id'];
    	$data = User::leftJoin('students', function($join){
    		$join->on('students.studentid', '=', 'users.id')->where('students.deleted_at', '=', NULL);
    	})
    	->leftJoin('boards', 'students.board_id', '=', 'boards.id')
    	->leftJoin('my_classes', 'students.class_id', '=', 'my_classes.id')
    	->leftJoin('states', 'students.state_id', '=', 'states.id')
    	->leftJoin('countries', 'students.country_id', '=', 'countries.id')
    	->select('students.name AS studentName','students.email AS studentEmail','boards.name AS boardName','my_classes.name AS className')
    	->where('users.id','=',$userId)->get();
    	
    	return view('user.profile',compact('data'));
    }
    
    public function changePassword(Request $request)
    {
    	$user = new User();
    	$user->update(['password' =>Hash::make($request['password'])]);
    	
    	return true;
    }
}