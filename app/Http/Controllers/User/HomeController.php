<?php

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyChapterRequest;
use App\Http\Requests\StoreChapterRequest;
use App\Http\Requests\UpdateChapterRequest;
use DB;
use Auth;
use App\Board;
use App\MyClass;
use App\Subject;
use App\Chapter;
use App\Unit;
use App\QuestionAnswer;
use App\StudentUnitAccessLog;
use App\StudentCourse;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    public function index()
    {
        return view('user.home');
    }
    
    public function mycourse()
    {
    	return view('user.mycourse');
    }

    public function chaptersIndex(Request $request)
    {
    	$subject= DB::select ( DB::raw ( "SELECT name from subjects where id=$request->id"));
        $data['id'] = $request->id;
        return view('user.chapters',compact('data','subject'));
    }

    public function unitsIndex(Request $request)
    {
    	$chapters= DB::select ( DB::raw ( "SELECT name, subject_id from chapters where id=$request->id"));
        $data['id'] = $request->id;
        return view('user.units',compact('data','chapters'));
    }

    public function QAIndex(Request $request)
    {
    	$units= DB::select ( DB::raw ( "SELECT name, chapter_id from units where id=$request->id"));
    	$user = auth()->user();
    	$userId = $user['id'];
        $data['id'] = $request->id;
        $student = DB::select ( DB::raw ( "SELECT id from students where studentid=$userId"));
        $studentId = $student[0]->id;
        $qLogDatas = DB::select ( DB::raw ( "SELECT session_studyagain,session_gotit from student_unit_access_log where unit_id=$request->id and student_id=$studentId"));
        $consolidatedArrays = array();
        foreach($qLogDatas as $qLogData){
        	$studyagain = $qLogData->session_studyagain;
        	$gotit = $qLogData->session_gotit;
        	$studyagainArrays = explode(",",$studyagain);
        	$gotitArrays = explode(",",$gotit);
        	if($studyagainArrays[0]){
        		foreach($studyagainArrays as $studyagainArray){
        			if(array_key_exists($studyagainArray,$consolidatedArrays)){
        				$consolidatedArrays[$studyagainArray] = $consolidatedArrays[$studyagainArray]-1;
        			}else{
        				$consolidatedArrays[$studyagainArray] = -1;
        			}
        		}
        	}
        	if($gotitArrays[0]){
        		foreach($gotitArrays as $gotitArray){
        			if(array_key_exists($gotitArray,$consolidatedArrays)){
        				$consolidatedArrays[$gotitArray] = $consolidatedArrays[$gotitArray]+1;
        			}else{
        				$consolidatedArrays[$gotitArray] = 1;
        			}
        		}
        	}
        }
        $qaDataArray = array();
        $master =0;$familiar =0; $notfamiliar =0;
        $qaDatas = DB::select ( DB::raw ( "SELECT id from question_answers where unit_id=$request->id"));
        foreach ($qaDatas as $qaData){
        	//array_push($qaDataArray,$qaData->id);
        	if(array_key_exists($qaData->id,$consolidatedArrays)){
        		if($consolidatedArrays[$qaData->id]>0){
        			$master++;
        		}elseif($consolidatedArrays[$qaData->id]==0){
        			$familiar++;
        		}elseif($consolidatedArrays[$qaData->id]<=0){
        			$notfamiliar++;
        		}
        	}else{
        		$notfamiliar++;
        	}
        	
        }

        return view('user.qa',compact('data','master','familiar','notfamiliar','units'));
    }

    public function subjectAjaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 5;
        $user = auth()->user();
        $userId = $user['id'];
        $student = DB::select ( DB::raw ( "SELECT id from students where studentid=$userId"));
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    $data = Subject::leftJoin('chapters', function($join){
                        $join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
                    })
                    ->join('students', 'students.class_id', '=', 'subjects.class_id')
                    ->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
                    ->where('subjects.deleted_at','=',NULL)
                    ->where('students.studentid','=',$userId)
                    ->groupBy('sfs.subjects.id')
                    ->where ( 'subjects.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'subjects.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );
                } elseif(isset($request->filterData) && !empty($request->filterData)) {
                    $filterData = explode('_', $request->filterData);
                    $data = Subject::query();
                    $data = $data->leftJoin('chapters', function($join){
                        $join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
                    })
                    ->join('students', 'students.class_id', '=', 'subjects.class_id')
                    ->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
                    ->where('subjects.deleted_at','=',NULL)
                    ->where('students.studentid','=',$userId)
                    ->where("subjects.board_id","=",$filterData[0],'AND')
                    ->groupBy('sfs.subjects.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
              } else {
                    $data = Subject::leftJoin('chapters', function($join){
                        $join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
                    })
                    
                    ->join('students', 'students.class_id', '=', 'subjects.class_id')
                    ->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
                    ->where('subjects.deleted_at','=',NULL)
                    ->where('students.studentid','=',$userId)
                    ->groupBy('sfs.subjects.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
                }
                $boards = Board::all()->pluck('name', 'id');
                $data->boards = $boards;

                $classes = MyClass::all()->pluck('name', 'id');
                $data->classes = $classes;
                $studentId = $student[0]->id;
                $studentscourses = DB::select ( DB::raw ( "SELECT subject_id from student_courses where student_id=$studentId"));
                $joinSubject = array();
                foreach($studentscourses as $studentscourse){
                	array_push($joinSubject,$studentscourse->subject_id);
                }
                    
                return view('cdesign.user_card', compact('data','joinSubject'));
            }
        return false;
    }

    public function chapterAjaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 5;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    $data = Chapter::leftJoin('units', function($join){
                        $join->on('units.chapter_id', '=', 'chapters.id')->where('units.deleted_at', '=', NULL);
                    })
                    // $data = Chapter::leftjoin('units', 'units.chapter_id', '=', 'chapters.id')
                    ->select('chapters.*', \DB::raw("COUNT(units.chapter_id) AS total"))
                    ->where('chapters.deleted_at','=',NULL)
                    ->groupBy('sfs.chapters.id')
                    ->where ( 'chapters.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'chapters.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );
                } else {
                    // $data = Chapter::orderBy($sort[0],$sort[1])->paginate($page);
                    $data = Chapter::leftJoin('units', function($join){
                        $join->on('units.chapter_id', '=', 'chapters.id')->where('units.deleted_at', '=', NULL);
                    })
                    ->select('chapters.*', \DB::raw("COUNT(units.chapter_id) AS total"))
                    ->where('chapters.deleted_at','=',NULL)
                    ->where('chapters.subject_id', '=',$request->getById,'AND')
                    ->groupBy('sfs.chapters.id')
                    ->orderBy($sort[0],$sort[1])->paginate($page);
                }

                $boards = Board::all()->pluck('name', 'id');
                $data->boards = $boards;

                $classes = MyClass::all()->pluck('name', 'id');
                $data->classes = $classes;

                $subjects = Subject::all()->pluck('name', 'id');
                $data->subjects = $subjects;
                    
                return view('cdesign.user_card', compact('data'));
            }
        return false;
    }

    public function unitAjaxData(Request $request)
    {
        $sort = ['id','desc'];
        $page = 10;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                if(isset($request->q) && !empty($request->q)){
                    $q = $request->q;
                    $data = Unit::leftJoin('question_answers', function($join){
                        $join->on('question_answers.unit_id', '=', 'units.id')->where('question_answers.deleted_at', '=', NULL);
                    })
                    ->select('units.*', \DB::raw("COUNT(question_answers.unit_id) AS total"))
                    ->where('units.deleted_at','=',NULL)
                    ->groupBy('sfs.units.id')
                    ->where ( 'units.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'units.code', 'LIKE', '%' . $q . '%' )
                    ->orderBy('sort_order','asc')->paginate($page)->setPath ( '' );
                } else {
                    $data = Unit::leftJoin('question_answers', function($join){
                        $join->on('question_answers.unit_id', '=', 'units.id')->where('question_answers.deleted_at', '=', NULL);
                    })
                    // $data = Unit::leftjoin('question_answers', 'question_answers.unit_id', '=', 'units.id')
                    ->leftjoin('users', 'units.modified_by_id', '=', 'users.id')
                    ->select('units.*', \DB::raw("COUNT(question_answers.unit_id) AS total"),'users.name as username')
                    ->where('units.deleted_at','=',NULL)
                    ->where('units.chapter_id', '=',$request->getById,'AND')
                    ->groupBy('sfs.units.id')
                    ->orderBy('sort_order','asc')->paginate($page);
                }
                $boards = Board::all()->pluck('name', 'id');
                $data->boards = $boards;

                $classes = MyClass::all()->pluck('name', 'id');
                $data->classes = $classes;

                $subjects = Subject::all()->pluck('name', 'id');
                $data->subjects = $subjects;

                return view('cdesign.user_card', compact('data'));
            }
        return false;
    }

    public function QAAjaxData(Request $request)
    {
        $sort = ['id','desc'];
        $user = auth()->user();
        $page = 10;
           if ($request->ajax()) {
               
               if(isset($request->s) && !empty($request->s)){
                $sort = explode(',', $request->s);
               }

                // if(isset($request->q) && !empty($request->q)){
                //     $q = $request->q;
                //     $data = Unit::leftJoin('question_answers', function($join){
                //         $join->on('question_answers.unit_id', '=', 'units.id')->where('question_answers.deleted_at', '=', NULL);
                //     })
                //     ->select('units.*', \DB::raw("COUNT(question_answers.unit_id) AS total"))
                //     ->where('units.deleted_at','=',NULL)
                //     ->groupBy('sfs.units.id')
                //     ->where ( 'units.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'units.code', 'LIKE', '%' . $q . '%' )
                //     ->orderBy('sort_order','asc')->paginate($page)->setPath ( '' );
                // } else {
                //     $data = Unit::leftJoin('question_answers', function($join){
                //         $join->on('question_answers.unit_id', '=', 'units.id')->where('question_answers.deleted_at', '=', NULL);
                //     })
                //     // $data = Unit::leftjoin('question_answers', 'question_answers.unit_id', '=', 'units.id')
                //     ->leftjoin('users', 'units.modified_by_id', '=', 'users.id')
                //     ->select('units.*', \DB::raw("COUNT(question_answers.unit_id) AS total"),'users.name as username')
                //     ->where('units.deleted_at','=',NULL)
                //     ->where('units.chapter_id', '=',$request->getById,'AND')
                //     ->groupBy('sfs.units.id')
                //     ->orderBy('sort_order','asc')->paginate($page);
                // }
                $data = QuestionAnswer::where('unit_id',$request->getById)->orderBy('sort_order','asc')->get();
                
                $boards = Board::all()->pluck('name', 'id');
                $data->boards = $boards;

                $classes = MyClass::all()->pluck('name', 'id');
                $data->classes = $classes;

                $subjects = Subject::all()->pluck('name', 'id');
                $data->subjects = $subjects;
                $userId = $user['id'];
                $qLogData = DB::select ( DB::raw ( "SELECT session_studyagain,session_gotit from student_unit_access_log where id=$request->getById and unit_id=$userId"));

                //$datas->qLogData = $qLogData;
                return view('cdesign.flash_card', compact('data','qLogData'));
            }
        return false;
    }
    
    public function QAAjaxSubmitData(Request $request)
    {	
    	$user = auth()->user();
    	$userId = $user['id'];
    	$qaLog = new StudentUnitAccessLog();
    	$student = DB::select ( DB::raw ( "SELECT id from students where studentid=$userId"));
    	$studentId = $student[0]->id;
    	if($request->qalog){
    		$qus= $request->q;
    		
    		if($request->typ){
    			$exitingid = DB::select ( DB::raw ( "SELECT session_gotit from student_unit_access_log where id=$request->qalog"));
    		}else{
    			$exitingid = DB::select ( DB::raw ( "SELECT session_studyagain from student_unit_access_log where id=$request->qalog"));
    		}
    		$timingdata = DB::select ( DB::raw ( "SELECT session_time from student_unit_access_log where id=$request->qalog"));
    		
    		$qaLog= StudentUnitAccessLog::where('student_unit_access_log.id', $request->qalog);
    		if($request->typ){
    			if($exitingid[0]->session_gotit){
    				$qusId = $exitingid[0]->session_gotit.','.$qus;
    			}else{
    				$qusId = $qus;
    			}
    			$qaLog->update(['session_gotit' =>$qusId]);
    		}else{
    			if($exitingid[0]->session_studyagain){
    				$qusId = $exitingid[0]->session_studyagain.','.$qus;
    			}else{
    				$qusId = $qus;
    			}
    			$qaLog->update(['session_studyagain' =>$qusId]);
    		}
    		$timingarray = unserialize($timingdata[0]->session_time);
    		$timingarray[$request->q]=$request->t;
    		$qaLog= StudentUnitAccessLog::where('student_unit_access_log.id', $request->qalog);
    		$qaLog->update(['session_time' =>serialize($timingarray)]);
    		$data['id']= $request->qalog;
    		//$request->qalog;
    	}else{
    		$qus= $request->q;
    		$qaLog->student_id = $studentId;
    		$qaLog->unit_id = $request->u;
    		if($request->typ){
    			$qaLog->session_gotit = $qus;
    			$qaLog->session_studyagain = '';
    		}else{
    			$qaLog->session_gotit = '';
    			$qaLog->session_studyagain = $qus;
    		}
    		$timeing[$request->q]=$request->t;
    		$qaLog->session_time = serialize($timeing);
    		$qaLog->created_at = date('Y-m-d H:i:s');
    		$qaLog->deleted_at = NULL;
    		$qaLog->save();
    		
    		$data['id'] = $qaLog->id;
    	}
    	$userId=$user['id'];
    	$qLogDatas = DB::select ( DB::raw ( "SELECT session_studyagain,session_gotit from student_unit_access_log where unit_id=$request->u and student_id=$studentId"));
    	$consolidatedArrays = array();
    	foreach($qLogDatas as $qLogData){
    		$studyagain = $qLogData->session_studyagain;
    		$gotit = $qLogData->session_gotit;
    		$studyagainArrays = explode(",",$studyagain);
    		$gotitArrays = explode(",",$gotit);
    		if($studyagainArrays[0]){
    		foreach($studyagainArrays as $studyagainArray){
    			if(array_key_exists($studyagainArray,$consolidatedArrays)){
    				$consolidatedArrays[$studyagainArray] = $consolidatedArrays[$studyagainArray]-1;
    			}else{
    				$consolidatedArrays[$studyagainArray] = -1;
    			}
    		}
    	    }
    	    if($gotitArrays[0]){
    		foreach($gotitArrays as $gotitArray){
    			if(array_key_exists($gotitArray,$consolidatedArrays)){
    				$consolidatedArrays[$gotitArray] = $consolidatedArrays[$gotitArray]+1;
    			}else{
    				$consolidatedArrays[$gotitArray] = 1;
    			}
    		}
    		}
    	}
    	$qaDataArray = array();
    	$master =0;$familiar =0; $notfamiliar =0;
    	$qaDatas = DB::select ( DB::raw ( "SELECT id from question_answers where unit_id=$request->u"));
    	foreach ($qaDatas as $qaData){
    		//array_push($qaDataArray,$qaData->id);
    		if(array_key_exists($qaData->id,$consolidatedArrays)){
    			if($consolidatedArrays[$qaData->id]>0){
    				$master++;
    			}elseif($consolidatedArrays[$qaData->id]==0){
    				$familiar++;
    			}elseif($consolidatedArrays[$qaData->id]<0){
    				$notfamiliar++;
    			}
    		}else{
    			$notfamiliar++;
    		}
    		
    	}
    	
    	//$data['qLogData']= $consolidatedArrays;
    	$data['master']= $master;
    	$data['familiar']= $familiar;
    	$data['notfamiliar']= $notfamiliar;
    	return $data;
    	
    }
    public function courseAssignAjaxData(Request $request)
    {
    	$user = auth()->user();
    	$userId = $user['id'];
    	$student = DB::select ( DB::raw ( "SELECT id from students where studentid=$userId"));
    	$studentId = $student[0]->id;
    	$studentCourse = new StudentCourse();
    	$studentCourse->student_id = $studentId;
    	$studentCourse->subject_id = $request->sub;
    	$studentCourse->is_active = '1';
    	$studentCourse->created_at = date('Y-m-d H:i:s');
    	$studentCourse->deleted_at = NULL;
    	$studentCourse->save();
    	
    	$data['status'] = 'success';
    	$data['sub'] = $request->sub;
    	return $data;
    }
    
    public function mycourseSubAjaxData(Request $request)
    {
    	$sort = ['id','desc'];
    	$page = 5;
    	$user = auth()->user();
    	$userId = $user['id'];
    	$student = DB::select ( DB::raw ( "SELECT id from students where studentid=$userId"));
    	$studentId = $student[0]->id;
    	if ($request->ajax()) {
    		
    		if(isset($request->s) && !empty($request->s)){
    			$sort = explode(',', $request->s);
    		}
    		
    		if(isset($request->q) && !empty($request->q)){
    			$q = $request->q;
    			$data = Subject::leftJoin('chapters', function($join){
    				$join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
    			})
    			->join('students', 'students.class_id', '=', 'subjects.class_id')
    			->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
    			->where('subjects.deleted_at','=',NULL)
    			->where('students.studentid','=',$userId)
    			->groupBy('sfs.subjects.id')
    			->where ( 'subjects.name', 'LIKE', '%' . $q . '%' )->orWhere ( 'subjects.code', 'LIKE', '%' . $q . '%' )
    			->orderBy($sort[0],$sort[1])->paginate($page)->setPath ( '' );
    		} elseif(isset($request->filterData) && !empty($request->filterData)) {
    			$filterData = explode('_', $request->filterData);
    			$data = Subject::query();
    			$data = $data->leftJoin('chapters', function($join){
    				$join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
    			})
    			->join('students', 'students.class_id', '=', 'subjects.class_id')
    			->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
    			->where('subjects.deleted_at','=',NULL)
    			->where('students.studentid','=',$userId)
    			->where("subjects.board_id","=",$filterData[0],'AND')
    			->groupBy('sfs.subjects.id')
    			->orderBy($sort[0],$sort[1])->paginate($page);
    		} else {
    			$data = Subject::leftJoin('chapters', function($join){
    				$join->on('chapters.subject_id', '=', 'subjects.id')->where('chapters.deleted_at', '=', NULL);
    			})
    			
    			->join('student_courses', 'student_courses.subject_id', '=', 'subjects.id')
    			->select('subjects.*', \DB::raw("COUNT(chapters.subject_id) AS total"))
    			->where('subjects.deleted_at','=',NULL)
    			->where('student_courses.student_id','=',$studentId)
    			->groupBy('sfs.subjects.id')
    			->orderBy($sort[0],$sort[1])->paginate($page);
    		}
    		$boards = Board::all()->pluck('name', 'id');
    		$data->boards = $boards;
    		
    		$classes = MyClass::all()->pluck('name', 'id');
    		$data->classes = $classes;
    		/*$studentscourses = DB::select ( DB::raw ( "SELECT subject_id from student_courses where student_id=$studentId"));
    		$joinSubject = array();
    		foreach($studentscourses as $studentscourse){
    			array_push($joinSubject,$studentscourse->subject_id);
    		}*/
    		
    		return view('cdesign.mycourse_user_card', compact('data'));
    	}
    	return false;
    }
}
 