<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentStudyUnitRequest;
use App\Http\Requests\UpdateStudentStudyUnitRequest;
use App\Http\Resources\Admin\StudentStudyUnitResource;
use App\StudentStudyUnit;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StudentStudyUnitsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('student_study_unit_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new StudentStudyUnitResource(StudentStudyUnit::with(['studentid', 'unitid', 'chapterid', 'subjectid'])->get());
    }

    public function store(StoreStudentStudyUnitRequest $request)
    {
        $studentStudyUnit = StudentStudyUnit::create($request->all());

        return (new StudentStudyUnitResource($studentStudyUnit))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(StudentStudyUnit $studentStudyUnit)
    {
        abort_if(Gate::denies('student_study_unit_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new StudentStudyUnitResource($studentStudyUnit->load(['studentid', 'unitid', 'chapterid', 'subjectid']));
    }

    public function update(UpdateStudentStudyUnitRequest $request, StudentStudyUnit $studentStudyUnit)
    {
        $studentStudyUnit->update($request->all());

        return (new StudentStudyUnitResource($studentStudyUnit))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(StudentStudyUnit $studentStudyUnit)
    {
        abort_if(Gate::denies('student_study_unit_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $studentStudyUnit->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
