<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreQuestionTypeRequest;
use App\Http\Requests\UpdateQuestionTypeRequest;
use App\Http\Resources\Admin\QuestionTypeResource;
use App\QuestionType;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionTypeApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('question_type_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new QuestionTypeResource(QuestionType::with(['modified_by'])->get());
    }

    public function store(StoreQuestionTypeRequest $request)
    {
        $questionType = QuestionType::create($request->all());

        return (new QuestionTypeResource($questionType))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(QuestionType $questionType)
    {
        abort_if(Gate::denies('question_type_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new QuestionTypeResource($questionType->load(['modified_by']));
    }

    public function update(UpdateQuestionTypeRequest $request, QuestionType $questionType)
    {
        $questionType->update($request->all());

        return (new QuestionTypeResource($questionType))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(QuestionType $questionType)
    {
        abort_if(Gate::denies('question_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $questionType->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
