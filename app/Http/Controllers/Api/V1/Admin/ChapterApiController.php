<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Chapter;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreChapterRequest;
use App\Http\Requests\UpdateChapterRequest;
use App\Http\Resources\Admin\ChapterResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ChapterApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('chapter_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ChapterResource(Chapter::with(['board', 'class', 'subject', 'modified_by'])->get());
    }

    public function store(StoreChapterRequest $request)
    {
        $chapter = Chapter::create($request->all());

        if ($request->input('image', false)) {
            $chapter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }

        return (new ChapterResource($chapter))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Chapter $chapter)
    {
        abort_if(Gate::denies('chapter_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ChapterResource($chapter->load(['board', 'class', 'subject', 'modified_by']));
    }

    public function update(UpdateChapterRequest $request, Chapter $chapter)
    {
        $chapter->update($request->all());

        if ($request->input('image', false)) {
            if (!$chapter->image || $request->input('image') !== $chapter->image->file_name) {
                $chapter->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($chapter->image) {
            $chapter->image->delete();
        }

        return (new ChapterResource($chapter))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Chapter $chapter)
    {
        abort_if(Gate::denies('chapter_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chapter->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
