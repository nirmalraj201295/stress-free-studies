<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreQuestionAnswerRequest;
use App\Http\Requests\UpdateQuestionAnswerRequest;
use App\Http\Resources\Admin\QuestionAnswerResource;
use App\QuestionAnswer;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionAnswerApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('question_answer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new QuestionAnswerResource(QuestionAnswer::with(['unit', 'language', 'question_type', 'modified_by'])->get());
    }

    public function store(StoreQuestionAnswerRequest $request)
    {
        $questionAnswer = QuestionAnswer::create($request->all());

        if ($request->input('image', false)) {
            $questionAnswer->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }

        return (new QuestionAnswerResource($questionAnswer))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(QuestionAnswer $questionAnswer)
    {
        abort_if(Gate::denies('question_answer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new QuestionAnswerResource($questionAnswer->load(['unit', 'language', 'question_type', 'modified_by']));
    }

    public function update(UpdateQuestionAnswerRequest $request, QuestionAnswer $questionAnswer)
    {
        $questionAnswer->update($request->all());

        if ($request->input('image', false)) {
            if (!$questionAnswer->image || $request->input('image') !== $questionAnswer->image->file_name) {
                $questionAnswer->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($questionAnswer->image) {
            $questionAnswer->image->delete();
        }

        return (new QuestionAnswerResource($questionAnswer))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(QuestionAnswer $questionAnswer)
    {
        abort_if(Gate::denies('question_answer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $questionAnswer->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
