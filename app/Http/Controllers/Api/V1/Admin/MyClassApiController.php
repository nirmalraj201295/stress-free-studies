<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreMyClassRequest;
use App\Http\Requests\UpdateMyClassRequest;
use App\Http\Resources\Admin\MyClassResource;
use App\MyClass;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MyClassApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('my_class_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MyClassResource(MyClass::with(['board', 'modified_by'])->get());
    }

    public function store(StoreMyClassRequest $request)
    {
        $myClass = MyClass::create($request->all());

        if ($request->input('image', false)) {
            $myClass->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
        }

        return (new MyClassResource($myClass))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(MyClass $myClass)
    {
        abort_if(Gate::denies('my_class_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MyClassResource($myClass->load(['board', 'modified_by']));
    }

    public function update(UpdateMyClassRequest $request, MyClass $myClass)
    {
        $myClass->update($request->all());

        if ($request->input('image', false)) {
            if (!$myClass->image || $request->input('image') !== $myClass->image->file_name) {
                $myClass->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        } elseif ($myClass->image) {
            $myClass->image->delete();
        }

        return (new MyClassResource($myClass))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(MyClass $myClass)
    {
        abort_if(Gate::denies('my_class_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $myClass->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
