<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Auth\AuthenticationException;

class AdminChecker
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if (!$user || auth()->user()->roles->first()->title != "Admin") {
            return redirect('/home');
        }
        // else {
        //     return redirect('/home');
        // }

        return $next($request);
    }
}
