<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'languages';

    public static $searchable = [
        'name',
    ];

    const IS_ACTIVE_RADIO = [
        'yes' => 'Yes',
        'no'  => 'No',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'code',
        'name',
        'is_active',
        'created_at',
        'updated_at',
        'deleted_at',
        'modified_by_id',
    ];

    public function questionAnswers()
    {
        return $this->hasMany(QuestionAnswer::class, 'language_id', 'id');
    }

    public function modified_by()
    {
        return $this->belongsTo(User::class, 'modified_by_id');
    }
}
