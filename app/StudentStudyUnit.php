<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentStudyUnit extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'student_study_units';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'unitid_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'studentid_id',
        'chapterid_id',
        'subjectid_id',
    ];

    public function studentid()
    {
        return $this->belongsTo(Student::class, 'studentid_id');
    }

    public function unitid()
    {
        return $this->belongsTo(Unit::class, 'unitid_id');
    }

    public function chapterid()
    {
        return $this->belongsTo(Chapter::class, 'chapterid_id');
    }

    public function subjectid()
    {
        return $this->belongsTo(Subject::class, 'subjectid_id');
    }
}
