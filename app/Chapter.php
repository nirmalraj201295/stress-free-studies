<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Chapter extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, Auditable;

    public $table = 'chapters';

    protected $appends = [
        'image',
    ];

    public static $searchable = [
        'code',
        'name',
    ];

    const IS_ACTIVE_RADIO = [
        'yes' => 'Yes',
        'no'  => 'No',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const STATUS_SELECT = [
        'draft'     => 'Draft',
        'review'    => 'Review',
        'published' => 'Published',
    ];

    protected $fillable = [
        'code',
        'name',
        'status',
        'board_id',
        'class_id',
        'is_active',
        'subject_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'description',
        'modified_by_id',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function units()
    {
        return $this->hasMany(Unit::class, 'chapter_id', 'id');
    }

    public function studentStudyUnits()
    {
        return $this->hasMany(StudentStudyUnit::class, 'chapterid_id', 'id');
    }

    public function board()
    {
        return $this->belongsTo(Board::class, 'board_id');
    }

    function class()
    {
        return $this->belongsTo(MyClass::class, 'class_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function getImageAttribute()
    {
        $file = $this->getMedia('image')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function modified_by()
    {
        return $this->belongsTo(User::class, 'modified_by_id');
    }
}
