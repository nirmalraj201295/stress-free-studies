<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class StudentUnitAccessLog extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, Auditable;
    
    public $table = 'student_unit_access_log';
    
    const IS_ACTIVE_RADIO = [
        'yes' => 'Yes',
        'no'  => 'No',
    ];
    
    protected $dates = [
        'created_at',
    ];
    
    protected $fillable = [
        'student_id',
        'unit_id',
        'session_gotit',
        'session_studyagain',
        'session_time',
        'created_at',
    ];
}
