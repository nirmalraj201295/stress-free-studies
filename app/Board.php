<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Board extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, Auditable;

    public $table = 'boards';

    protected $appends = [
        'image',
    ];

    public static $searchable = [
        'code',
        'name',
    ];

    const IS_ACTIVE_RADIO = [
        'yes' => 'Yes',
        'no'  => 'No',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const STATUS_SELECT = [
        'draft'     => 'Draft',
        'review'    => 'Review',
        'published' => 'Published',
    ];

    protected $fillable = [
        'code',
        'name',
        'status',
        'is_active',
        'created_at',
        'updated_at',
        'deleted_at',
        'description',
        'modified_by_id',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function myClasses()
    {
        return $this->hasMany(MyClass::class, 'board_id', 'id');
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'board_id', 'id');
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class, 'board_id', 'id');
    }

    public function units()
    {
        return $this->hasMany(Unit::class, 'board_id', 'id');
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'board_id', 'id');
    }

    public function getImageAttribute()
    {
        $file = $this->getMedia('image')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function modified_by()
    {
        return $this->belongsTo(User::class, 'modified_by_id');
    }
}
