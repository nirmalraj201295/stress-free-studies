var searchData = "";
var sort = "";
var page = 0;
var filterData = "";
var myDropzone;
var getById = "";

var currentFile = null;

function modalTitle(){
   var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];
    last_part = last_part.split("#")[0];
    if(last_part == "boards")
        last_part = "Board";
    else if(last_part == "my-classes")
        last_part = "Class";
    else if(last_part == "subjects")
        last_part = "Subject";
    else if(last_part == "chapters")
        last_part = "Chapter";
    return last_part;
} 

// console.log("url",modalTitle());

$('#addbtn').html('Add '+modalTitle());
// Dropzone.autoDiscover = false;
$(document).ready(function()
    {
        
        // getData(page);

        $(window).on('hashchange', function() {
            if (window.location.hash) {
                page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                }else{
                    getData(page);
                }
            }
        });

        $(document).on('click', '.pagination a',function(event)
        {
            event.preventDefault();
            
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');
  
            var myurl = $(this).attr('href');
            var page=$(this).attr('href').split('page=')[1];
  
            getData(page);
        });
  
    });


    $("#add_form").submit(function (e) {
        e.preventDefault();
        var url = "", type = "";
        console.log($('#submit').text());
        if($('#submit').text() == 'Add'){
            url = addurl;
        } else {
            url = updateurl;
            url = url.replace(':id', $('#submit').val());
        }
        console.log(url);
        $.ajax({
            url: url,
            type: "POST",
            data: $('#add_form').serialize(),
            success: function (response_sub) {
                if (response_sub.status == true) {
                    $( '#add_form' ).each(function(){
                        this.reset();
                    });
                    getData(0);
                    
                    $('#addFormModal').modal('hide');
                }
            }
        });
        
        return false;
    });

    $('#search').on('keyup',function(){
        page = 0;
        searchData = $(this).val();
        getData(page);
    });

    $('#sortby').on('change', function() {
        page = 0;
        var sortVal = $(this).val();
        if(sortVal == 'created_at'){
            sort = "created_at,desc";
        }else if(sortVal == 'is_enable'){
            sort = "is_active,desc";
        }else if(sortVal == 'atoz'){
            sort = "name,asc";
        }else if(sortVal == 'ztoa'){
            sort = "name,desc";
        }
        getData(page);
    });

    function getData(page){
        $('.loading').show();
        var q = "";
        // if(searchData == "")
        //     q = 'page=' + page;
        // else
            q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&sv=' + subview + '&filterData=' + filterData + '&getById=' + getById;

        $.ajax(
        {
            // url: '{{ route('admin.boards.ajaxData') }}?page=' + page,
            url: ajaxurl+'?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data){
            $("#tag_container").empty().html(data);
            location.hash = page;
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $('.loading').hide();
            alert('No response from server');
        });
    }


    function viewRedirect(url){
        window.location.href = url;
    }

    

    function deleteData(id){
      var url = deleteurl;
          url = url.replace(':id', id);
        if(confirm('Are you sure?')){
            $.ajax({
            url: url,
            type: "POST",
            data: { '_method' : 'DELETE' },
            success: function (response_sub) {
                    getData(0);
            }
        });
        }       
    }

    function addModal(){
        // myDropzone.on("addedfile", function(file) {
        //     if (currentFile) {
        //         this.removeFile(currentFile);
        //         console.log("file remove");
        //     }
        //     currentFile = file;
        //     console.log("file added");
        //     });
        $('#image-dropzone').removeClass('dz-started');
        $('.dz-preview').remove();
        $( '#add_form' ).each(function(){
            this.reset();
        });
        $('#addFormModal').modal('show');
        $('#addFormModalLabel').html("Add "+modalTitle());
        $('#_method').val('POST');
        $('#submit').html('Add');
    }

    function editModal(id){
        $('.loading').show();
        var url = editurl;
            url = url.replace(':id', id);
        $.ajax(
        {
            url: url,
            type: "get",
        }).done(function(data){
           console.log(data);
            $('.loading').hide();
            $('#addFormModal').modal('show');
            $('#addFormModalLabel').html("Edit "+modalTitle());
            $('#_method').val('PATCH');
            $('#submit').html('Upadte');
            $('#submit').val(data.id);
            $('#name').val(data.name);
            $('#code').val(data.code);
            $('#description').val(data.description);

            $("#status").children('option').filter(function() {
                return $(this).val() == data.status;
            }).prop('selected', true);  

            $("#is_active").children('option').filter(function() {
                return $(this).val() == data.is_active;
            }).prop('selected', true);  

            $("#board_id").children('option').filter(function() {
                if($(this).val() == data.board_id){
                    $('#board_id').val(data.board_id).trigger('change');
                    return true;
                }
                return false;
            }).prop('selected', true);  

             
           
            
            // $('#status').attr('selected',true);
            // $('#pre_img').attr('src','http://localhost:8000/storage/1/conversions/5df08f9307e24_9bc45c71-5c50-47d1-a063-de881ff06d1c-thumb.jpg');
            // $('#image-dropzone').val(data.image-dropzone);

            // Dropzone.forElement("#image-dropzone").destroy();
            // dropImage(data.image)
            // dropImage("http://localhost:8000/storage/4/5e00af5730d2f_93dc428c-e814-4989-8c8f-73185d3fdac9.jpeg")
            var mockFile = { name: data.image.name, size: data.image.size};
            // myDropzone.emit("addedfile", mockFile);
            // myDropzone.on("addedfile", function(file) {
            // if (currentFile) {
            //     this.removeFile(currentFile);
            //     console.log("file remove");
            // }
            // currentFile = file;
            // console.log("file added");
            // });
            // myDropzone.removeFile(mockFile);
            // myDropzone.removeAllFiles(true);
            myDropzone.emit("addedfile", mockFile);
            myDropzone.emit("thumbnail", mockFile, data.image.url);
            myDropzone.emit("complete", mockFile);
            $('#image-dropzone').addClass('dz-started');
                       
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $('.loading').hide();
            alert('No response from server');
        });
        
    }
    

        dropImage(null);
    
    function dropImage(img) {
        console.log(img);
        
        Dropzone.options.imageDropzone = {
        url: dropImageurl,
        maxFilesize: 2, // MB
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        maxFiles: 1,
        addRemoveLinks: true,
        headers: {
        'X-CSRF-TOKEN': csrf_token
        },
        params: {
        size: 2,
        width: 4096,
        height: 4096
        },
        success: function (file, response) {
        $('form').find('input[name="image"]').remove()
        $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
        },
        removedfile: function (file) {
        file.previewElement.remove()
        if (file.status !== 'error') {
            $('form').find('input[name="image"]').remove()
            this.options.maxFiles = this.options.maxFiles + 1
        }
        },
        init: function () {
            myDropzone = this;
        // if(img != null) {
        // var file = img
        //     this.options.addedfile.call(this, file)
        // //   this.options.thumbnail.call(this, file, '{{ $board->image->getUrl('thumb') }}')
        // this.options.thumbnail.call(this, file, img.thumbnail)
        // file.previewElement.classList.add('dz-complete')
        // $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
        // this.options.maxFiles = this.options.maxFiles - 1
        
        // }
        this.on("addedfile", function(file) {
            if (currentFile) {
                this.removeFile(currentFile);
                console.log("file remove");
            }
            currentFile = file;
            console.log("file added");
            });
    // @endif

        

        },
        error: function (file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }
    }
}
