@extends('layouts.adminpanel')
@section('content')
@can('chapter_create')
    <!-- <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.chapters.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.chapter.title_singular') }}
            </a>
        </div>
    </div> -->
@endcan
@include('partials.setupheader')

<!--modal window start-->
<div class="modal fade" id="addFormModal" tabindex="-1" role="dialog" aria-labelledby="addFormModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <span id="addFormModalLabel">Add Class</span></h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" id="add_form" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" id="_method" value="POST"/>
                    <div class="form-group">
                        <select class="form-control {{ $errors->has('board') ? 'is-invalid' : '' }}" name="board_id" id="board_id" required>
                            @foreach($boards as $id => $board)
                                <option value="{{ $id }}" {{ old('board_id') == $id ? 'selected' : '' }}>{{ $board }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                       <select class="form-control {{ $errors->has('class') ? 'is-invalid' : '' }}" name="class_id" id="class_id" required>
                              <option value="">{{ trans('global.pleaseSelect') }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                    <select class="form-control {{ $errors->has('subject') ? 'is-invalid' : '' }}" name="subject_id" id="subject_id" required>
                             <option value="">{{ trans('global.pleaseSelect') }}</option>
                    </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"  name="name" id="name" value="{{ old('name', '') }}"
                            placeholder="{{ trans('cruds.chapter.fields.name') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" name="code" id="code" value="{{ old('code', '') }}"
                            placeholder="{{ trans('cruds.chapter.fields.code') }}" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="{{ trans('cruds.chapter.fields.description') }}"  name="description" id="description" value="{{ old('description', '') }}" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                    <img id="pre_img" style="display: none;"/>
                    <div class="upload-section">
                        <a href="#" class="upload-btn needsclick dropzone {{ $errors->has('image') ? 'is-invalid' : '' }}  dz-message" id="image-dropzone">
                            <span class="upload-icon dz-message" ><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                            <span class="upload-text dz-message" data-dz-message>Upload Cover Image</span>
                        </a>
                    </div>
                    </div>

                    <div class="form-group">
                        <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" required>
                            <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.chooseStatus') }}</option>
                            @foreach(App\Board::STATUS_SELECT as $key => $label)
                                <option value="{{ $key }}" {{ old('status', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="form-control {{ $errors->has('is_active') ? 'is-invalid' : '' }}" name="is_active" id="is_active" required>
                            <option value disabled {{ old('is_active', null) === null ? 'selected' : '' }}>{{ trans('global.chooseEnableDisable') }}</option>
                        @foreach(App\Board::IS_ACTIVE_RADIO as $key => $label)
                              <option value="{{ $key }}" {{ old('is_active', '') === (string) $key ? 'selected' : '' }}>{{ $label == 'Yes'? 'Enable' : 'Disable' }}</option>
                        @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="modified_by_id" id="modified_by_id" value="{{ Auth::user()->id }}"/>
                   

                </div>
                <div class="modal-footer text-center">
                    <button type="submit" id="submit" class="btn btn-primary btn-small">Add</button>
                    <button type="button" class="btn btn-light btn-small" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

  </div>
         </div>
     </main>

@endsection
@section('scripts')
@parent
<script>
//     $(function () {
//   let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
// @can('chapter_delete')
//   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
//   let deleteButton = {
//     text: deleteButtonTrans,
//     url: "{{ route('admin.chapters.massDestroy') }}",
//     className: 'btn-danger',
//     action: function (e, dt, node, config) {
//       var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
//           return entry.id
//       });

//       if (ids.length === 0) {
//         alert('{{ trans('global.datatables.zero_selected') }}')

//         return
//       }

//       if (confirm('{{ trans('global.areYouSure') }}')) {
//         $.ajax({
//           headers: {'x-csrf-token': _token},
//           method: 'POST',
//           url: config.url,
//           data: { ids: ids, _method: 'DELETE' }})
//           .done(function () { location.reload() })
//       }
//     }
//   }
//   dtButtons.push(deleteButton)
// @endcan

//   let dtOverrideGlobals = {
//     buttons: dtButtons,
//     processing: true,
//     serverSide: true,
//     retrieve: true,
//     aaSorting: [],
//     ajax: "{{ route('admin.chapters.index') }}",
//     columns: [
//       { data: 'placeholder', name: 'placeholder' },
// { data: 'id', name: 'id' },
// { data: 'board_name', name: 'board.name' },
// { data: 'board.code', name: 'board.code' },
// { data: 'class_name', name: 'class.name' },
// { data: 'class.code', name: 'class.code' },
// { data: 'subject_name', name: 'subject.name' },
// { data: 'subject.code', name: 'subject.code' },
// { data: 'code', name: 'code' },
// { data: 'name', name: 'name' },
// { data: 'description', name: 'description' },
// { data: 'image', name: 'image', sortable: false, searchable: false },
// { data: 'status', name: 'status' },
// { data: 'is_active', name: 'is_active' },
// { data: 'modified_by_name', name: 'modified_by.name' },
// { data: 'actions', name: '{{ trans('global.actions') }}' }
//     ],
//     order: [[ 1, 'desc' ]],
//     pageLength: 100,
//   };
//   $('.datatable-Chapter').DataTable(dtOverrideGlobals);
//     $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
//         $($.fn.dataTable.tables(true)).DataTable()
//             .columns.adjust();
//     });
// });


var ajaxurl = "{{ route('admin.chapters.ajaxData') }}";
var addurl = '{{ route("admin.chapters.store") }}';
var editurl = '{{ route("admin.chapters.edit", ":id") }}';
var updateurl = '{{ route("admin.chapters.update", ":id") }}';
var deleteurl = '{{ route("admin.chapters.destroy", ":id") }}';
var dropImageurl = "{{ route('admin.chapters.storeMedia') }}";
    

   
</script>
<script src="{{ asset('js/setupjs.js') }}"></script>
<script>
    $("#board_id").change(function(){
        $.ajax({
            url: "{{ route('admin.subjects.get_by_classes') }}?board_id=" + $(this).val(),
            method: 'GET',
            success: function(data) {
                $('#class_id').html(data.html);
            }
        });
    });
    $("#class_id").change(function(){
        $.ajax({
            url: "{{ route('admin.chapters.get_by_subjects') }}?class_id=" + $(this).val(),
            method: 'GET',
            success: function(data) {
                $('#subject_id').html(data.html);
            }
        });
    });
</script>
@endsection