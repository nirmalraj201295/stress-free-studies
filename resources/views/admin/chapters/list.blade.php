@extends('layouts.adminpanel')
@section('content')

<main class="chapter-section">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="col-2">
                    <div class="filter">
                        <p><strong class="text-purple">Filters</strong></p>
                        <hr/>
                        <p><strong>Board</strong></p>
                        <div class="form-group mb-15">
                            <input type="search" class="form-control" placeholder="Search board" />
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">CBSE</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">ICSE</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">CVE</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">IB</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">State boards</label>
                        </div>
                        <a href="#" class="btn btn-link text-purple">15 More >></a>
                        <hr />
                        <p><strong>Standard / Class</strong></p>
                        <div class="form-group mb-15">
                            <input type="search" class="form-control" placeholder="Search board" />
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">1 std</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">2 std</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">3 std</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">4 std</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">5 std</label>
                        </div>
                        <a href="#" class="btn btn-link text-purple">45 More >></a>
                        <hr />
                        <p><strong>Subject</strong></p>
                        <div class="form-group mb-15">
                            <input type="search" class="form-control" placeholder="Search board" />
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Tamil</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">English</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Maths</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Sciense</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Computer Sciense</label>
                        </div>
                        <a href="#" class="btn btn-link text-purple">150 More >></a>

                    </div>
                </div> -->
                <div class="col-10">
                    <div class="row" id="tag_container">
                        <!-- <div class="col-4">
                            <div class="chapter-box">
                                <span class="text-orange text-small">10 Units</span>
                                <span class="text-small pull-right">Few hours ago</span>
                                <div class="media">
                                        <img class="align-self-start mr-3" src="{{ asset('img/profile.jpg') }}" alt="">
                                        <div class="media-body">
                                          <h5 class="mt-0">The importance local spices and herbs to the human health</h5>
                                        </div>
                                      </div>
                                      <div class="board-type chapter-btn"><span>CBSC Board</span> <span>10th std</span>
                                        <span>Science</span></div>
                                <p>Lorem ipsum dolor sit amet, Setur piscing elit, sed do mod tempor ididunt ut labore et dolore...</p>
                                <div class="row">
                                        <div class="col-4">CBSE Board
                                        </div>
                                        <div class="col-4 text-center">10 std
                                        </div>
                                        <div class="col-4 text-right">Science
                                        </div>
                                    </div>          
                 
                                   
                            </div>
                        </div> -->
                        <!-- <div class="row" id="tag_container"></div> -->
                        
                        

                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
@section('scripts')
@parent
<script>
    var ajaxurl = "{{ route('admin.chapters.ajaxData') }}";
    var addurl = '{{ route("admin.chapters.store") }}';
    var editurl = '{{ route("admin.chapters.edit", ":id") }}';
    var updateurl = '{{ route("admin.chapters.update", ":id") }}';
    var deleteurl = '{{ route("admin.chapters.destroy", ":id") }}';
    var dropImageurl = "{{ route('admin.chapters.storeMedia') }}";
    
    subview = "clist";
   
</script>
<script src="{{ asset('js/setupjs.js') }}"></script>

@endsection