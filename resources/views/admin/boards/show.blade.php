@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.board.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.boards.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.id') }}
                        </th>
                        <td>
                            {{ $board->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.code') }}
                        </th>
                        <td>
                            {{ $board->code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.name') }}
                        </th>
                        <td>
                            {{ $board->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.description') }}
                        </th>
                        <td>
                            {{ $board->description }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.image') }}
                        </th>
                        <td>
                            @if($board->image)
                                <a href="{{ $board->image->getUrl() }}" target="_blank">
                                    <img src="{{ $board->image->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.status') }}
                        </th>
                        <td>
                            {{ App\Board::STATUS_SELECT[$board->status] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.is_active') }}
                        </th>
                        <td>
                            {{ App\Board::IS_ACTIVE_RADIO[$board->is_active] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.board.fields.modified_by') }}
                        </th>
                        <td>
                            {{ $board->modified_by->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.boards.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>


    </div>
</div>
@endsection