@extends('layouts.adminpanel')
@section('content')
@can('board_create')
    <!-- <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.boards.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.board.title_singular') }}
            </a>
        </div>
    </div> -->
@endcan

@include('partials.setupheader')

<!-- <div class="card">
    <div class="card-header">
        {{ trans('cruds.board.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Board">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.board.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.code') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.name') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.description') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.image') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.status') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.is_active') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.modified_by') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>


    </div>
</div> -->

<!--modal window start-->
<div class="modal fade" id="addFormModal" tabindex="-1" role="dialog" aria-labelledby="addFormModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <span id="addFormModalLabel">Add Board</span></h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" id="add_form" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" id="_method" value="POST"/>
                    <div class="form-group">
                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"  name="name" id="name" value="{{ old('name', '') }}"
                            placeholder="{{ trans('cruds.board.fields.name') }}" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" name="code" id="code" value="{{ old('code', '') }}"
                            placeholder="{{ trans('cruds.board.fields.code') }}" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="{{ trans('cruds.board.fields.description') }}"  name="description" id="description" value="{{ old('description', '') }}" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                    <img id="pre_img" style="display: none;"/>
                    <div class="upload-section">
                        <a href="#" class="upload-btn needsclick dropzone {{ $errors->has('image') ? 'is-invalid' : '' }}  dz-message" id="image-dropzone">
                            <span class="upload-icon dz-message" ><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                            <span class="upload-text dz-message" data-dz-message>Upload Cover Image</span>
                        </a>
                    </div>
                    <!-- <DIV>
                        <div class="dropzone needsclick" id="demo-upload1" action="/upload">
                        <DIV class="dz-message needsclick">    
                            Drop files here or click to upload.<BR>
                            <SPAN class="note needsclick">(This is just a demo dropzone. Selected 
                            files are <STRONG>not</STRONG> actually uploaded.)</SPAN>
                        </DIV>
                        </div>
                    </DIV> -->
                    </div>

                    <div class="form-group">
                        <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" required>
                            <option value>{{ trans('global.chooseStatus') }}</option>
                            @foreach(App\Board::STATUS_SELECT as $key => $label)
                                <option value="{{ $key }}" {{ old('status', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="form-control {{ $errors->has('is_active') ? 'is-invalid' : '' }}" name="is_active" id="is_active" required>
                            <option value disabled {{ old('is_active', null) === null ? 'selected' : '' }}>{{ trans('global.chooseEnableDisable') }}</option>
                        @foreach(App\Board::IS_ACTIVE_RADIO as $key => $label)
                            <option value="{{ $key }}" {{ old('is_active', '') === (string) $key ? 'selected' : '' }}>{{ $label == 'Yes'? 'Enable' : 'Disable' }}</option>
                        @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="modified_by_id" id="modified_by_id" value="{{ Auth::user()->id }}"/>
                   

                </div>
                <div class="modal-footer text-center">
                    <button type="submit" id="submit" class="btn btn-primary btn-small">Add</button>
                    <button type="button" class="btn btn-light btn-small" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

  </div>
         </div>
     </main>
     
@endsection
@section('scripts')
@parent
<script>
//     $(function () {
//   let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
// @can('board_delete')
//   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
//   let deleteButton = {
//     text: deleteButtonTrans,
//     url: "{{ route('admin.boards.massDestroy') }}",
//     className: 'btn-danger',
//     action: function (e, dt, node, config) {
//       var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
//           return entry.id
//       });

//       if (ids.length === 0) {
//         alert('{{ trans('global.datatables.zero_selected') }}')

//         return
//       }

//       if (confirm('{{ trans('global.areYouSure') }}')) {
//         $.ajax({
//           headers: {'x-csrf-token': _token},
//           method: 'POST',
//           url: config.url,
//           data: { ids: ids, _method: 'DELETE' }})
//           .done(function () { location.reload() })
//       }
//     }
//   }
//   dtButtons.push(deleteButton)
// @endcan

//   let dtOverrideGlobals = {
//     buttons: dtButtons,
//     processing: true,
//     serverSide: true,
//     retrieve: true,
//     aaSorting: [],
//     ajax: "{{ route('admin.boards.index') }}",
//     columns: [
//       { data: 'placeholder', name: 'placeholder' },
// { data: 'id', name: 'id' },
// { data: 'code', name: 'code' },
// { data: 'name', name: 'name' },
// { data: 'description', name: 'description' },
// { data: 'image', name: 'image', sortable: false, searchable: false },
// { data: 'status', name: 'status' },
// { data: 'is_active', name: 'is_active' },
// { data: 'modified_by_name', name: 'modified_by.name' },
// { data: 'actions', name: '{{ trans('global.actions') }}' }
//     ],
//     order: [[ 1, 'desc' ]],
//     pageLength: 100,
//   };
//   $('.datatable-Board').DataTable(dtOverrideGlobals);
//     $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
//         $($.fn.dataTable.tables(true)).DataTable()
//             .columns.adjust();
//     });
// });
// var searchData = "";
// var sort = "";
// var page = 0;

// $('#addbtn').html('Add Board');

// getData(page);

// $(window).on('hashchange', function() {
//         if (window.location.hash) {
//             page = window.location.hash.replace('#', '');
//             if (page == Number.NaN || page <= 0) {
//                 return false;
//             }else{
//                 getData(page);
//             }
//         }
//     });

//     $(document).ready(function()
//     {
//         $(document).on('click', '.pagination a',function(event)
//         {
//             event.preventDefault();
            
//             $('li').removeClass('active');
//             $(this).parent('li').addClass('active');
  
//             var myurl = $(this).attr('href');
//             var page=$(this).attr('href').split('page=')[1];
  
//             getData(page);
//         });
  
//     });

    

//     $('#search').on('keyup',function(){
//         page = 0;
//         searchData = $(this).val();
//         getData(page);
//     });

//     $('#sortby').on('change', function() {
//         page = 0;
//         var sortVal = $(this).val();
//         if(sortVal == 'created_at'){
//             sort = "created_at,desc";
//         }else if(sortVal == 'is_enable'){
//             sort = "is_active,desc";
//         }else if(sortVal == 'atoz'){
//             sort = "name,asc";
//         }else if(sortVal == 'ztoa'){
//             sort = "name,desc";
//         }
//         getData(page);
//     });

//     function getData(page){
//         $('.loading').show();
//         var q = "";
//         // if(searchData == "")
//         //     q = 'page=' + page;
//         // else
//             q = 'q=' + searchData + '&s=' + sort + '&page=' + page;

//         $.ajax(
//         {
//             // url: '{{ route('admin.boards.ajaxData') }}?page=' + page,
//             url: '{{ route('admin.boards.ajaxData') }}?' + q,
//             type: "get",
//             datatype: "html"
//         }).done(function(data){
//             $("#tag_container").empty().html(data);
//             location.hash = page;
//             $('.loading').hide();
//         }).fail(function(jqXHR, ajaxOptions, thrownError){
//             $('.loading').hide();
//             alert('No response from server');
//         });
//     }

//     function deleteData(id){
//       var url = '{{ route("admin.boards.destroy", ":id") }}';
//           url = url.replace(':id', id);
//         if(confirm('{{ trans('global.areYouSure') }}')){
//             $.ajax({
//             url: url,
//             type: "POST",
//             data: { '_method' : 'DELETE' },
//             success: function (response_sub) {
//                     getData(0);
//             }
//         });
//         }       
//     }

//     function addModal(){
//         $('#addFormModal').modal('show');
//         $('#addFormModalLabel').html("Add Board");
//         $('#submit').html('Add');
//     }

//     function editModal(id){
//         $('.loading').show();
//         var url = '{{ route("admin.boards.edit", ":id") }}';
//             url = url.replace(':id', id);
//         $.ajax(
//         {
//             url: url,
//             type: "get",
//         }).done(function(data){
//            console.log(data);
//             $('.loading').hide();
//             $('#addFormModal').modal('show');
//             $('#addFormModalLabel').html("Edit Board");
            
//             $('#submit').html('Upadte');
//             $('#submit').val(data.id);
//             $('#name').val(data.name);
//             $('#code').val(data.code);
//             $('#description').val(data.description);
//             $('#pre_img').attr('src','http://localhost:8000/storage/1/conversions/5df08f9307e24_9bc45c71-5c50-47d1-a063-de881ff06d1c-thumb.jpg');
//             // $('#image-dropzone').val(data.image-dropzone);

//             // Dropzone.forElement("#image-dropzone").destroy();
//             // dropImage(data.image)
                       
//         }).fail(function(jqXHR, ajaxOptions, thrownError){
//             $('.loading').hide();
//             alert('No response from server');
//         });
        
//     }

// $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         error: function (jqXHR, exception) {
//             if (jqXHR.status === 0) {
//                 alert('Not connect.\n Verify Network.');
//             } else if (jqXHR.status == 404) {
//                 alert('Requested page not found. [404]');
//             } else if (jqXHR.status == 500) {
//                 alert('Internal Server Error [500].');
//             } else if (exception === 'parsererror') {
//                 alert('Requested JSON parse failed.');
//             } else if (exception === 'timeout') {
//                 alert('Time out error.');
//             } else if (exception === 'abort') {
//                 alert('Ajax request aborted.');
//             } else {
//                 var err = eval("(" + jqXHR.responseText + ")");
//                 if(err.errors.code != undefined)
//                     alert('Error!\n' + err.errors.code);
//                 else if(err.errors.name != undefined)
//                     alert('Error!\n' + err.errors.name);
//                 else
//                     alert('Error!\n' + err);
//             }
//         }
//     });
//     $("#add_form").submit(function (e) {
//         e.preventDefault();
//         var url = "", type = "";
//         console.log($('#submit').text());
//         if($('#submit').text() == 'Add'){
//             url = '{{ route("admin.boards.store") }}';
//         } else {
//             url = '{{ route("admin.boards.update", ":id") }}';
//             url = url.replace(':id', $('#submit').val());
//         }
//         console.log(url);
//         $.ajax({
//             url: url,
//             type: "POST",
//             data: $('#add_form').serialize(),
//             success: function (response_sub) {
//                 if (response_sub.status == true) {
//                     getData(0);
//                     $( '#add_form' ).each(function(){
//                         this.reset();
//                     });
//                     $('#addFormModal').modal('hide');
//                 }
//             }
//         });
        
//         return false;
//     });

//     dropImage(null);
    
//     function dropImage(img) {
//         console.log(img);
//     Dropzone.options.imageDropzone = {
//     url: '{{ route('admin.boards.storeMedia') }}',
//     maxFilesize: 2, // MB
//     acceptedFiles: '.jpeg,.jpg,.png,.gif',
//     maxFiles: 1,
//     addRemoveLinks: true,
//     headers: {
//       'X-CSRF-TOKEN': "{{ csrf_token() }}"
//     },
//     params: {
//       size: 2,
//       width: 4096,
//       height: 4096
//     },
//     success: function (file, response) {
//       $('form').find('input[name="image"]').remove()
//       $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
//     },
//     removedfile: function (file) {
//       file.previewElement.remove()
//       if (file.status !== 'error') {
//         $('form').find('input[name="image"]').remove()
//         this.options.maxFiles = this.options.maxFiles + 1
//       }
//     },
//     init: function () {
// @if(isset($board) && $board->image)
//     //   var file = {!! json_encode($board->image) !!}
//     var file = img
//           this.options.addedfile.call(this, file)
//     //   this.options.thumbnail.call(this, file, '{{ $board->image->getUrl('thumb') }}')
//     this.options.thumbnail.call(this, file, img.thumbnail)
//       file.previewElement.classList.add('dz-complete')
//       $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
//       this.options.maxFiles = this.options.maxFiles - 1
// @endif
//     },
//     error: function (file, response) {
//         if ($.type(response) === 'string') {
//             var message = response //dropzone sends it's own error messages in string
//         } else {
//             var message = response.errors.file
//         }
//         file.previewElement.classList.add('dz-error')
//         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
//         _results = []
//         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
//             node = _ref[_i]
//             _results.push(node.textContent = message)
//         }

//         return _results
//     }
// }
// }
var ajaxurl = "{{ route('admin.boards.ajaxData') }}";
var addurl = '{{ route("admin.boards.store") }}';
var editurl = '{{ route("admin.boards.edit", ":id") }}';
var updateurl = '{{ route("admin.boards.update", ":id") }}';
var deleteurl = '{{ route("admin.boards.destroy", ":id") }}';
var dropImageurl = "{{ route('admin.boards.storeMedia') }}";

    

   
</script>
<script src="{{ asset('js/setupjs.js') }}"></script>
@endsection