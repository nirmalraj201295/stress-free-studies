@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.studentStudyUnit.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.student-study-units.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.studentStudyUnit.fields.id') }}
                        </th>
                        <td>
                            {{ $studentStudyUnit->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studentStudyUnit.fields.studentid') }}
                        </th>
                        <td>
                            {{ $studentStudyUnit->studentid->studentid ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studentStudyUnit.fields.unitid') }}
                        </th>
                        <td>
                            {{ $studentStudyUnit->unitid->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studentStudyUnit.fields.chapterid') }}
                        </th>
                        <td>
                            {{ $studentStudyUnit->chapterid->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.studentStudyUnit.fields.subjectid') }}
                        </th>
                        <td>
                            {{ $studentStudyUnit->subjectid->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.student-study-units.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>


    </div>
</div>
@endsection