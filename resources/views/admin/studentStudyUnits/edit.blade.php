@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.studentStudyUnit.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.student-study-units.update", [$studentStudyUnit->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="studentid_id">{{ trans('cruds.studentStudyUnit.fields.studentid') }}</label>
                <select class="form-control select2 {{ $errors->has('studentid') ? 'is-invalid' : '' }}" name="studentid_id" id="studentid_id" required>
                    @foreach($studentids as $id => $studentid)
                        <option value="{{ $id }}" {{ ($studentStudyUnit->studentid ? $studentStudyUnit->studentid->id : old('studentid_id')) == $id ? 'selected' : '' }}>{{ $studentid }}</option>
                    @endforeach
                </select>
                @if($errors->has('studentid_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('studentid_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentStudyUnit.fields.studentid_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="unitid_id">{{ trans('cruds.studentStudyUnit.fields.unitid') }}</label>
                <select class="form-control select2 {{ $errors->has('unitid') ? 'is-invalid' : '' }}" name="unitid_id" id="unitid_id" required>
                    @foreach($unitids as $id => $unitid)
                        <option value="{{ $id }}" {{ ($studentStudyUnit->unitid ? $studentStudyUnit->unitid->id : old('unitid_id')) == $id ? 'selected' : '' }}>{{ $unitid }}</option>
                    @endforeach
                </select>
                @if($errors->has('unitid_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('unitid_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentStudyUnit.fields.unitid_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="chapterid_id">{{ trans('cruds.studentStudyUnit.fields.chapterid') }}</label>
                <select class="form-control select2 {{ $errors->has('chapterid') ? 'is-invalid' : '' }}" name="chapterid_id" id="chapterid_id" required>
                    @foreach($chapterids as $id => $chapterid)
                        <option value="{{ $id }}" {{ ($studentStudyUnit->chapterid ? $studentStudyUnit->chapterid->id : old('chapterid_id')) == $id ? 'selected' : '' }}>{{ $chapterid }}</option>
                    @endforeach
                </select>
                @if($errors->has('chapterid_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('chapterid_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentStudyUnit.fields.chapterid_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="subjectid_id">{{ trans('cruds.studentStudyUnit.fields.subjectid') }}</label>
                <select class="form-control select2 {{ $errors->has('subjectid') ? 'is-invalid' : '' }}" name="subjectid_id" id="subjectid_id" required>
                    @foreach($subjectids as $id => $subjectid)
                        <option value="{{ $id }}" {{ ($studentStudyUnit->subjectid ? $studentStudyUnit->subjectid->id : old('subjectid_id')) == $id ? 'selected' : '' }}>{{ $subjectid }}</option>
                    @endforeach
                </select>
                @if($errors->has('subjectid_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('subjectid_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentStudyUnit.fields.subjectid_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>


    </div>
</div>
@endsection