@extends('layouts.adminpanel')
@section('content')
<section class="banner-section" style="margin: -20px -15px auto">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-6">
                            <a href="#" class="text-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> cancel & back
                                to chapters & units list</a>
                        </div>
                        <div class="col-6 text-right">
                            <a href="#" class="text-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                Chapter</a>
                        </div>
                     </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="media">
                                <img class="align-self-start mr-3" width="153" height="169" src="{{ asset('img/profile.jpg') }}"
                                    alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">{{ $data->chapter->name }}</h5>
                                    <p>{{ $data->chapter->description }}</p>
                                    <a href="#" class="text-warning">Show more</a>
                                    <ul class="media-list">
                                    <li>
                                    @foreach($boards as $id => $board)
                                         {{ ($data->board_id == $id) ? $board : '' }}
                                    @endforeach 
                                    </li>
                                        <li>@foreach($classes as $id => $class)
                                         {{ ($data->class_id == $id) ? $class : '' }}
                                    @endforeach </li>
                                        <li>@foreach($subjects as $id => $subject)
                                         {{ ($data->subject_id == $id) ? $subject : '' }}
                                    @endforeach</li>
                                    </ul>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </section>
    <main class="chapter-section" >
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <h1 class="admin-heading-level1">Create Unit</h1>
                </div>
                <div class="col-6 text-right">
                    <a href="#" class="btn btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                        Save as Draft</a>

                    <a href="#" class="btn btn-primary"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
                        Publish Unit</a>

                </div>
            </div>
            <div class="main-section box-shadow">
                <div class="unit-header">
                    <span class="badge badge-primary">1st Unit</span>
                    <a class="pull-right text-warning" href="#"> <i class="fa fa-download" aria-hidden="true"></i>
                        Import from
                        Excel</a>
                    <a href="#" class="accordion-btn"><i class="fa fa-bars" aria-hidden="true"></i></a>
                </div>
                <div class="info-box mb-5">
                    Unit title and description
                </div>

                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Add Cover Image for Unit</label>
                            <input type="email" class="form-control" id="" aria-describedby="emailHelp"
                                placeholder="Enter email">
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Unit name</label>
                            <input type="text" class="form-control" id="" aria-describedby="emailHelp"
                                placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Unit description</label>
                            <input type="text" class="form-control" id="" aria-describedby="emailHelp"
                                placeholder="Enter email">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Keywords</label>
                    <input type="email" class="form-control" id="" aria-describedby="emailHelp"
                        placeholder="Enter email">
                </div>
                <div class="info-box mb-5">
                    Add questions and answers
                </div>
                <div>
                    <div class="que-ans-box">
                        <span class="box-number">1</span>
                        <span class="acc-btn"><i class="fa fa-bars" aria-hidden="true"></i></span>
                        <span class="trash-icon text-danger"><i class="fa fa-trash" aria-hidden="true"></i></span>
                        <div class="action-list"><span><i class="fa fa-font" aria-hidden="true"></i></span><span><img
                                    src="{{ asset('img/ux-design.svg') }}" /></span><span><i class="fa fa-microphone"
                                    aria-hidden="true"></i></span></div>
                        <p><span class="badge badge-warning">Question</span><span>The importance local spices and herbs
                                to the human health sed do eiusm tempor incididunt ut labore et dolore</span></p>


                        <div class="media">
                            <span class="badge badge-primary">Answer</span>
                            <div class="media-body">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit, sed do eiusm tempor incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip </div>
                            <img class="ml-3" src="{{ asset('img/profile.jpg') }}" width="128" height="108"
                                alt="Generic placeholder image">

                        </div>
                    </div>
                    <div class="text-right"><a href="#" class="addnew text-warning"><i class="fa fa-plus-circle"
                                aria-hidden="true"></i> Add New Card</a></div>
                </div>
                


            </div>
            <div class="row mt-5">
                    <div class="col-2 offset-4 text-center">
                            <a href="#" class="addnew text-warning"><i class="fa fa-plus-circle"
                                aria-hidden="true"></i> Add Unit</a>
                    </div>
                    <div class="col-6 text-right">
                        <a href="#" class="btn btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                            Save as Draft</a>
    
                        <a href="#" class="btn btn-primary"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
                            Publish Unit</a>
    
                    </div>
                </div>
    
        </div>
    </main>
<!-- <div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.unit.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.units.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="board_id">{{ trans('cruds.unit.fields.board') }}</label>
                <select class="form-control select2 {{ $errors->has('board') ? 'is-invalid' : '' }}" name="board_id" id="board_id" required>
                    @foreach($boards as $id => $board)
                        <option value="{{ $id }}" {{ old('board_id') == $id ? 'selected' : '' }}>{{ $board }}</option>
                    @endforeach
                </select>
                @if($errors->has('board_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('board_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.board_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="class_id">{{ trans('cruds.unit.fields.class') }}</label>
                <select class="form-control select2 {{ $errors->has('class') ? 'is-invalid' : '' }}" name="class_id" id="class_id" required>
                    @foreach($classes as $id => $class)
                        <option value="{{ $id }}" {{ old('class_id') == $id ? 'selected' : '' }}>{{ $class }}</option>
                    @endforeach
                </select>
                @if($errors->has('class_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('class_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.class_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="subject_id">{{ trans('cruds.unit.fields.subject') }}</label>
                <select class="form-control select2 {{ $errors->has('subject') ? 'is-invalid' : '' }}" name="subject_id" id="subject_id" required>
                    @foreach($subjects as $id => $subject)
                        <option value="{{ $id }}" {{ old('subject_id') == $id ? 'selected' : '' }}>{{ $subject }}</option>
                    @endforeach
                </select>
                @if($errors->has('subject_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('subject_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.subject_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="chapter_id">{{ trans('cruds.unit.fields.chapter') }}</label>
                <select class="form-control select2 {{ $errors->has('chapter') ? 'is-invalid' : '' }}" name="chapter_id" id="chapter_id" required>
                    @foreach($chapters as $id => $chapter)
                        <option value="{{ $id }}" {{ old('chapter_id') == $id ? 'selected' : '' }}>{{ $chapter }}</option>
                    @endforeach
                </select>
                @if($errors->has('chapter_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('chapter_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.chapter_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="code">{{ trans('cruds.unit.fields.code') }}</label>
                <input class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" type="text" name="code" id="code" value="{{ old('code', '') }}" required>
                @if($errors->has('code'))
                    <div class="invalid-feedback">
                        {{ $errors->first('code') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.code_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.unit.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.unit.fields.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', '') }}">
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="image">{{ trans('cruds.unit.fields.image') }}</label>
                <div class="needsclick dropzone {{ $errors->has('image') ? 'is-invalid' : '' }}" id="image-dropzone">
                </div>
                @if($errors->has('image'))
                    <div class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.image_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.unit.fields.status') }}</label>
                <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" required>
                    <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Unit::STATUS_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('status', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('status') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.unit.fields.is_active') }}</label>
                @foreach(App\Unit::IS_ACTIVE_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('is_active') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="is_active_{{ $key }}" name="is_active" value="{{ $key }}" {{ old('is_active', 'yes') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="is_active_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('is_active'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_active') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.is_active_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="modified_by_id">{{ trans('cruds.unit.fields.modified_by') }}</label>
                <select class="form-control select2 {{ $errors->has('modified_by') ? 'is-invalid' : '' }}" name="modified_by_id" id="modified_by_id" required>
                    @foreach($modified_bies as $id => $modified_by)
                        <option value="{{ $id }}" {{ old('modified_by_id') == $id ? 'selected' : '' }}>{{ $modified_by }}</option>
                    @endforeach
                </select>
                @if($errors->has('modified_by_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('modified_by_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.unit.fields.modified_by_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>


    </div>
</div> -->
@endsection

@section('scripts')
<script>
    Dropzone.options.imageDropzone = {
    url: '{{ route('admin.units.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="image"]').remove()
      $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="image"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($unit) && $unit->image)
      var file = {!! json_encode($unit->image) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $unit->image->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection