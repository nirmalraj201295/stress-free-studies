@extends('layouts.adminpanel')
@section('content')
@can('unit_create')
    <!-- <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.units.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.unit.title_singular') }}
            </a>
        </div>
    </div> -->
@endcan
@include('cdesign.chapter_banner')
<style>
nav .pagination{
    margin: 20px;
}
</style>
<main class="chapter-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <h1 class="admin-heading-level1">Units <span id="total_unit"><span></h1>
            </div>
            <div class="col-6 text-right">
                <a class="sort-text" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Sort by <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="dropdown-menu profile-dropdown" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">Latest</a>
                    <a class="dropdown-item" href="#">Alphabetical</a>
                    <a class="dropdown-item" href="#">Last created</a>
                    <a class="dropdown-item" href="#">Last modified</a>
                </div>

                <!-- <a href="#" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>
                    Edit Unit</a> -->

            </div>
         </div>
     </div>
<div class="main-section box-shadow unit-section">
    <div class="row" id="tag_container"></div>
</div>
</main>

 <!--modal window start-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Are you sure remove this unit ?</h3>
                    <p>if you remove this unit you can't recover it.</p>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary btn-small" id="deleteModalId" onclick="deleteunit(this.value)" value="">Yes</button>
                    <button type="button" class="btn btn-light btn-small" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
@parent
<script>

var ajaxurl = "{{ route('admin.units.ajaxData') }}";
var addurl = '{{ route("admin.units.store") }}';
var editurl = '{{ route("admin.units.edit", ":id") }}';
var updateurl = '{{ route("admin.units.update", ":id") }}';
var deleteurl = '{{ route("admin.units.destroy", ":id") }}';
var dropImageurl = "{{ route('admin.units.storeMedia') }}";

subview = "ulist";
    

   
</script>
<script src="{{ asset('js/setupjs.js') }}"></script>
<script>
    var max_fields_limit = 8; //set limit for maximum input fields
var x = 1; //initialize counter for text box
$(document).ready(function() {
    $("#tag_container").sortable({
        placeholder: "ui-state-highlight",
        stop: function(event, ui) {
            var selectedData = new Array();
            $('.unit-row').each(function() {
                var $li = $(this);
                var newVal = $(this).index() + 1;
                selectedData.push($(this).children('span.unit-id').attr('id'));
                // console.log("aa",$(this).children('span.unit-id').attr("id"));
            });
            // console.log("New position: " + ui.item.index() + 1);            
            updateOrder(selectedData);
            // console.log("position", selectedData );
        }
    });
    $( "#tag_container" ).disableSelection();

});

function deleteModal(id){
    $('#deleteModal').modal('show');
    $('#deleteModalId').val(id);
}

function deleteunit(id){
      var url = deleteurl;
          url = url.replace(':id', id);
        // if(confirm('Are you sure?')){
            $.ajax({
            url: url,
            type: "POST",
            data: { '_method' : 'DELETE' },
            success: function (response_sub) {
                    getData(0);
            }
        });
        $('#deleteModal').modal('hide');
        // }       
}

function updateOrder(data) {
        $.ajax({
            url:"{{ route("admin.units.sort_order") }}",
            type:'post',
            data:{position:data},
            success:function(){
                // alert('your change successfully saved');
            }
        })
    }

function add_more_button(){
    // e.preventDefault();
    if(x < max_fields_limit){ //check conditions
    x++; //counter increment
    $('.input_fields_container_part').append('<div class="dragitem ui-state-default"> <div class="que-ans-box"> <span class="ui-state-default box-number">'+x+'</span> <span class="acc-btn"><i class="fa fa-bars" aria-hidden="true"></i></span> <span class="trash-icon text-danger remove_field"><i class="fa fa-trash" aria-hidden="true"></i></span> <div class="action-list"><span><i class="fa fa-font" aria-hidden="true"></i></span><span><img src="{{asset('img/ux-design.svg')}}"/></span><span><i class="fa fa-microphone" aria-hidden="true"></i></span></div><p><span class="badge badge-warning">Question</span><span><input class="col-10" type="text" name="question[]" id="question" required/> </span></p><div class="media"> <span class="badge badge-primary">Answer</span> <div class="media-body"> <input class="col-11" type="text" name="answer[]" id="answer" style="height:100px" required/> </div><img class="ml-3" src="{{asset('img/profile.jpg')}}" width="128" height="108" alt="Generic placeholder image"> </div></div><div class="text-right"><a href="javascript:void(0);" class="addnew text-warning add_more_button" onclick="add_more_button()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Card</a></div></div>'); 
    }
}
</script>

@endsection