@extends('layouts.adminpanel')
@section('content')
@can('question_answer_create')
    <!-- <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.question-answers.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.questionAnswer.title_singular') }}
            </a>
            <button class="btn btn-warning" data-toggle="modal" data-target="#csvImportModal">
                {{ trans('global.app_csvImport') }}
            </button>
            @include('csvImport.modal', ['model' => 'QuestionAnswer', 'route' => 'admin.question-answers.parseCsvImport'])
        </div>
    </div> -->
@endcan
@include('cdesign.chapter_banner')
<main class="chapter-section" >
    <form method="POST" id="unit_qa_form" enctype="multipart/form-data">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <h1 class="admin-heading-level1">Create Unit</h1>
                </div>
                <div class="col-6 text-right">
                         
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
                        Publish Unit</button>

                </div>
            </div>
            <div class="main-section box-shadow">
                <div class="unit-header">
                    <span class="badge badge-primary">1st Unit</span>
                    <a class="pull-right text-warning" href="#" data-toggle="modal" data-target="#importModal"> <i class="fa fa-download" aria-hidden="true"></i>
                    Import from Excel to Import Question & Answer</a>
                    <a href="#" class="accordion-btn"><i class="fa fa-bars" aria-hidden="true"></i></a>
                </div>

                <div class="info-box mb-5">
                    Unit title and description
                </div>
                
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <div class="upload-section">
                                <a href="#" class="upload-btn needsclick dropzone {{ $errors->has('image') ? 'is-invalid' : '' }}  dz-message" id="image-dropzone">
                                    <span class="upload-icon dz-message" ><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                                    <span class="upload-text dz-message" data-dz-message>Add Cover Image for Unit</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="form-group">
                            <label >Unit name</label>
                            <input type="hidden" name="id" value="{{$unit->id}}">
                            <input type="text" class="form-control" id="name" name="name" value="{{$unit->name}}" 
                                placeholder="Enter unit name" required>
                        </div>
                        <div class="form-group">
                            <label >Unit code</label>
                            <input type="text" class="form-control" id="code" name="code" value="{{$unit->code}}" 
                                placeholder="Enter unit code">
                        </div>
                        <div class="form-group">
                            <label >Unit description</label>
                            <input type="text" class="form-control" id="description" name="description" value="{{$unit->description}}" 
                                placeholder="Enter unit description">
                        </div>
                    </div>
                </div>
                <div class="form-group" hidden >
                    <label >Keywords</label>
                    <input type="email" class="form-control" id="" 
                        placeholder="Enter Keywords">
                </div>
                <div class="info-box mb-5">
                    Add questions and answers
                </div>
                <div class="input_fields_container_part mt-5">
                    @if(count($questionAnswers) > 0)
                    @foreach($questionAnswers as $questionAnswer)
                    <div class="dragitem ui-state-default">
                        <div class="que-ans-box">
                            <input type="hidden" name="qa_id[]" value="{{ $questionAnswer->id }}"/>
                            <input type="hidden" class="qa_sort_order" name="qa_sort_order[]" value="{{ $questionAnswer->sort_order }}"/>
                            <span class="ui-state-default box-number" id="{{$questionAnswer->id}}">{{ $loop->iteration }}</span>
                            <span class="acc-btn"><i class="fa fa-bars" aria-hidden="true"></i></span>
                            <span class="trash-icon text-danger remove_field"><i class="fa fa-trash" aria-hidden="true"></i></span>
                            <div class="action-list"><span><i class="fa fa-font" aria-hidden="true"></i></span><span><img
                                        src="{{ asset('img/ux-design.svg') }}" /></span><span><i class="fa fa-microphone"
                                        aria-hidden="true"></i></span></div>
                           
                            <div class="media">
                                <span class="badge badge-warning">Question</span>
                                <div class="media-body">
                                    <textarea class="form-control unit-textarea" placeholder="" name="question[]"  required>{{$questionAnswer->question}}</textarea>
                                </div>
                            </div><br>

                            <div class="media">
                                <span class="badge badge-primary">Answer</span>
                                <div class="media-body">
                                    <textarea class="form-control unit-textarea" placeholder="" name="answer[]"  required>{{$questionAnswer->answer}}</textarea>
                                </div>
                            </div>

                                
                        </div>
                        
                        <div class="text-right"><a href="javascript:void(0);" class="addnew text-warning add_more_button" onclick="add_more_button()"><i class="fa fa-plus-circle"
                                    aria-hidden="true"></i> Add New Card</a></div>
                     </div>
                     @endforeach
                     @else
                     <div class="dragitem ui-state-default">
                        <div class="que-ans-box">
                            <span class="ui-state-default box-number">1</span>
                            <span class="acc-btn"><i class="fa fa-bars" aria-hidden="true"></i></span>
                            <span class="trash-icon text-danger remove_field"><i class="fa fa-trash" aria-hidden="true"></i></span>
                            <div class="action-list"><span><i class="fa fa-font" aria-hidden="true"></i></span><span><img
                                        src="{{ asset('img/ux-design.svg') }}" /></span><span><i class="fa fa-microphone"
                                        aria-hidden="true"></i></span></div>
                           
                            <div class="media">
                                <span class="badge badge-warning">Question</span>
                                <div class="media-body">
                                    <textarea class="form-control unit-textarea" placeholder="" name="question[]"  required></textarea>
                                </div>
                            </div><br>

                            <div class="media">
                                <span class="badge badge-primary">Answer</span>
                                <div class="media-body">
                                    <textarea class="form-control unit-textarea" placeholder="" name="answer[]"  required></textarea>
                                </div>
                            </div>

                                
                        </div>
                        
                        <div class="text-right"><a href="javascript:void(0);" class="addnew text-warning add_more_button" onclick="add_more_button()"><i class="fa fa-plus-circle"
                                    aria-hidden="true"></i> Add New Card</a></div>
                     </div>

                     @endif

                 </div>


            </div>
            <div class="row mt-5">
                    <div class="col-2 offset-4 text-center">
                            <!-- <a href="#" class="addnew text-warning"><i class="fa fa-plus-circle"
                                aria-hidden="true"></i> Add Unit</a> -->
                    </div>
                    <div class="col-6 text-right">
                    <input type="hidden" name="status" value="published"/>
                    <input type="hidden" name="is_active" value="yes"/>
                    <input type="hidden" name="board_id" value="{{$chapter->board_id}}"/>
                    <input type="hidden" name="class_id" value="{{$chapter->class_id}}"/>
                    <input type="hidden" name="subject_id" value="{{$chapter->subject_id}}"/>
                    <input type="hidden" name="chapter_id" value="{{$chapter->id}}"/>
                    <input type="hidden" name="modified_by_id" id="modified_by_id" value="{{ Auth::user()->id }}"/>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
                        Publish Unit</button>
    
                    </div>
                </div>
    
        </div>
</form>
    </main>
    <div class="modal fade modal-lg import-content" id="importModal" tabindex="-1" role="dialog"
    aria-labelledby="addFormModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg import-content" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <span
                        id="addFormModalLabel">Import from Excel to Import Question & Answer</span></h5>
                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="main-section mb-5 p-5">

                    <div class="row">
                        <div class="col-12">
                            <p><strong>Import content for unit 1</strong></p>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Copy and Paste your data here (from Word, Excel, Google
                                    Docs, etc.)</label>
                                <textarea class="form-control custom-textarea" id="import_content_data"
                                    placeholder=""></textarea>
                            </div>
                        </div>

                        <div class="col-6">
                            <p><strong>Separate the question and its answer by</strong></p>
                            <div class="radio-box ">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="separator" id="separate_tab" value="tab" >
                                    <label class="form-check-label" for="separate_tab">Tab</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="separator" id="separate_comma" value="comma" checked>
                                    <label class="form-check-label" for="separate_comma">Comma</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="separator" value="custom">
                                        <label for="separate_custom">
                                            <input type="text" class="form-control" placeholder="-" id="separate_custom" />
                                        </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <p><strong>Separate each set(question and answer) by</strong></p>
                            <div class="radio-box ">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="separator1" id="separate_semmicolon" value="semmicolon">
                                    <label class="form-check-label" for="separate_semmicolon">Semmicolon</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="separator1" id="separate_newline" value="newline" checked>
                                    <label class="form-check-label" for="separate_newline">New Line</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="separator1"  value="custom">
                                    <label for="separate_custom1">
                                        <input type="text" class="form-control" placeholder="\n\n" id="separate_custom1" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="imported_content" class="mt-5"></div>

                </div>
                <div class="modal-footer text-center pull-right">
                    <button onclick="imported_content()"  class="btn btn-primary btn-small">Import Content</button>
                    <button type="button" class="btn btn-light btn-small" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@parent
<script>
var max_fields_limit = 8; //set limit for maximum input fields
var x = <?php echo (count($questionAnswers) > 0)?count($questionAnswers):1 ?>; //initialize counter for text box
var separator = ",";
var separator1 = "\n";

$(document).delegate('#import_content_data', 'keydown', function(e) {
    var TABKEY = 9;
    var keyCode = e.keyCode || e.which;
    if(keyCode === TABKEY) {
        this.value += "\t";
        if(e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }
});
$(document).ready(function() {

    $('#import_content_data').on('input', function(event) { 
        var data = $(this).val();
        import_content(data,"#imported_content");
    });

$('#import_content_data').attr("placeholder", "Word 1"+separator+"Definition 1"+separator1+"Word 2"+separator+"Definition 2"+separator1+"Word 3"+separator+"Definition 3");
$('input[type=radio][name=separator]').change(function(e) {
   e.preventDefault();
   if($(this).val() == "tab") {
       separator = "\t";
   } else if($(this).val() == "comma") {
       separator = ",";
   }
   else if($(this).val() == "custom") {
       if($("#separate_custom").val() == undefined)
           separator = "-";
       else
           separator = $("#separate_custom").val();
   }

   $('#import_content_data').attr("placeholder", "Word 1"+separator+"Definition 1"+separator1+"Word 2"+separator+"Definition 2"+separator1+"Word 3"+separator+"Definition 3");
  
   import_content($('#import_content_data').val(),"#imported_content");
});

$('input[type=radio][name=separator1]').change(function(e) {
   e.preventDefault();
   if($(this).val() == "semmicolon") {
       separator1 = ";";
   } else if($(this).val() == "newline") {
       separator1 = "\n";
   }
   else if($(this).val() == "custom") {
       if($("#separate_custom1").val() == undefined)
           separator1 = "\n\n";
       else
           separator1 = $("#separate_custom1").val();
   }

   $('#import_content_data').attr("placeholder", "Word 1"+separator+"Definition 1"+separator1+"Word 2"+separator+"Definition 2"+separator1+"Word 3"+separator+"Definition 3");
 
   import_content($('#import_content_data').val(),"#imported_content");
});

$('.input_fields_container_part').on("click",".remove_field", function(e){ //user click on remove text links
    e.preventDefault(); 
    if($('.dragitem').length > 1) {
        $(this).parent().parent('div').remove();
        x--;
    }
    $('.dragitem').each(function(index, el){
        $(el).children('div.que-ans-box').children('.box-number').html(index+1);
    });
})

$('#unit_qa_form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '{{ route("admin.units.store") }}',
            type: "POST",
            data: $('#unit_qa_form').serialize(),
            success: function (response_sub) {
                if (response_sub.status == true) {
                    alert("Updated successfully.");
                }
            }
        });
        
        return false;
    });

    $(".input_fields_container_part").sortable({
        placeholder: "ui-state-highlight",
        // helper: 'clone',
        sort: function(e, ui) {
            // $(ui.placeholder).html(Number($(".input_fields_container_part > div:visible").index(ui.placeholder)) + 1);
        },
        update: function(event, ui) {
            var $lis = $(this).children('.dragitem');
            $lis.each(function() {
                var $li = $(this);
                var newVal = $(this).index() + 1;
                // $(this).children('div.que-ans-box').children('.box-number').html(newVal);
                // console.log("position",$(this).children('.dragitem').children('.box-number').html());
            // $(this).children('#item_display_order').val(newVal);
            });
        },
        stop: function(event, ui) {
            var selectedData = new Array();
            $('.dragitem').each(function() {
                var $li = $(this);
                var newVal = $(this).index() + 1;
                var uid = $(this).children('div.que-ans-box').children('.box-number').attr("id");
                $(this).children('div.que-ans-box').children('.qa_sort_order').val(newVal);
                selectedData.push(uid);
                $(this).children('div.que-ans-box').children('.box-number').html(newVal);
                
                // console.log("aa",$(this).children('div.que-ans-box'));
            });
            // console.log("New position: " + ui.item.index() + 1);
            updateOrder(selectedData);
            console.log("position", selectedData );
        }
    });
    $( ".input_fields_container_part" ).disableSelection();

});

function updateOrder(data) {
        $.ajax({
            url:"{{ route("admin.question-answers.sort_order") }}",
            type:'post',
            data:{position:data},
            success:function(){
                // alert('your change successfully saved');
            }
        })
    }

function add_more_button(){
     x++; //counter increment
    // $('.input_fields_container_part').append('<div class="dragitem ui-state-default"> <div class="que-ans-box"><input type="hidden" name="qa_sort_order[]" value="'+x+'"/> <span class="ui-state-default box-number">'+x+'</span> <span class="acc-btn"><i class="fa fa-bars" aria-hidden="true"></i></span> <span class="trash-icon text-danger remove_field"><i class="fa fa-trash" aria-hidden="true"></i></span> <div class="action-list"><span><i class="fa fa-font" aria-hidden="true"></i></span><span><img src="{{asset('img/ux-design.svg')}}"/></span><span><i class="fa fa-microphone" aria-hidden="true"></i></span></div><p><span class="badge badge-warning">Question</span><span><input class="col-10" type="text" name="question[]" id="question" required/> </span></p><div class="media"> <span class="badge badge-primary">Answer</span> <div class="media-body"> <input class="col-11" type="text" name="answer[]" id="answer" style="height:100px" required/> </div><img class="ml-3" src="{{asset('img/profile.jpg')}}" width="128" height="108" alt="Generic placeholder image"> </div></div><div class="text-right"><a href="javascript:void(0);" class="addnew text-warning add_more_button" onclick="add_more_button()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Card</a></div></div>'); 
    $('.input_fields_container_part').append(
                    '<div class="dragitem ui-state-default"> <div class="que-ans-box"><input type="hidden" class="qa_sort_order" name="qa_sort_order[]" value="'+x+'"/> <span class="ui-state-default box-number">' + x + '</span> <span class="acc-btn"><i class="fa fa-bars" aria-hidden="true"></i></span> <span class="trash-icon text-danger remove_field"><i class="fa fa-trash" aria-hidden="true"></i></span> <div class="action-list"><span><i class="fa fa-font" aria-hidden="true"></i></span><span><img src="{{asset('img/ux-design.svg ')}}"/></span><span><i class="fa fa-microphone" aria-hidden="true"></i></span></div><div class="media"> <span class="badge badge-warning">Question</span> <div class="media-body"> <textarea class="form-control unit-textarea" placeholder="" name="question[]" required></textarea> </div></div><br><div class="media"> <span class="badge badge-primary">Answer</span> <div class="media-body"> <textarea class="form-control unit-textarea" placeholder="" name="answer[]" required></textarea> </div></div></div><div class="text-right"><a href="javascript:void(0);" class="addnew text-warning add_more_button" onclick="add_more_button()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Card</a></div></div>'
                    );
}

function import_content(data,id){
    var rows = data.split(separator1);
    var xx = 0;
    if(id == ".input_fields_container_part")
        xx = x;
    var table = "";
    for(var y in rows) {
        var results = rows[y].split(separator);
        var question = results[0];
        var answer = results[1] == undefined?"":results[1];
        table += '<div class="dragitem ui-state-default"> <div class="que-ans-box"><input type="hidden" class="qa_sort_order" name="qa_sort_order[]" value="'+((parseInt(y)+1) + xx)+'"/> <span class="ui-state-default box-number">' + ((parseInt(y)+1) + xx) + '</span> <span class="acc-btn"><i class="fa fa-bars" aria-hidden="true"></i></span> <span class="trash-icon text-danger remove_field"><i class="fa fa-trash" aria-hidden="true"></i></span> <div class="action-list"><span><i class="fa fa-font" aria-hidden="true"></i></span><span><img src="{{asset('img/ux-design.svg ')}}"/></span><span><i class="fa fa-microphone" aria-hidden="true"></i></span></div><div class="media"> <span class="badge badge-warning">Question</span> <div class="media-body"> <textarea class="form-control unit-textarea" placeholder="" name="question[]" required>'+question+'</textarea> </div></div><br><div class="media"> <span class="badge badge-primary">Answer</span> <div class="media-body"><textarea class="form-control unit-textarea" placeholder="" name="answer[]" required>'+answer+'</textarea> </div></div></div><div class="text-right"><a href="javascript:void(0);" class="addnew text-warning add_more_button" onclick="add_more_button()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Card</a></div></div>';
             
    }
    // Insert into DOM
    if(id == ".input_fields_container_part")
        $(id).append(table);
    else
        $(id).html(table);
}


function imported_content(){
    import_content($('#import_content_data').val(),'.input_fields_container_part');
    $('#importModal').modal('hide');
}

Dropzone.options.imageDropzone = {
    url: '{{ route('admin.units.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="image"]').remove()
      $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="image"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($unit) && $unit->image)
      var file = {!! json_encode($unit->image) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $unit->image->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}

</script>
@endsection