@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.questionAnswer.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.question-answers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.id') }}
                        </th>
                        <td>
                            {{ $questionAnswer->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.unit') }}
                        </th>
                        <td>
                            {{ $questionAnswer->unit->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.language') }}
                        </th>
                        <td>
                            {{ $questionAnswer->language->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.question_type') }}
                        </th>
                        <td>
                            {{ $questionAnswer->question_type->type ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.question') }}
                        </th>
                        <td>
                            {{ $questionAnswer->question }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.option') }}
                        </th>
                        <td>
                            {{ $questionAnswer->option }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.answer') }}
                        </th>
                        <td>
                            {{ $questionAnswer->answer }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.image') }}
                        </th>
                        <td>
                            @if($questionAnswer->image)
                                <a href="{{ $questionAnswer->image->getUrl() }}" target="_blank">
                                    <img src="{{ $questionAnswer->image->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.sort_order') }}
                        </th>
                        <td>
                            {{ $questionAnswer->sort_order }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.status') }}
                        </th>
                        <td>
                            {{ App\QuestionAnswer::STATUS_SELECT[$questionAnswer->status] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.is_active') }}
                        </th>
                        <td>
                            {{ App\QuestionAnswer::IS_ACTIVE_RADIO[$questionAnswer->is_active] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.questionAnswer.fields.modified_by') }}
                        </th>
                        <td>
                            {{ $questionAnswer->modified_by->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.question-answers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>


    </div>
</div>
@endsection