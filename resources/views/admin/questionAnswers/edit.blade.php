@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.questionAnswer.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.question-answers.update", [$questionAnswer->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="unit_id">{{ trans('cruds.questionAnswer.fields.unit') }}</label>
                <select class="form-control select2 {{ $errors->has('unit') ? 'is-invalid' : '' }}" name="unit_id" id="unit_id" required>
                    @foreach($units as $id => $unit)
                        <option value="{{ $id }}" {{ ($questionAnswer->unit ? $questionAnswer->unit->id : old('unit_id')) == $id ? 'selected' : '' }}>{{ $unit }}</option>
                    @endforeach
                </select>
                @if($errors->has('unit_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('unit_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.unit_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="language_id">{{ trans('cruds.questionAnswer.fields.language') }}</label>
                <select class="form-control select2 {{ $errors->has('language') ? 'is-invalid' : '' }}" name="language_id" id="language_id" required>
                    @foreach($languages as $id => $language)
                        <option value="{{ $id }}" {{ ($questionAnswer->language ? $questionAnswer->language->id : old('language_id')) == $id ? 'selected' : '' }}>{{ $language }}</option>
                    @endforeach
                </select>
                @if($errors->has('language_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('language_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.language_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="question_type_id">{{ trans('cruds.questionAnswer.fields.question_type') }}</label>
                <select class="form-control select2 {{ $errors->has('question_type') ? 'is-invalid' : '' }}" name="question_type_id" id="question_type_id" required>
                    @foreach($question_types as $id => $question_type)
                        <option value="{{ $id }}" {{ ($questionAnswer->question_type ? $questionAnswer->question_type->id : old('question_type_id')) == $id ? 'selected' : '' }}>{{ $question_type }}</option>
                    @endforeach
                </select>
                @if($errors->has('question_type_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question_type_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.question_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="question">{{ trans('cruds.questionAnswer.fields.question') }}</label>
                <textarea class="form-control {{ $errors->has('question') ? 'is-invalid' : '' }}" name="question" id="question" required>{{ old('question', $questionAnswer->question) }}</textarea>
                @if($errors->has('question'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.question_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="option">{{ trans('cruds.questionAnswer.fields.option') }}</label>
                <textarea class="form-control {{ $errors->has('option') ? 'is-invalid' : '' }}" name="option" id="option">{{ old('option', $questionAnswer->option) }}</textarea>
                @if($errors->has('option'))
                    <div class="invalid-feedback">
                        {{ $errors->first('option') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.option_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="answer">{{ trans('cruds.questionAnswer.fields.answer') }}</label>
                <input class="form-control {{ $errors->has('answer') ? 'is-invalid' : '' }}" type="text" name="answer" id="answer" value="{{ old('answer', $questionAnswer->answer) }}" required>
                @if($errors->has('answer'))
                    <div class="invalid-feedback">
                        {{ $errors->first('answer') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.answer_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="image">{{ trans('cruds.questionAnswer.fields.image') }}</label>
                <div class="needsclick dropzone {{ $errors->has('image') ? 'is-invalid' : '' }}" id="image-dropzone">
                </div>
                @if($errors->has('image'))
                    <div class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.image_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sort_order">{{ trans('cruds.questionAnswer.fields.sort_order') }}</label>
                <input class="form-control {{ $errors->has('sort_order') ? 'is-invalid' : '' }}" type="number" name="sort_order" id="sort_order" value="{{ old('sort_order', $questionAnswer->sort_order) }}" step="1" required>
                @if($errors->has('sort_order'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sort_order') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.sort_order_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.questionAnswer.fields.status') }}</label>
                <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" required>
                    <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\QuestionAnswer::STATUS_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('status', $questionAnswer->status) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('status') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.questionAnswer.fields.is_active') }}</label>
                @foreach(App\QuestionAnswer::IS_ACTIVE_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('is_active') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="is_active_{{ $key }}" name="is_active" value="{{ $key }}" {{ old('is_active', $questionAnswer->is_active) === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="is_active_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('is_active'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_active') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.is_active_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="modified_by_id">{{ trans('cruds.questionAnswer.fields.modified_by') }}</label>
                <select class="form-control select2 {{ $errors->has('modified_by') ? 'is-invalid' : '' }}" name="modified_by_id" id="modified_by_id" required>
                    @foreach($modified_bies as $id => $modified_by)
                        <option value="{{ $id }}" {{ ($questionAnswer->modified_by ? $questionAnswer->modified_by->id : old('modified_by_id')) == $id ? 'selected' : '' }}>{{ $modified_by }}</option>
                    @endforeach
                </select>
                @if($errors->has('modified_by_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('modified_by_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.questionAnswer.fields.modified_by_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>


    </div>
</div>
@endsection

@section('scripts')
<script>
    Dropzone.options.imageDropzone = {
    url: '{{ route('admin.question-answers.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="image"]').remove()
      $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="image"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($questionAnswer) && $questionAnswer->image)
      var file = {!! json_encode($questionAnswer->image) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $questionAnswer->image->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection