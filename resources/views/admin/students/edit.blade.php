@extends('layouts.adminpanel')
@section('content')

<div class="card" style="margin-top: 60px;">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.student.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.students.update", [$student->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="studentid">{{ trans('cruds.student.fields.studentid') }}</label>
                <input class="form-control {{ $errors->has('studentid') ? 'is-invalid' : '' }}" type="text" name="studentid" id="studentid" value="{{ old('studentid', $student->studentid) }}" required>
                @if($errors->has('studentid'))
                    <div class="invalid-feedback">
                        {{ $errors->first('studentid') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.studentid_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.student.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $student->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="dob">{{ trans('cruds.student.fields.dob') }}</label>
                <input class="form-control date {{ $errors->has('dob') ? 'is-invalid' : '' }}" type="text" name="dob" id="dob" value="{{ old('dob', $student->dob) }}" required>
                @if($errors->has('dob'))
                    <div class="invalid-feedback">
                        {{ $errors->first('dob') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.dob_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.student.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email', $student->email) }}" required>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="board_id">{{ trans('cruds.student.fields.board') }}</label>
                <select class="form-control select2 {{ $errors->has('board') ? 'is-invalid' : '' }}" name="board_id" id="board_id" required>
                    @foreach($boards as $id => $board)
                        <option value="{{ $id }}" {{ ($student->board ? $student->board->id : old('board_id')) == $id ? 'selected' : '' }}>{{ $board }}</option>
                    @endforeach
                </select>
                @if($errors->has('board_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('board_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.board_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="class_id">{{ trans('cruds.student.fields.class') }}</label>
                <select class="form-control select2 {{ $errors->has('class') ? 'is-invalid' : '' }}" name="class_id" id="class_id" required>
                    @foreach($classes as $id => $class)
                        <option value="{{ $id }}" {{ ($student->class ? $student->class->id : old('class_id')) == $id ? 'selected' : '' }}>{{ $class }}</option>
                    @endforeach
                </select>
                @if($errors->has('class_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('class_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.class_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="current_address">{{ trans('cruds.student.fields.current_address') }}</label>
                <input class="form-control {{ $errors->has('current_address') ? 'is-invalid' : '' }}" type="text" name="current_address" id="current_address" value="{{ old('current_address', $student->current_address) }}">
                @if($errors->has('current_address'))
                    <div class="invalid-feedback">
                        {{ $errors->first('current_address') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.current_address_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="city_id">{{ trans('cruds.student.fields.city') }}</label>
                <select class="form-control select2 {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city_id" id="city_id">
                    @foreach($cities as $id => $city)
                        <option value="{{ $id }}" {{ ($student->city ? $student->city->id : old('city_id')) == $id ? 'selected' : '' }}>{{ $city }}</option>
                    @endforeach
                </select>
                @if($errors->has('city_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('city_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.city_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="state_id">{{ trans('cruds.student.fields.state') }}</label>
                <select class="form-control select2 {{ $errors->has('state') ? 'is-invalid' : '' }}" name="state_id" id="state_id">
                    @foreach($states as $id => $state)
                        <option value="{{ $id }}" {{ ($student->state ? $student->state->id : old('state_id')) == $id ? 'selected' : '' }}>{{ $state }}</option>
                    @endforeach
                </select>
                @if($errors->has('state_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('state_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.state_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="country_id">{{ trans('cruds.student.fields.country') }}</label>
                <select class="form-control select2 {{ $errors->has('country') ? 'is-invalid' : '' }}" name="country_id" id="country_id">
                    @foreach($countries as $id => $country)
                        <option value="{{ $id }}" {{ ($student->country ? $student->country->id : old('country_id')) == $id ? 'selected' : '' }}>{{ $country }}</option>
                    @endforeach
                </select>
                @if($errors->has('country_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('country_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.country_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.student.fields.is_active') }}</label>
                @foreach(App\Student::IS_ACTIVE_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('is_active') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="is_active_{{ $key }}" name="is_active" value="{{ $key }}" {{ old('is_active', $student->is_active) === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="is_active_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('is_active'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_active') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.is_active_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="modified_by_id">{{ trans('cruds.student.fields.modified_by') }}</label>
                <select class="form-control select2 {{ $errors->has('modified_by') ? 'is-invalid' : '' }}" name="modified_by_id" id="modified_by_id" required>
                    @foreach($modified_bies as $id => $modified_by)
                        <option value="{{ $id }}" {{ ($student->modified_by ? $student->modified_by->id : old('modified_by_id')) == $id ? 'selected' : '' }}>{{ $modified_by }}</option>
                    @endforeach
                </select>
                @if($errors->has('modified_by_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('modified_by_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.student.fields.modified_by_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>


    </div>
</div>
@endsection