@extends('layouts.adminpanel')
@section('content')
@can('student_create')
    <!-- <div style="margin-top: 60px; margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.students.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.student.title_singular') }}
            </a>
        </div>
    </div> -->
@endcan
<div class="card" style="margin-top: 60px;">
    <div class="card-header">
        {{ trans('cruds.student.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Student">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.student.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.studentid') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.name') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.dob') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.email') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.board') }}
                    </th>
                    <th>
                        {{ trans('cruds.board.fields.code') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.class') }}
                    </th>
                    <th>
                        {{ trans('cruds.myClass.fields.code') }}
                    </th>
                    <!-- <th>
                        {{ trans('cruds.student.fields.current_address') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.city') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.state') }}
                    </th>
                    <th>
                        {{ trans('cruds.student.fields.country') }}
                    </th> -->
                    <th>
                        {{ trans('cruds.student.fields.is_active') }}
                    </th>
                    <!-- <th>
                        {{ trans('cruds.student.fields.modified_by') }}
                    </th> -->
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('student_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.students.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.students.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'studentid', name: 'studentid' },
{ data: 'name', name: 'name' },
{ data: 'dob', name: 'dob' },
{ data: 'email', name: 'email' },
{ data: 'board_name', name: 'board.name' },
{ data: 'board.code', name: 'board.code' },
{ data: 'class_name', name: 'class.name' },
{ data: 'class.code', name: 'class.code' },
// { data: 'current_address', name: 'current_address' },
// { data: 'city_name', name: 'city.name' },
// { data: 'state_name', name: 'state.name' },
// { data: 'country_name', name: 'country.name' },
{ data: 'is_active', name: 'is_active' },
// { data: 'modified_by_name', name: 'modified_by.name' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  $('.datatable-Student').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});

</script>
@endsection