@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.myClass.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.my-classes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.id') }}
                        </th>
                        <td>
                            {{ $myClass->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.board') }}
                        </th>
                        <td>
                            {{ $myClass->board->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.code') }}
                        </th>
                        <td>
                            {{ $myClass->code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.name') }}
                        </th>
                        <td>
                            {{ $myClass->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.description') }}
                        </th>
                        <td>
                            {{ $myClass->description }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.image') }}
                        </th>
                        <td>
                            @if($myClass->image)
                                <a href="{{ $myClass->image->getUrl() }}" target="_blank">
                                    <img src="{{ $myClass->image->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.status') }}
                        </th>
                        <td>
                            {{ App\MyClass::STATUS_SELECT[$myClass->status] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.is_active') }}
                        </th>
                        <td>
                            {{ App\MyClass::IS_ACTIVE_RADIO[$myClass->is_active] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.myClass.fields.modified_by') }}
                        </th>
                        <td>
                            {{ $myClass->modified_by->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.my-classes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>


    </div>
</div>
@endsection