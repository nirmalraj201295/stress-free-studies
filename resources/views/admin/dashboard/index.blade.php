@extends('layouts.adminpanel')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-3">
                    <div class="card-box dashboard_card_box">
                        <div class="row">
                            <div class="col-3 icon"><i class="fas fa-chalkboard" aria-hidden="true"></i></div>
                            <div class="col-8">
                                <div class="board-name">{{ $settings1['chart_title'] }}</div>
                                <div class="class-count">{{ number_format($settings1['total_number']) }}</div>
                            </div>
                        </div>
                        <div class="board-name"><a href="{{ route('admin.boards.index') }}">{{ $settings1['chart_link'] }}<span><i class="fa fa-arrow-right pull-right" aria-hidden="true"></i></span></a></div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="card-box dashboard_card_box">
                        <div class="row">
                            <div class="col-3 icon red"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                            <div class="col-8">
                                <div class="board-name">{{ $settings2['chart_title'] }}</div>
                                <div class="class-count">{{ number_format($settings2['total_number']) }}</div>
                            </div>
                        </div>
                        <div class="board-name"><a href="{{ route('admin.my-classes.index') }}">{{ $settings2['chart_link'] }}<span><i class="fa fa-arrow-right pull-right" aria-hidden="true"></i></span></a></div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="card-box dashboard_card_box">
                        <div class="row">
                            <div class="col-3 icon blue"><i class="fas fa-book-open" aria-hidden="true"></i></div>
                            <div class="col-8">
                                <div class="board-name">{{ $settings3['chart_title'] }}</div>
                                <div class="class-count">{{ number_format($settings3['total_number']) }}</div>
                            </div>
                        </div>
                        <div class="board-name"><a href="{{ route('admin.subjects.index') }}">{{ $settings3['chart_link'] }}<span><i class="fa fa-arrow-right pull-right" aria-hidden="true"></i></span></a></div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="card-box dashboard_card_box">
                        <div class="row">
                            <div class="col-3 icon yellow"><i class="fa fa-file-alt" aria-hidden="true"></i></div>
                            <div class="col-8">
                                <div class="board-name">{{ $settings4['chart_title'] }}</div>
                                <div class="class-count">{{ number_format($settings4['total_number']) }}</div>
                            </div>
                        </div>
                        <div class="board-name"><a href="{{ route('admin.chapters.index') }}">{{ $settings4['chart_link'] }}<span><i class="fa fa-arrow-right pull-right" aria-hidden="true"></i></span></a></div>
                    </div>
                </div>

                <!-- <div class="col-3">
                    <div class="card-box">
                        <div class="board-name"><div class="class-count">{{ number_format($settings2['total_number']) }}</div>{{ $settings2['chart_title'] }}</div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="card-box">
                        <div class="board-name"><div class="class-count">{{ number_format($settings3['total_number']) }}</div>{{ $settings3['chart_title'] }}</div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="card-box">
                        <div class="board-name"><div class="class-count">{{ number_format($settings4['total_number']) }}</div>{{ $settings4['chart_title'] }}</div>
                    </div>
                </div>
            </div> -->
    
            <!-- <div class="card">
                @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <div class="card-body">
                <div class="row">
                   
                </div>

                   

                </div>
            </div> -->
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
@endsection