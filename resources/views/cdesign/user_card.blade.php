<?php 
      $total_name = "";?>
       @foreach ($data as $value)
        <div class="col-6">
                <div class="card-box">
                    
                        @if($value->table == "subjects")
                        <div id="subject_{{$value->id}}" class="center" onClick="@if(in_array($value->id, $joinSubject)) viewRedirect('{{route('user.chapters',$value->id)}}') @else assignCourse({{$value->id}});@endif">
                            <div>
                                <img src="{{ isset($value->image)?$value->image->getUrl('thumb') : asset('img/default-img.jpg') }}" width="120" height="90">
                            </div>
                            
                            <div >
                                <div class="board-name text-left">{{ $value->name }}</div>
                                <!-- <div class="board-code">{{ $value->code }}</div> -->
                            </div>
                            <div class="text-left">{{ $value->total }} Chapters</div>
                
                               <button type="submit" class="btn btn-primary btn-small" style="width:auto !important; margin-top: 10px"> @if(in_array($value->id, $joinSubject)) View @else Join @endif </button>
                        </div>
                            </button>
                            
                        @elseif($value->table == "chapters")
                        <div class="center" onClick="viewRedirect('{{route('user.units',$value->id)}}')">
                            <div class="course-img">
                                <img src="{{ isset($value->image)?$value->image->getUrl('thumb') : asset('img/default-img.jpg') }}" min-width="120" height="90">
                            </div>
                            
                            <div>
                                <div class="board-name text-left">{{ $value->name }}</div>
                                <!-- <div class="board-code">{{ $value->code }}</div> -->
                            </div>
                            <div class="class-count text-left">{{ $value->total }} Units</div>
                        </div>
                        @elseif($value->table == "units")
                        <div class="center" onClick="viewRedirect('{{route('user.qa',$value->id)}}')">
                            <div>
                                <img src="{{ isset($value->image)?$value->image->getUrl('thumb') : asset('img/default-img.jpg') }}" width="120" height="90">
                            </div>
                            
                            <div>
                                <div class="board-name text-left">{{ $value->name }}</div>
                                <!-- <div class="board-code">{{ $value->code }}</div> -->
                            </div>
                            <div class="class-count text-left">{{ $value->total }} Questions</div>
                        </div>
                        @else
                            <div class="board-name">{{ $value->name }}</div>
                            <div class="board-code">{{ $value->code }}</div>
                        @endif

                    @if($value->table == "my_classes")
                    <div class="board-type">
                        @foreach($data->boards as $id => $board)
                            {{ ($value->board->id == $id) ? $board : '' }}
                        @endforeach
                    </div>
                    @elseif($value->table == "subjects")
                        <!-- <div class="board-type">
                            <span>
                                @foreach($data->boards as $id => $board)
                                {{ ($value->board->id == $id) ? $board : '' }}
                                @endforeach
                            </span>
                            <span>
                                @foreach($data->classes as $id => $class)
                                {{ ($value->class->id == $id) ? $class : '' }}
                                @endforeach
                            </span>
                        </div> -->
                    @elseif($value->table == "chapters")
                        <!-- <div class="board-type chapter-btn">
                            <span>
                                @foreach($data->boards as $id => $board)
                                {{ ($value->board->id == $id) ? $board : '' }}
                                @endforeach
                            </span>
                            <span>
                                @foreach($data->classes as $id => $class)
                                {{ ($value->class->id == $id) ? $class : '' }}
                                @endforeach
                            </span>
                            <span>
                                @foreach($data->subjects as $id => $subject)
                                {{ ($value->subject->id == $id) ? $subject : '' }}
                                @endforeach
                            </span>
                        </div> -->
                    @endif
                </div>
            </div>
         
        @endforeach
  <!-- <script>
      
  var total_name = modalTitle();
  if(total_name == "Class" || total_name == "Subject" || total_name == "Chapter")
        $('.filterBoards').attr('hidden',false);
  if(total_name == "Subject"  || total_name == "Chapter")
        $('.filterClasses').attr('hidden',false);
  if(total_name == "Chapter")
        $('.filterSubjects').attr('hidden',false);

      $('#totalBoard').html('Total '+total_name+'  - {{$data->total()}}');
      </script> -->
      <script>
      function assignCourse(subjectId){

    	  q = 'sub=' + subjectId;
    	  clickfun = "viewRedirect('{{URL::to('chapters/')}}/"+subjectId+"')";
          $.ajax({
              // url: '{{ route('admin.boards.ajaxData') }}?page=' + page,
              url: "{{ route('user.course.assignAjaxData') }}"+'?' + q,
              type: "post",
              datatype: "html"
          }).done(function(data){
    		console.log(data['sub']);
    		if(data['status']=='success'){
    			console.log('t'+data['sub']);
        		$('#subject_'+data['sub']+' .status').text(' Joined ');
        		$('#subject_'+data['sub']).attr('onclick',clickfun);
        	}
          }).fail(function(jqXHR, ajaxOptions, thrownError){
              $('.loading').hide();
              alert('No response from server');
          });    
      } 
      </script>
{!! $data->render() !!}
