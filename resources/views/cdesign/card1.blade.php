@foreach ($data as $value)

<div class="col-4 clickable" onClick="viewRedirect('{{ route("admin.units.index") }}?id={{ $value->id }}')">
<!-- <a href="#"> -->
    <div class="card-box chapter-box">
        <!-- <span class="edit-box"><a href="javascript:void(0)" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-pencil-square-o"
                    aria-hidden="true"></i></a><a href="#"><i class="fa fa-trash"
                    aria-hidden="true"></i></a></span> -->
    <span class="text-purple text-small">{{ $value->total }} Units</span>
    <span class="text-small pull-right text-grey">{{ $value->created_at->diffForHumans() }}</span>
            <div class="media">
            <img class="align-self-start mr-3" src="{{ isset($value->image)?$value->image->getUrl('thumb') : asset('img/default-img.jpg') }}" alt="">
            <div class="media-body">
                <h5 class="mt-0">{{ $value->name }}</h5>
                <div class="board-type chapter-btn"> 
                    <span>
                        @foreach($data->boards as $id => $board)
                        {{ ($value->board->id == $id) ? $board : '' }}
                        @endforeach
                    </span>
                    <span>
                        @foreach($data->classes as $id => $class)
                        {{ ($value->class->id == $id) ? $class : '' }}
                        @endforeach
                    </span>
                    <span>
                        @foreach($data->subjects as $id => $subject)
                        {{ ($value->subject->id == $id) ? $subject : '' }}
                        @endforeach
                    </span>
                </div>
            </div>
            
            </div>
        
        <div class="chapter-content">
            <p>{{ $value->description }}</p>
        </div>
        
    </div>
    <!-- </a> -->
</div>

@endforeach
{!! $data->render() !!}