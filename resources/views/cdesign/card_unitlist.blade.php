@foreach ($data as $value)

<div class="unit-row clickable">
    <span class="unit-id" hidden id="{{ $value->id }}"></span>
    <div class="row">
        <div class="col-4">
            <img src="../img/default-img.jpg" class="row-img">
            <span class="unit-title">{{ $value->name }}</span>
        </div>
        <div class="col-1">
            <span class="top-text">{{ $value->total }}</span>
            <span class="bottom-text">Questions</span>
        </div>
        <div class="col-2">
            <span class="top-text">{{ date('d-M-Y h:m', strtotime($value->created_at)) }}</span>
            <span class="bottom-text">Created On</span>
        </div>
        <div class="col-2">
            <span class="top-text">{{ date('d-M-Y h:m', strtotime($value->updated_at)) }}</span>
            <span class="bottom-text">Last Modified</span>
        </div>
        <div class="col-2">
            <span class="top-text">{{ $value->username }}</span>
            <span class="bottom-text">Modified by</span>
        </div>
        <div class="col-1">
            <a href="#" class="drag-icon" onClick="viewRedirect('{{ route("admin.question-answers.index") }}?id={{ $value->chapter->id }}&uid={{$value->id}}')">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
            <a href="#" class="drag-icon"><i class="fa fa-arrows" aria-hidden="true"></i></a>
            <a href="#" href="javascript:void(0)" onclick="deleteModal({{ $value->id }})"
                class="delete-icon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </div>
    </div>
</div>

@endforeach 
<script>
    $('#total_unit').html('({{$data->total()}})');
</script>
{!! $data->render() !!}