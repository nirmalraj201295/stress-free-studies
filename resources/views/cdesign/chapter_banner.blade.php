<section class="banner-section" style="margin: -20px -15px auto">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-6">
                            <a href="javascript:history.back()" class="text-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Chapters list</a>
                        </div>
                        <div class="col-6 text-right">
                            <!-- <a href="#" class="text-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                Chapter</a> -->
                        </div>
                     </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="media">
                                <img class="align-self-start mr-3" width="153" height="169" src="{{ asset('img/default-img.jpg') }}"
                                    alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">{{ $chapter->name }}</h5>
                                    <p>{{ $chapter->description }}</p>
                                    <a href="#" class="text-warning">Show more</a>
                                    <ul class="media-list">
                                    <li>
                                    @foreach($boards as $id => $board)
                                         {{ ($chapter->board_id == $id) ? $board : '' }}
                                    @endforeach 
                                    </li>
                                        <li>@foreach($classes as $id => $class)
                                         {{ ($chapter->class_id == $id) ? $class : '' }}
                                    @endforeach </li>
                                        <li>@foreach($subjects as $id => $subject)
                                         {{ ($chapter->subject_id == $id) ? $subject : '' }}
                                    @endforeach</li>
                                    </ul>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </section>