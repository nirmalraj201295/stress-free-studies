<?php 
      $total_name = "";?>
       @foreach ($data as $value)
        <div class="col-3">
                <div class="card-box">
                    <span class="edit-box"><a href="#" onClick="editModal({{$value->id}})"><i class="fa fa-pencil-square-o"
                                aria-hidden="true"></i></a><a href="#" onClick="deleteData({{$value->id}})"><i class="fa fa-trash"
                                aria-hidden="true"></i></a></span>
                    <div class="class-count">{{ $value->total }} 
                        @if($value->table == "boards")
                            Classes
                        @elseif($value->table == "my_classes")
                            Subjects
                        @elseif($value->table == "subjects")
                            Chapters
                        @elseif($value->table == "chapters")
                            Units
                        @endif
                        </div>
                        @if($value->table == "subjects")
                        <div class="row align-items-end">
                                <div class="col">
                                    <div class="board-name">{{ $value->name }}</div>
                                    <div class="board-code">{{ $value->code }}</div>
                                </div>
                                <div class="col text-right">
                                    <img src="{{ isset($value->image)?$value->image->getUrl('thumb') : asset('img/default-img.jpg') }}" width="94" height="94"/>
                                </div>
                         </div>
                        @elseif($value->table == "chapters")
                        <div class="row align-items-start">
                            <div class="col">
                                <div class="board-name">{{ $value->name }}</div>
                                <div class="board-code">{{ $value->code }}</div>
                            </div>
                            <div class="col text-right">
                            <img src="{{ isset($value->image)?$value->image->getUrl('thumb') : asset('img/default-img.jpg') }}" width="86" height="86">
                            </div>
                        </div>
                        @else
                            <div class="board-name">{{ $value->name }}</div>
                            <div class="board-code">{{ $value->code }}</div>
                        @endif

                    @if($value->table == "my_classes")
                    <div class="board-type">
                        @foreach($data->boards as $id => $board)
                            {{ ($value->board->id == $id) ? $board : '' }}
                        @endforeach
                    </div>
                    @elseif($value->table == "subjects")
                        <div class="board-type">
                            <span>
                                @foreach($data->boards as $id => $board)
                                {{ ($value->board->id == $id) ? $board : '' }}
                                @endforeach
                            </span>
                            <span>
                                @foreach($data->classes as $id => $class)
                                {{ ($value->class->id == $id) ? $class : '' }}
                                @endforeach
                            </span>
                        </div>
                    @elseif($value->table == "chapters")
                        <div class="board-type chapter-btn">
                            <span>
                                @foreach($data->boards as $id => $board)
                                {{ ($value->board->id == $id) ? $board : '' }}
                                @endforeach
                            </span>
                            <span>
                                @foreach($data->classes as $id => $class)
                                {{ ($value->class->id == $id) ? $class : '' }}
                                @endforeach
                            </span>
                            <span>
                                @foreach($data->subjects as $id => $subject)
                                {{ ($value->subject->id == $id) ? $subject : '' }}
                                @endforeach
                            </span>
                        </div>
                    @endif
                </div>
            </div>
         
        @endforeach
  <script>
      
  var total_name = modalTitle();
  if(total_name == "Class" || total_name == "Subject" || total_name == "Chapter")
        $('.filterBoards').attr('hidden',false);
  if(total_name == "Subject"  || total_name == "Chapter")
        $('.filterClasses').attr('hidden',false);
  if(total_name == "Chapter")
        $('.filterSubjects').attr('hidden',false);

      $('#totalBoard').html('Total '+total_name+'  - {{$data->total()}}');
      </script>
{!! $data->render() !!}
