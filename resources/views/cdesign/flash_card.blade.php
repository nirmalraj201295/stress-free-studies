@php
$i =1;
$arralength = count($data);
@endphp
@foreach ($data as $value)
<div class="flipper col-sm-12 flashCard">
    <div class="card qus-{{$i}}" style="min-height: 320px !important;">
        <div class="front {{ $value->id}}">
            <!-- <p>
            <img src="http://dummyimage.com/150x110/999/fff">
            </p> -->
            <div class="question-{{$i}} text-justify" style="padding: 5em !important">
            <p>{{ $value->question }}</p>
            </div>
            <div class="Timerparent d-none"><span class="Timer"></span><span class="Timer1">s</span></div>
            <input type="hidden" id="{{ $value->id}}" value="{{ $value->unit_id}}">
            <!-- <button class="btn btn-default bottom" type="submit">Learn More</button> -->
        </div>
        <div class="back text-justify" style="padding: 5em !important">
            <p>{{ $value->answer }}</p>
            <!-- <p>here is some information on the back of card one. I would like to <a href="#">link some text</a> for people to follow on the back of the card - but the click action messes that up.</p> -->
            <!-- <div class="goback">
            <p>back</p>
            </div> -->
        </div>
    </div> 
    <div class="col-sm-12 text-center"><span style="font-size: 14px;"> Tap anywhere in the card to flip </span></div>      
</div>
@php
$i++;
@endphp
@endforeach
<div class="col-sm-12 footer" style="position:fixed;bottom:7px;">
<div style="position:relative;margin-top:5%;" class="col-sm-12">
            <!-- <div class="col-md-12 text-center">
                <button type="button" id="pagination" class="w-100 btn btn-primary btn-sm"></button>
            </div> -->
</div>
    <div style="position:relative;margin-top:2%;" class="row">
         <div class="col-md-12 text-center">
             <div class="submit col-md-12">
                <button type="button" class="studyAgain btn btn-danger btn-sm" value=""><i class="fas fa-book-open"></i> Study Again</button>
                <button type="button" class="gotIt btn btn-success btn-sm" value="" >Got it <i class="fa fa-lightbulb-o" aria-hidden="true"></i></button>
             </div>
            </div>
   </div>
</div>   
<div class="nextCircle d-none">
            <i class="fa fa-arrow-right next" aria-hidden="true"></i>
        </div>
        <div class="prevCircle d-none">
            <i class="fa fa-arrow-left previous" aria-hidden="true"></i>
        </div>
<script>
var submittedCard = new Array();
$(document).ready(function(){
	setButtonValue('1');
	heigthsetting("1");
});

function heigthsetting(val){
	var height = $('.card .question-'+val).outerHeight();
	var screenheight = ($( window ).height());
	var maxheight = screenheight-($('.container').height())-($('.footer').height())-($('.mobile-app-icon-bar').height()) - 35;
	console.log(screenheight-($('.container').height())-($('.footer').height())-($('.mobile-app-icon-bar').height()));
	console.log($('.sliderContainer').height());
	console.log($('.container').height());
	console.log($('.footer').height());
	console.log($('.mobile-app-icon-bar').height());
	if(height>maxheight){
		height=maxheight;
		$('.card.qus-'+val).css('height',height+'px');
		$('.card .question-'+val).css('height',height+'px');
		$('.card .question-'+val).css('overflow-y','scroll');
	}else{
		$('.card.qus-'+val).css('height',maxheight+'px');
	}
	//alert($('.card .question-'+val).outerHeight()+' '+maxheight);
	
	}

function setButtonValue(cardNum){
	var qusId = $('.qus-'+cardNum+' .front input').attr('id');
	$('.submit .studyAgain').val(qusId);
	$('.submit .gotIt').val(qusId);
	var pagination = cardNum+" of "+$('.flashCard').length;
	$('button#pagination').text(pagination)
	if($.inArray(cardNum, submittedCard)!== -1){
		$('.submit button').attr('disabled', true);
	}else{
		$('.submit button').attr('disabled', false);
	}	
} 

$(".flipper").click(function() {
        var target = $( event.target );
        if ( target.is("a") ) {
            //follow that link
            return true;
        } else {
            $(this).toggleClass("flip");
        }
        return false;
    });

    var i= 0;
	//when the next button is clicked on
	$('.next').on("click", function(){
		//increase the display picture index by 1
		i = i + 1;
		//alert(i);
		//if we are at the end of the image queue, set the index back to 0
		if (i == $('.flashCard').length) {
			i=0;
		}
		setButtonValue(i+1);
		//set current image and previous image
		var currentImg = $('.flashCard').eq(i);
		var prevImg = $('.flashCard').eq(i-1);
		//call function to animate the rotation of the images to the right
		animateImage(prevImg, currentImg);	
		heigthsetting(i+1);
	});
	
	//when the previous button is clicked on
	$('.previous').on("click", function(){
		//if we are at the beginning of the image queue, set the previous image to the first image and the current image to the last image of the queue
		if (i==0) {	
			prevImg = $('.flashCard').eq(0);
			i=$('.flashCard').length-1;
			currentImg = $('.flashCard').eq(i);
			setButtonValue(i+1);
		}
		//decrease the display picture index by 1
		else {
			i=i-1;
			setButtonValue(i+1);
			//set current and previous images
			currentImg = $('.flashCard').eq(i);
			prevImg = $('.flashCard').eq(i+1);
		}
		//call function to animate the rotation of the images to the left
		animateImageLeft(prevImg, currentImg);
		heigthsetting(i+1);	
	});
	//function to animate the rotation of the images to the left
	function animateImageLeft(prevImg, currentImg) {
		//move the image to be displayed off the visible container to the right
		currentImg.css({"left":"-100%"});
		//slide the image to be displayed from off the container onto the visible container to make it slide from the right to left
		currentImg.animate({"left":"0%"}, 1000);
		//slide the previous image off the container from right to left
		prevImg.animate({"left":"100%"}, 1000);
	}
	//function to animate the rotation of the images to the right
	function animateImage(prevImg, currentImg) {
		//move the image to be displayed off the container to the left
		currentImg.css({"left":"100%"});
		//slide the image to be displayed from off the container onto the container to make it slide from left to right
		currentImg.animate({"left":"0%"}, 1000);
		//slide the image from on the container to off the container to make it slide from left to right
		prevImg.animate({"left":"-100%"}, 1000);	
	}
	var qalogId = 0;
	var questionId;
	$('.submit .studyAgain').on("click",function(e){
		//alert('test');
		questionId = $(this).val();
		flashcard('studyAgain');
	});
	$('.submit .gotIt').on("click",function(e){
		questionId = $(this).val();
		flashcard('gotIt'); 
	});

	function flashcard(selection){
		var typ;
		if(selection == "studyAgain"){
			typ = 0;
		}else{
			typ = 1;
		}
		//alert(questionId);
		//console.log($('#2').val());
		var unitId = $('#'+questionId).val();
		var time = $('.'+questionId+' .Timer').text();
		q = 'q=' + questionId + '&t=' + time + '&qalog=' + qalogId + '&u=' + unitId + '&typ=' + typ;
		//console.log(q);
		$("."+questionId+" .studyAgain").attr("disabled", true);
		$("."+questionId+" .gotIt").attr("disabled", true);
		//alert($('.'+questionId+' .Timer').text());
		//alert('data');
		$.ajax({
            // url: '{{ route('admin.boards.ajaxData') }}?page=' + page,
            url: ajaxurl+'?' + q,
            type: "post",
            datatype: "html"
        }).done(function(data){
	        console.log(data);
	        $('#master').empty().text(data['master']);
	        $('#familiar').empty().text(data['familiar']);
	        $('#notfamiliar').empty().text(data['notfamiliar']);
	        //console.log(data['id']);
	        //console.log(data['qLogData']);
	        qalogId = data['id'];
            //$("#tag_container").empty().html(data);
            submittedCard.push(i+1);
            console.log(submittedCard);
            location.hash = page;
            $('.loading').hide();
            $('.nextCircle .next').click();
            $("."+questionId+" .Timerparent").empty();
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $('.loading').hide();
            alert('No response from server');
        });
	}
	var start = new Date;
	var sec;
	setInterval(function() {
		sec = Math.floor((new Date - start) / 1000);
		//time=sec.split(".");
	    $('.Timer').text(sec);
	}, 1000);
	
	$('.prevCircle').on("click",function(){
		timerfn();
	});
	$('.nextCircle').on("click",function(){
		timerfn();
	});
	
	function timerfn(){
		start = new Date;
	}
</script>