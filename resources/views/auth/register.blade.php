@extends('layouts.app')

<link href="{{ asset('css/appcustom.css') }}" rel="stylesheet" />
<link href="{{ asset('css/home-style.css') }}" rel="stylesheet" />
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="" hidden alt="IMG">
				</div>

				<form class="login100-form validate-form" method="POST" action="{{ route('athu.register.student') }}">
                    @csrf
                     <img class="logo-img" src="{{ asset('img/logo.png') }}" alt="IMG" >
                    <!-- <span class="login100-form-title p-b-20 text-purple">
						Stress Free Studies
					</span> -->
                    <span class="login100-form-title p-b-20">
						Registration
					</span>
                    @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(session('active'))
                            <div class="invalid-feedback">
                                {{ session('active') }}
                            </div>
                        @endif

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: xxx@yyy.zzz">
                        <input id="name" name="name" type="text" class="input100" required autocomplete="name" autofocus placeholder="{{ trans('global.user_name') }}" value="{{ old('name', null) }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user fa-fw" aria-hidden="true"></i>
						</span>
					</div>
                    
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: xxx@yyy.zzz">
                        <input id="username" name="email" type="text" class="input100" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <input id="password" name="password" type="password" class="input100" required placeholder="{{ trans('global.login_password') }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

                    <div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <input id="password_confirmation" name="password_confirmation" type="password" class="input100" required placeholder="{{ trans('global.login_password_confirmation') }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100 form-group">
                        <select class="input100 form-control {{ $errors->has('board') ? 'is-invalid' : '' }}" name="board_id" id="board_id" required>
                            @foreach($boards as $id => $board)
                                <option value="{{ $id }}" {{ old('board_id') == $id ? 'selected' : '' }}>{{ $board }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                       <select class="form-control {{ $errors->has('class') ? 'is-invalid' : '' }}" name="class_id" id="class_id" required>
                             <option value="">{{ trans('global.pleaseSelectClass') }}</option>
                        </select>
                    </div>
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">
                        {{ trans('global.register') }}
						</button>
					</div>

                    <div class="text-center p-t-16">
						<a class="txt2" href="{{ route('login') }}">
							Already have account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
                  
				</form>
			</div>
		</div>
	</div>
	@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function () {
	    $("#board_id").change(function(){
	    	$('.loading').show();
	        $.ajax({
	            url: "{{ route('subjects.get_by_classes') }}?board_id=" + $(this).val(),
	            method: 'GET',
	            success: function(data) {
	                $('#class_id').html(data.html);
	                $('.loading').hide();
	            }
	        });
	    });
	});
    </script>
    @endsection