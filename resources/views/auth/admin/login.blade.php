@extends('layouts.app')

<section class="admin-container">
        <div class="container-fluid">
            <div class="row  row-100">
                <div class="col-sm-6 login-banner">
                    
                    <div class="logo-text">{{ trans('panel.site_title') }}</div>
                    
                </div>
                <div class="col-sm-6 white-bg">
                    <img class="logo-img" src="{{ asset('img/logo.png') }}" alt="IMG" >
                    <div class="row">
                        <div class="col-12 col-sm-8 offset-sm-2 login-box">
                            <h3>Sign in as Admin </h3>
                            <p><strong>Enter your detail below</strong></p>
                            @if(session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <label class="sr-only" for="username">Username</label>
                                    <!-- <input type="text" class="form-control" id="username" placeholder="Username"> -->
                                    <input id="username" name="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">

                                    @if($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="pwd">Password</label>
                                    <!-- <input type="password" class="form-control" id="pwd" placeholder="Password"> -->
                                    <input id="password" name="password" type="password" id="pwd" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}">

                                    @if($errors->has('password'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="remembercheck">
                                            <label class="form-check-label" for="remembercheck">{{ trans('global.remember_me') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        @if(Route::has('password.request'))
                                            <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ trans('global.forgot_password') }}
                                            </a><br> -->
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group text-center"><button type="submit" class="btn btn-primary">{{ trans('global.login') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="footer">© 2019 {{ trans('panel.site_title') }} . <span class="pull-right">Term of use. Privacy policy</span></div>
                </div>
            </div>
        </div>
    </section>