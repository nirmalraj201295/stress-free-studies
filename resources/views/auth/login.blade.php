@extends('layouts.app')
<link href="{{ asset('css/appcustom.css') }}" rel="stylesheet" /> 
<link href="{{ asset('css/home-style.css') }}" rel="stylesheet" />

<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="" hidden alt="IMG">
				</div>

				<form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                    @csrf

                    <img class="logo-img" src="{{ asset('img/logo.png') }}" alt="IMG" >
                   <!-- <span class="login100-form-title p-b-20 text-purple">
						Stress Free Studies
					</span> -->
                    <span class="login100-form-title p-b-20">
						Login
					</span>
                    @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-danger">{{ session('message') }}</div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            @if(is_array(session()->get('success')))
                            <ul>
                                @foreach (session()->get('success') as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                            @else
                                {{ session()->get('success') }}
                            @endif
                        </div>
                    @endif
                    
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: xxx@yyy.zzz">
                        <input id="username" name="email" type="text" class="input100" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <input id="password" name="password" type="password" class="input100" required placeholder="{{ trans('global.login_password') }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">
							Login
						</button>
					</div>
                    @if(Route::has('password.request'))
					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="{{ route('password.request') }}">
							Username / Password?
						</a>
					</div>
                    @endif
					<div class="text-center p-t-136">
						<a class="txt2" href="{{ route('auth.register.student') }}">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>