<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-8">
                <nav>
                    <ul>
                        <li><a href="#" class="logo">
                        <!-- <img class="" src="{{ asset('img/logo_white.png') }}" width="42" height="42px" /> -->
                        {{ trans('panel.site_title') }}</a></li>
                        <li><a href="{{ route("admin.dashboard") }}" class="{{ request()->is('admin') || request()->is('admin') ? 'active' : '' }}"><i class="fa fa-th-large" aria-hidden="true"></i> {{ trans('global.dashboard') }}</a>
                        </li>
                        <li><a href="{{ route("admin.chapters.list") }}" class="{{ request()->is('admin/chapters/list') || request()->is('admin/units/create') || request()->is('admin/units/*') ? 'active' : '' }}"><i class="fa fa-book" aria-hidden="true"></i> {{ trans('cruds.chapter.title') }} & {{ trans('cruds.unit.title') }}</a>
                        </li>
                        <li><a href="{{ route("admin.boards.index") }}" class="{{ request()->is('admin/setup/boards') || request()->is('admin/setup/*') ? 'active' : '' }}"><i class="fa fa-cogs" aria-hidden="true"></i> Setup</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-4 text-right">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#createUnitModal" class="btn btn-small btn-secondary"><i class="fa fa-plus-square" aria-hidden="true"></i>
                    Create Unit</a>
                <a href="{{ route("admin.users.index") }}" class="setting-option"><span style="display:  {{ request()->is('admin/settings/users') || request()->is('admin/settings/*') ? 'inline-block' : 'none' }}">Settings</span> <i class="fa fa-cog" aria-hidden="true"></i></a>
                <a href="#" class="text-white" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="p-img" src="{{ asset('img/profile.png') }}" width="42" height="42px" />{{ Auth::user()->name }} <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    <div class="dropdown-menu profile-dropdown" style="margin-top: 28px !important;" aria-labelledby="dropdownMenuLink">
                        <!-- <a class="dropdown-item" href="#">Profile</a> -->
                        <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">Logout</a>
                    </div>
            </div>
        </div>
        </div>
        @if(request()->is('admin/settings/*'))
        <div class="container-fluid settings-header">
            <div class="row">
                <div class="col-8">
                    <nav>
                        <ul>
                            <li><a href="{{ route("admin.users.index") }}" class="{{ request()->is('admin/settings/users') ? 'active' : '' }}">User Management</a></li>
                            <li><a href="{{ route("admin.students.index") }}" class="{{ request()->is('admin/settings/students') ? 'active' : '' }}">Students</a></li>
                            <li><a href="{{ route("admin.roles.index") }}" class="{{ request()->is('admin/settings/roles') ? 'active' : '' }}">Role</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        @elseif(request()->is('admin/units/*'))
           @yield('banner')
        @endif

          <!--modal window start-->
    <div class="modal fade" id="createUnitModal" tabindex="-1" role="dialog" aria-labelledby="createUnitModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createUnitModalLabel"><i class="fa fa-plus-square" aria-hidden="true"></i>
                        Create new unit</h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mt-3">
                    <form method="POST" id="create_unit_form" enctype="multipart/form-data">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select School Board</label>
                                <select class="form-control {{ $errors->has('board') ? 'is-invalid' : '' }}" name="board_id" id="cuboard_id" required>
                                     <option value="">{{ trans('global.pleaseSelect') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Standard / Class</label>
                                <select class="form-control {{ $errors->has('class') ? 'is-invalid' : '' }}" name="class_id" id="cuclass_id" required>
                                    <option value="">{{ trans('global.pleaseSelect') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Subject</label>
                                <select class="form-control {{ $errors->has('subject') ? 'is-invalid' : '' }}" name="subject_id" id="cusubject_id" required>
                                        <option value="">{{ trans('global.pleaseSelect') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Chapter</label>
                                <select class="form-control {{ $errors->has('chapter') ? 'is-invalid' : '' }}" name="chapter_id" id="cuchapter_id" required>
                                        <option value="">{{ trans('global.pleaseSelect') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Description</label>
                                <textarea class="form-control" placeholder="{{ trans('cruds.chapter.fields.description') }}"  name="description" id="description" value="{{ old('description', '') }}" rows="6"></textarea>
                            </div>
                         </div>
                        <div class="col-md-4 mb-5">
                            <label for="exampleFormControlSelect1">Chapter Cover Image</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="validatedCustomFile" >
                                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                <div class="invalid-feedback">Example invalid custom file feedback</div>
                            </div>
                            <div class="cover-img"><img  src="{{ asset('img/profile.jpg') }}"/></div>
                                  
                        </div>
                    </div> -->
                    <!-- <input type="hidden" name="name" value="units"/>
                    <input type="hidden" name="code"  value="{{ Carbon\Carbon::now()->timestamp }}"/>
                    <input type="hidden" name="status" value="draft"/>
                    <input type="hidden" name="is_active" value="no"/>
                    <input type="hidden" name="modified_by_id" id="modified_by_id" value="{{ Auth::user()->id }}"/> -->
                    <div class="modal-footer text-center" style="display: list-item !important;">
                        <button type="submit" class="btn btn-primary btn-small">Next</button>
                        <button type="button" class="btn btn-light btn-small" data-dismiss="modal">Cancel</button>
                    </div>

                    </form>
                </div>
                
            </div>
        </div>
    </div>
   
 </header>