    <main class="chapter-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <h1 class="admin-heading-level1">Manage Board, Class, Subject & Chapters</h1>
                </div>
                <div class="col-6 text-right">
                    <a href="javascript:void(0)" onClick="addModal()" class="btn btn-secondary"><i class="fa fa-plus-square" aria-hidden="true"></i>
                        <span id="addbtn">Add Board</span></a>

                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <aside class="sidenav">
                        <ul>
                            <li><a href="{{ route('admin.boards.index') }}" class="{{ request()->is('admin/setup/boards')? 'active' : '' }}"><i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    Board</a></li>
                            <li><a href="{{ route('admin.my-classes.index') }}" class="{{ request()->is('admin/setup/my-classes')? 'active' : '' }}"><i class="fa fa-th-large" aria-hidden="true"></i> Class</a>
                            </li>
                            <li><a href="{{ route('admin.subjects.index') }}" class="{{ request()->is('admin/setup/subjects')? 'active' : '' }}"><i class="fa fa-book" aria-hidden="true"></i> Subject</a>
                            </li>
                            <li><a href="{{ route('admin.chapters.index') }}" class="{{ request()->is('admin/setup/chapters')? 'active' : '' }}"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Chapter</a></li>
                        </ul>
                     </aside>
                 </div>
                 <div class="col-10 ">
                    <div class="main-section">
             <!-- </div>
         </div>
     </main> -->