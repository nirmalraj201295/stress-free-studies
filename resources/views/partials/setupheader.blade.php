<div class="row">
            <div class="col-3">
                <h3 class="wrapper-heading" id="totalBoard"></h3>

            </div>
            <div class="col-9">
                <div class="form-row justify-content-end">
                    <div class="form-group col-md-4">
                        <input type="search" placeholder="Search" class="form-control"
                            id="search">
                    </div>
                    <div class="form-group col-md-2">
                        <select id="sortby" class="form-control">
                            <option selected>Sort by</option>
                            <option value="atoz">A to Z</option>
                            <option value="ztoa">Z to A</option>
                            <option value="is_enable">Is Enable</option>
                            <option value="created_at">Created At</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2 filterBoards" hidden>
                        <select id="filterBoard" class="form-control">
                            <option selected>Filter by Board</option>
                            <option>...</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2 filterClasses" hidden>
                        <select id="filterClass" class="form-control">
                            <option selected>Filter by Class</option>
                            <option>...</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2 filterSubjects" hidden>
                        <select id="filterSubject" class="form-control">
                            <option selected>Filter by Subject</option>
                            <option>...</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <img class="loading" src="{{ asset('img/loading.gif') }}" />
        <div class="row" id="tag_container"></div>
@section('scripts')
@parent
<script>
    // $(document).ready(function() {
   
     getBoard('filterBoard');
     $("#filterBoard").change(function(){
        // getClasses('filterClass',$(this).val());
        filterData = $(this).val();
        getData(0);
    });

    $("#filterClass").change(function(){
        // getSubjects('filterSubject',$(this).val());
        filterData = "_"+$(this).val();
        getData(0);
    });

    $("#filterSubject").change(function(){
        // getSubjects('filterSubject',$(this).val());
        filterData = "_"+$(this).val();
        getData(0);
    });
    
     getClasses('filterClass');
     getSubjects('filterSubject');
    // });
    </script>
@endsection