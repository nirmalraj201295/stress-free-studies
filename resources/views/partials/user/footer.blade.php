<!-- <div class="footerbar">
  <a href="#home" class="active">Home</a>
  <a href="#news">News</a>
  <a href="#contact">Contact</a>
</div> -->


<div class="mobile-app-icon-bar">
  <!-- <a href="#home" class="active">Home</a>
  <a href="#news">News</a>
  <a href="#contact">Contact</a> -->
  <a href="{{ route("user.home") }}" ><i class="fa fa-home" aria-hidden="true"></i><br/><span style="font-size: 10px;">Home</span></a>
  <a href="{{ route("user.mycourse") }}" ><i class="fa fa-book" aria-hidden="true"></i><br/><span style="font-size: 10px;">My Course</span></a>
  <a href="{{ route("user.profile") }}" ><i class="fa fa-user" aria-hidden="true"></i><br/><span style="font-size: 10px;">Profile</span></a>
  <!-- <button><i class="fa fa-list-ul" aria-hidden="true"></i></button>
  <button><i class="fa fa-cog" aria-hidden="true"></i></button> -->
</div>
