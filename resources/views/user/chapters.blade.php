
@extends('layouts.user')
<div class="container">
    <div class="row">
    <section class="banner-section">
        <div class="container-fluid">
             <a href="{{ route('user.home') }}" class="text-warning"><i style="font-size: 20px;" class="fa fa-long-arrow-left" aria-hidden="true"></i></a><span class="text-warning"> back </span>
        </div>
	</section>
    </div>
</div>

<div class="title-card">
    <img class="bg-dark" src="{{ asset('img/default-img.jpg') }}" width="70" height="70">
    <div class="ml-2 p-2">
    	<h2>{!! $subject[0]->name !!}</h2>
      
    </div>
</div>

<div class="container">
    <div class="row">
    <section class="banner-sec">
        <div class="container-fluid">
             <a href="#" class="text-warn">Chapters</a>
             <!-- <a href="#" class="view-property">View all</a> -->
        </div>
    </section>
    </div>
</div>

<div class="row row m-2" id="tag_container"></div>

@section('scripts')
@parent
<script>
    var ajaxurl = "{{ route('user.chapters.ajaxData') }}";
    // console.log("subject id",{{$data['id']}});
// var addurl = '{{ route("admin.chapters.store") }}';
// var editurl = '{{ route("admin.chapters.edit", ":id") }}';
// var updateurl = '{{ route("admin.chapters.update", ":id") }}';
// var deleteurl = '{{ route("admin.chapters.destroy", ":id") }}';
// var dropImageurl = "{{ route('admin.chapters.storeMedia') }}";
    

   
</script>
<script src="{{ asset('js/user.js') }}"></script>
<script>
$(document).ready(function(){
    getById = "{{$data['id']}}";
    getData(page);

    });
</script>
@endsection