<link href="{{ asset('css/flashcard.css') }}" rel="stylesheet" />
@extends('layouts.user')
@section('footer')
@stop

<div class="container">
    <div class="row">
    <section class="banner-section">
        <div class="container-fluid">
             <a href="{{ url('units/') }}/{{$units[0]->chapter_id}}" class="text-warning"><i style="font-size: 20px;" class="fa fa-long-arrow-left" aria-hidden="true"></i></a><span class="text-warning"> back </span>
        </div>
	</section>
    </div>
    <div class="row">
	    <div class="title-card">
		    <img class="bg-dark" src="{{ asset('img/default-img.jpg') }}" width="70" height="70">
		    <div class="ml-2 p-2">
		    	<h2>{!! $units[0]->name !!}</h2>
		    </div>
		</div>
	    <div class="col-md-12 text-center">
	        <button type="button" style="width: auto;font-size: 18px;" class="btn-primary btn-sm">
	        Mastered<br/>
	        <span id="master" class="">{!! $master !!}</span>
	        </button>
	        <button type="button" style="width: auto;font-size: 18px;" class="btn-primary btn-sm">
	        Familiar<br/>
	        <span id="familiar" class="">{!! $familiar !!}</span>
	        </button>
	        <button type="button" style="width: auto;font-size: 18px;" class="btn-primary btn-sm">
	        Not Familiar<br/>
	        <span id="notfamiliar" class="">{!! $notfamiliar !!}</span>
	        </button>
	     </div>
     </div>
</div>
<div class="sliderContainer" id="tag_container" style="margin-top:5%;"></div>

@section('scripts')
@parent
<script>
    var ajaxurl = "{{ route('user.qa.ajaxData') }}";
    // console.log("subject id",{{$data['id']}});
// var addurl = '{{ route("admin.chapters.store") }}';
// var editurl = '{{ route("admin.chapters.edit", ":id") }}';
// var updateurl = '{{ route("admin.chapters.update", ":id") }}';
// var deleteurl = '{{ route("admin.chapters.destroy", ":id") }}';
// var dropImageurl = "{{ route('admin.chapters.storeMedia') }}";

$(document).ready(function() {

    getById = "{{$data['id']}}";
    getData(page);

	
});

   
</script>
<script src="{{ asset('js/user.js') }}"></script>

@endsection