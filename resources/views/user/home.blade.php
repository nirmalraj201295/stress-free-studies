
@extends('layouts.user')
            <!--Carousel Wrapper-->
            <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel" style="margin-bottom: 1em !important">
                <!--Indicators-->
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <!--/.Indicators-->
                <!--Slides-->
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="view hm-white-slight">
                             <img  class="carousel-image" src="img/banner_carousel/carousel_1.png" alt="First slide">
                            <div class="mask"></div>
                        </div>
                        
                      <img src="{{ asset('img/logo.png') }}" alt="logo" class="carousel-logo">

                        <div class="carousel-cap">
                        <i>Welcome To <br>
                        Stress Free Studies</i>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <!--Mask color-->
                        <div class="view hm-white-slight">
                            <img class="carousel-image" src="img/banner_carousel/carousel_2.jpg" alt="Second slide">
                            <div class="mask"></div>
                        </div>
          
                       <img src="{{ asset('img/logo.png') }}" alt="logo" class="carousel-logo">
                       
                    </div>
                    <div class="carousel-item">
                        <!--Mask color-->
                        <div class="view hm-white-slight">
                            <img  class="carousel-image" src="img/banner_carousel/carousel_3.jpg" alt="Third slide">
                            <div class="mask"></div>
                        </div>
                      
                       
                     <img src="{{ asset('img/logo.png') }}" alt="logo" class="carousel-logo-3">
                    </div>
                </div>
                <!--/.Slides-->
                <!--Controls-->
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->

<div class="container">
    <div class="row">
    <section class="banner-sec">
        <div class="container-fluid">
             <a href="{{ route('user.home') }}" class="text-warn">Courses for You</a>
             <a href="#" class="view-property">View all</a>
        </div>
	</section>
    </div>
</div>
<div class="row row m-2" id="tag_container"></div>

@section('scripts')
@parent
<script>
    var ajaxurl = "{{ route('user.subjects.ajaxData') }}";
// var addurl = '{{ route("admin.chapters.store") }}';
// var editurl = '{{ route("admin.chapters.edit", ":id") }}';
// var updateurl = '{{ route("admin.chapters.update", ":id") }}';
// var deleteurl = '{{ route("admin.chapters.destroy", ":id") }}';
// var dropImageurl = "{{ route('admin.chapters.storeMedia') }}";
    

   
</script>
<script src="{{ asset('js/user.js') }}"></script>
<script>getData(page)</script>
@endsection