
@extends('layouts.user')
<div class="container">
    <div class="row">
    <section class="banner-sec">
        <div class="container-fluid">
             <a href="{{ route('user.mycourse') }}" class="text-warn">My Courses</a>
        </div>
	</section>
    </div>
</div>
<div class="row row m-2" id="tag_container"></div>

@section('scripts')
@parent
<script>
    var ajaxurl = "{{ route('user.mycourse.subAajaxData') }}";
// var addurl = '{{ route("admin.chapters.store") }}';
// var editurl = '{{ route("admin.chapters.edit", ":id") }}';
// var updateurl = '{{ route("admin.chapters.update", ":id") }}';
// var deleteurl = '{{ route("admin.chapters.destroy", ":id") }}';
// var dropImageurl = "{{ route('admin.chapters.storeMedia') }}";
    

   
</script>
<script src="{{ asset('js/user.js') }}"></script>
<script>getData(page)</script>
@endsection