@extends('layouts.user')
<link href="{{ asset('css/appcustom.css') }}" rel="stylesheet" />
<div class="container">
    <div class="row">
    <section class="banner-section">
        <div class="container-fluid d-inline-flex">
       	  <div class="text-left w-50">
             <a href="{{ route('user.home') }}" class="text-warning"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> back</a>
          </div>
          <div class="text-right w-50">
      		 <a href="#" class="text-warning" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
    		 {{ trans('global.logout') }}</a>&nbsp;<i class="nav-icon fas fa-fw fa-sign-out-alt"></i>
    	  </div> 
        </div>
	</section>
    </div>

    <div class="w-100 d-inline-flex">
    	<div class="w-50"><h1>Profile</h1></div>
    	<div class="pt-2 text-right w-50"><a data-target="#changePassword" data-toggle="modal" href="#">Change Password</a></div>
    </div>	
    <div class="p-2 d-inline-flex">
    	<img class="bg-dark" src="{{ asset('img/profile.png') }}" style="border-radius: 20px;" width="40" height="40">
    	<div class="ml-2 p-2"><p>{{$data[0]->studentName}}</p></div>
    </div>
    <div class="p-2 d-list">
	    <div class="form-group mb-2">
	    	<label>E-mail Id</label>
	    	<input type="text" class="form-control bg-white" id="name" name="name" value="{{$data[0]->studentEmail}}" readonly>
	    </div>
	    <div class="form-group mb-2">
	    	<label>My Board</label>
	    	<input type="text" class="form-control bg-white" id="name" name="name" value="{{$data[0]->boardName}}" readonly>
	    </div>
	    <div class="form-group mb-2">
	    	<label>My Class</label>
	    	<input type="text" class="form-control bg-white" id="name" name="name" value="{{$data[0]->className}}" readonly>
	    </div>
    </div>
</div>

<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content pb-2">
                 <div class="modal-header">
                    <h5 class="modal-title"><span id="addFormModalLabel">Change Password</span></h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                <form id="changePasswordForm" class="login100-form validate-form" method="POST" action="{{ route('user.changepassword') }}">
                
               
	                <div class="pt-2">  
	                <span class="error d-none text-danger">Passwords does not match.</span>
	                <span class="success d-none text-success">Your Password have Successfully updated.</span>	                  
                    <div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <input id="password" name="password" type="password" class="input100" required placeholder="{{ trans('global.login_password') }}" value="">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

                    <div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <input id="password_confirmation" name="password_confirmation" type="password" class="input100" required placeholder="{{ trans('global.login_password_confirmation') }}" value="">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					</div>
					</form>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="submit btn btn-success">Change</button>
        			<button type="button" class="cancel btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    @section('scripts')
    <script>
    $("#changePassword .submit").on('click',function(){
    	var password = $('#password').val();
    	var passwordConf = $('#password_confirmation').val();
    	if((password == passwordConf)&&!(password=='')){
    		//$('#changePasswordForm').submit();
	        $.ajax({
	            url: "{{ route('user.changepassword') }}?password=" + password,
	            method: 'POST',
	            success: function(data) {
	                //$('#class_id').html(data.html);
	                //alert('done');
	                //$('#theModal').modal('hide');	                
	            	$('#changePassword .success').removeClass('d-none');
	            	setTimeout(function(){location.reload(); }, 1500);	            	
	            }
	        });
        }else{
			$('#changePassword .error').removeClass('d-none');
			//$('#changePassword').modal('show');				
        }
    });
    </script>
    @endsection