<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToQuestionAnswersTable extends Migration
{
    public function up()
    {
        Schema::table('question_answers', function (Blueprint $table) {
            $table->unsignedInteger('unit_id');

            $table->foreign('unit_id', 'unit_fk_658144')->references('id')->on('units');

            $table->unsignedInteger('language_id');

            $table->foreign('language_id', 'language_fk_658145')->references('id')->on('languages');

            $table->unsignedInteger('question_type_id');

            $table->foreign('question_type_id', 'question_type_fk_658146')->references('id')->on('question_types');

            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_658155')->references('id')->on('users');
        });
    }
}
