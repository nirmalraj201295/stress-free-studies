<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToChaptersTable extends Migration
{
    public function up()
    {
        Schema::table('chapters', function (Blueprint $table) {
            $table->unsignedInteger('board_id');

            $table->foreign('board_id', 'board_fk_657357')->references('id')->on('boards');

            $table->unsignedInteger('class_id');

            $table->foreign('class_id', 'class_fk_657358')->references('id')->on('my_classes');

            $table->unsignedInteger('subject_id');

            $table->foreign('subject_id', 'subject_fk_657359')->references('id')->on('subjects');

            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657367')->references('id')->on('users');
        });
    }
}
