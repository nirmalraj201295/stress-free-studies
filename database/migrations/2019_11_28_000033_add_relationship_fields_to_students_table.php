<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToStudentsTable extends Migration
{
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->unsignedInteger('board_id');

            $table->foreign('board_id', 'board_fk_657890')->references('id')->on('boards');

            $table->unsignedInteger('class_id');

            $table->foreign('class_id', 'class_fk_657891')->references('id')->on('my_classes');

            $table->unsignedInteger('city_id')->nullable();

            $table->foreign('city_id', 'city_fk_657893')->references('id')->on('cities');

            $table->unsignedInteger('state_id')->nullable();

            $table->foreign('state_id', 'state_fk_657894')->references('id')->on('states');

            $table->unsignedInteger('country_id')->nullable();

            $table->foreign('country_id', 'country_fk_657895')->references('id')->on('countries');

            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657898')->references('id')->on('users');
        });
    }
}
