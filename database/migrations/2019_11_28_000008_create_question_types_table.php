<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionTypesTable extends Migration
{
    public function up()
    {
        Schema::create('question_types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('is_active');

            $table->string('type');

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
