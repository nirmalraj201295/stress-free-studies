<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToBoardsTable extends Migration
{
    public function up()
    {
        Schema::table('boards', function (Blueprint $table) {
            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657287')->references('id')->on('users');
        });
    }
}
