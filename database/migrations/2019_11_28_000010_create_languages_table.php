<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code')->nullable();

            $table->string('name')->nullable();

            $table->string('is_active');

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
