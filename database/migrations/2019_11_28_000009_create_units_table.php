<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code')->unique();

            $table->string('name');

            $table->string('description')->nullable();

            $table->integer('sort_order')->nullable();

            $table->string('status');

            $table->string('is_active');

            $table->timestamps();

            $table->softDeletes(); 
        });
    }
}
