<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToMyClassesTable extends Migration
{
    public function up()
    {
        Schema::table('my_classes', function (Blueprint $table) {
            $table->unsignedInteger('board_id');

            $table->foreign('board_id', 'board_fk_657330')->references('id')->on('boards');

            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657338')->references('id')->on('users');
        });
    }
}
