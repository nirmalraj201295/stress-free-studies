<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionAnswersTable extends Migration
{
    public function up()
    {
        Schema::create('question_answers', function (Blueprint $table) {
            $table->increments('id');

            $table->longText('question');

            $table->longText('option')->nullable();

            $table->longText('answer');

            $table->integer('sort_order');

            $table->string('status');

            $table->string('is_active');

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
