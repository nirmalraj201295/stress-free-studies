<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToStatesTable extends Migration
{
    public function up()
    {
        Schema::table('states', function (Blueprint $table) {
            $table->unsignedInteger('country_id');

            $table->foreign('country_id', 'country_fk_657728')->references('id')->on('countries');

            $table->unsignedInteger('modified_by_id')->nullable();

            $table->foreign('modified_by_id', 'modified_by_fk_657733')->references('id')->on('users');
        });
    }
}
