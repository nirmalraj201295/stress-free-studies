<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToQuestionTypesTable extends Migration
{
    public function up()
    {
        Schema::table('question_types', function (Blueprint $table) {
            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657519')->references('id')->on('users');
        });
    }
}
