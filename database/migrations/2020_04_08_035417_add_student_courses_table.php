<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStudentCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_courses', function (Blueprint $table) {
        	$table->unsignedInteger('student_id');
        	
        	$table->foreign('student_id', 'studentid_fk_657944')->references('id')->on('students');
        	$table->unsignedInteger('subject_id');
        	
        	$table->foreign('subject_id', 'subjectid_fk_657955')->references('id')->on('subjects');
        });
    }

}
