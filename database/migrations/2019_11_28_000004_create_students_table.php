<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');

            $table->string('studentid');

            $table->string('name');

            $table->date('dob')->nullable();

            $table->string('email');

            $table->string('current_address')->nullable();

            $table->string('is_active');

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
