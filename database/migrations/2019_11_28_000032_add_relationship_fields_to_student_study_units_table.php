<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToStudentStudyUnitsTable extends Migration
{
    public function up()
    {
        Schema::table('student_study_units', function (Blueprint $table) {
            $table->unsignedInteger('studentid_id');

            $table->foreign('studentid_id', 'studentid_fk_657923')->references('id')->on('students');

            $table->unsignedInteger('unitid_id');

            $table->foreign('unitid_id', 'unitid_fk_657924')->references('id')->on('units');

            $table->unsignedInteger('chapterid_id');

            $table->foreign('chapterid_id', 'chapterid_fk_657925')->references('id')->on('chapters');

            $table->unsignedInteger('subjectid_id');

            $table->foreign('subjectid_id', 'subjectid_fk_657926')->references('id')->on('subjects');
        });
    }
}
