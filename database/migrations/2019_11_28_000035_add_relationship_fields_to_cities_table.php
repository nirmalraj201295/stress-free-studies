<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCitiesTable extends Migration
{
    public function up()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->unsignedInteger('country_id');

            $table->foreign('country_id', 'country_fk_657847')->references('id')->on('countries');

            $table->unsignedInteger('state_id');

            $table->foreign('state_id', 'state_fk_657848')->references('id')->on('states');

            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657853')->references('id')->on('users');
        });
    }
}
