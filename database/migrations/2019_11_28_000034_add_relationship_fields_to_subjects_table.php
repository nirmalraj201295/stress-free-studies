<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToSubjectsTable extends Migration
{
    public function up()
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->unsignedInteger('board_id');

            $table->foreign('board_id', 'board_fk_657343')->references('id')->on('boards');

            $table->unsignedInteger('class_id');

            $table->foreign('class_id', 'class_fk_657344')->references('id')->on('my_classes');

            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657352')->references('id')->on('users');
        });
    }
}
