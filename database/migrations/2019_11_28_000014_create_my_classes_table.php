<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyClassesTable extends Migration
{
    public function up()
    {
        Schema::create('my_classes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code')->unique();

            $table->string('name');

            $table->string('description')->nullable();

            $table->string('status');

            $table->string('is_active');

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
