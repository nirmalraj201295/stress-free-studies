<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToStudentUnitAccessLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_unit_access_log', function (Blueprint $table) {
        	$table->unsignedInteger('student_id');
        	
        	$table->foreign('student_id', 'studentid_fk_657955')->references('id')->on('students');
        	$table->unsignedInteger('unit_id');
        	
        	$table->foreign('unit_id', 'unitid_fk_657944')->references('id')->on('units');
        	
        });
    }

    
}
