<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToUnitsTable extends Migration
{
    public function up()
    {
        Schema::table('units', function (Blueprint $table) {
            $table->unsignedInteger('board_id');

            $table->foreign('board_id', 'board_fk_657372')->references('id')->on('boards');

            $table->unsignedInteger('class_id');

            $table->foreign('class_id', 'class_fk_657373')->references('id')->on('my_classes');

            $table->unsignedInteger('subject_id');

            $table->foreign('subject_id', 'subject_fk_657374')->references('id')->on('subjects');

            $table->unsignedInteger('chapter_id');

            $table->foreign('chapter_id', 'chapter_fk_657375')->references('id')->on('chapters');

            $table->unsignedInteger('modified_by_id');

            $table->foreign('modified_by_id', 'modified_by_fk_657383')->references('id')->on('users');
        });
    }
}
