<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'                 => 1,
                'name'               => 'Admin',
                'email'              => 'admin@admin.com',
                'password'           => '$2y$10$fYe.BtULFh14De4.kzZJ.u3ugPWIfFgpK0bheonhg8XJKh6O0t2Dy',
                'remember_token'     => null,
                'approved'           => 1,
                'verified'           => 1,
                'verified_at'        => '2019-11-27 11:04:24',
                'verification_token' => '',
            ],
        ];

        User::insert($users);
    }
}
