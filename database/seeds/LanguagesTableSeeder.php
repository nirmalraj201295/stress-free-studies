<?php

use App\Language;
use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    public function run()
    {
        $languages = [
            [
                'code' => 'en',
                'name' => 'English',
                'is_active' => 'yes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'modified_by_id' => 1,
            ],
            
        ];
        Language::insert($languages);
    }
}