<?php

use App\QuestionType;
use Illuminate\Database\Seeder;

class QuestiontypeTableSeeder extends Seeder
{
    public function run()
    {
        $questiontypes = [
            [
                'type' => 'flash_card',
                'is_active' => 'yes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'modified_by_id' => 1,
            ],
            
        ];
        QuestionType::insert($questiontypes);
    }
}