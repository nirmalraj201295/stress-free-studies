<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'audit_log_show',
            ],
            [
                'id'    => '18',
                'title' => 'audit_log_access',
            ],
            [
                'id'    => '19',
                'title' => 'user_alert_create',
            ],
            [
                'id'    => '20',
                'title' => 'user_alert_show',
            ],
            [
                'id'    => '21',
                'title' => 'user_alert_delete',
            ],
            [
                'id'    => '22',
                'title' => 'user_alert_access',
            ],
            [
                'id'    => '23',
                'title' => 'faq_management_access',
            ],
            [
                'id'    => '24',
                'title' => 'faq_category_create',
            ],
            [
                'id'    => '25',
                'title' => 'faq_category_edit',
            ],
            [
                'id'    => '26',
                'title' => 'faq_category_show',
            ],
            [
                'id'    => '27',
                'title' => 'faq_category_delete',
            ],
            [
                'id'    => '28',
                'title' => 'faq_category_access',
            ],
            [
                'id'    => '29',
                'title' => 'faq_question_create',
            ],
            [
                'id'    => '30',
                'title' => 'faq_question_edit',
            ],
            [
                'id'    => '31',
                'title' => 'faq_question_show',
            ],
            [
                'id'    => '32',
                'title' => 'faq_question_delete',
            ],
            [
                'id'    => '33',
                'title' => 'faq_question_access',
            ],
            [
                'id'    => '34',
                'title' => 'content_management_access',
            ],
            [
                'id'    => '35',
                'title' => 'content_category_create',
            ],
            [
                'id'    => '36',
                'title' => 'content_category_edit',
            ],
            [
                'id'    => '37',
                'title' => 'content_category_show',
            ],
            [
                'id'    => '38',
                'title' => 'content_category_delete',
            ],
            [
                'id'    => '39',
                'title' => 'content_category_access',
            ],
            [
                'id'    => '40',
                'title' => 'content_tag_create',
            ],
            [
                'id'    => '41',
                'title' => 'content_tag_edit',
            ],
            [
                'id'    => '42',
                'title' => 'content_tag_show',
            ],
            [
                'id'    => '43',
                'title' => 'content_tag_delete',
            ],
            [
                'id'    => '44',
                'title' => 'content_tag_access',
            ],
            [
                'id'    => '45',
                'title' => 'content_page_create',
            ],
            [
                'id'    => '46',
                'title' => 'content_page_edit',
            ],
            [
                'id'    => '47',
                'title' => 'content_page_show',
            ],
            [
                'id'    => '48',
                'title' => 'content_page_delete',
            ],
            [
                'id'    => '49',
                'title' => 'content_page_access',
            ],
            [
                'id'    => '50',
                'title' => 'board_create',
            ],
            [
                'id'    => '51',
                'title' => 'board_edit',
            ],
            [
                'id'    => '52',
                'title' => 'board_show',
            ],
            [
                'id'    => '53',
                'title' => 'board_delete',
            ],
            [
                'id'    => '54',
                'title' => 'board_access',
            ],
            [
                'id'    => '55',
                'title' => 'my_class_create',
            ],
            [
                'id'    => '56',
                'title' => 'my_class_edit',
            ],
            [
                'id'    => '57',
                'title' => 'my_class_show',
            ],
            [
                'id'    => '58',
                'title' => 'my_class_delete',
            ],
            [
                'id'    => '59',
                'title' => 'my_class_access',
            ],
            [
                'id'    => '60',
                'title' => 'subject_create',
            ],
            [
                'id'    => '61',
                'title' => 'subject_edit',
            ],
            [
                'id'    => '62',
                'title' => 'subject_show',
            ],
            [
                'id'    => '63',
                'title' => 'subject_delete',
            ],
            [
                'id'    => '64',
                'title' => 'subject_access',
            ],
            [
                'id'    => '65',
                'title' => 'chapter_create',
            ],
            [
                'id'    => '66',
                'title' => 'chapter_edit',
            ],
            [
                'id'    => '67',
                'title' => 'chapter_show',
            ],
            [
                'id'    => '68',
                'title' => 'chapter_delete',
            ],
            [
                'id'    => '69',
                'title' => 'chapter_access',
            ],
            [
                'id'    => '70',
                'title' => 'unit_create',
            ],
            [
                'id'    => '71',
                'title' => 'unit_edit',
            ],
            [
                'id'    => '72',
                'title' => 'unit_show',
            ],
            [
                'id'    => '73',
                'title' => 'unit_delete',
            ],
            [
                'id'    => '74',
                'title' => 'unit_access',
            ],
            [
                'id'    => '75',
                'title' => 'question_type_create',
            ],
            [
                'id'    => '76',
                'title' => 'question_type_edit',
            ],
            [
                'id'    => '77',
                'title' => 'question_type_show',
            ],
            [
                'id'    => '78',
                'title' => 'question_type_delete',
            ],
            [
                'id'    => '79',
                'title' => 'question_type_access',
            ],
            [
                'id'    => '80',
                'title' => 'country_create',
            ],
            [
                'id'    => '81',
                'title' => 'country_edit',
            ],
            [
                'id'    => '82',
                'title' => 'country_show',
            ],
            [
                'id'    => '83',
                'title' => 'country_delete',
            ],
            [
                'id'    => '84',
                'title' => 'country_access',
            ],
            [
                'id'    => '85',
                'title' => 'state_create',
            ],
            [
                'id'    => '86',
                'title' => 'state_edit',
            ],
            [
                'id'    => '87',
                'title' => 'state_show',
            ],
            [
                'id'    => '88',
                'title' => 'state_delete',
            ],
            [
                'id'    => '89',
                'title' => 'state_access',
            ],
            [
                'id'    => '90',
                'title' => 'city_create',
            ],
            [
                'id'    => '91',
                'title' => 'city_edit',
            ],
            [
                'id'    => '92',
                'title' => 'city_show',
            ],
            [
                'id'    => '93',
                'title' => 'city_delete',
            ],
            [
                'id'    => '94',
                'title' => 'city_access',
            ],
            [
                'id'    => '95',
                'title' => 'student_create',
            ],
            [
                'id'    => '96',
                'title' => 'student_edit',
            ],
            [
                'id'    => '97',
                'title' => 'student_show',
            ],
            [
                'id'    => '98',
                'title' => 'student_delete',
            ],
            [
                'id'    => '99',
                'title' => 'student_access',
            ],
            [
                'id'    => '100',
                'title' => 'student_study_unit_create',
            ],
            [
                'id'    => '101',
                'title' => 'student_study_unit_edit',
            ],
            [
                'id'    => '102',
                'title' => 'student_study_unit_show',
            ],
            [
                'id'    => '103',
                'title' => 'student_study_unit_delete',
            ],
            [
                'id'    => '104',
                'title' => 'student_study_unit_access',
            ],
            [
                'id'    => '105',
                'title' => 'language_create',
            ],
            [
                'id'    => '106',
                'title' => 'language_edit',
            ],
            [
                'id'    => '107',
                'title' => 'language_show',
            ],
            [
                'id'    => '108',
                'title' => 'language_delete',
            ],
            [
                'id'    => '109',
                'title' => 'language_access',
            ],
            [
                'id'    => '110',
                'title' => 'question_answer_create',
            ],
            [
                'id'    => '111',
                'title' => 'question_answer_edit',
            ],
            [
                'id'    => '112',
                'title' => 'question_answer_show',
            ],
            [
                'id'    => '113',
                'title' => 'question_answer_delete',
            ],
            [
                'id'    => '114',
                'title' => 'question_answer_access',
            ],
        ];

        Permission::insert($permissions);
    }
}
